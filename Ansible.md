### Ansible 是什么？

Ansible 是新出现的自动化运维工具，基于 Python 开发，集合了众多运维工具（puppet、chef、func、fabric）的优点，实现了批量系统配置、批量程序部署、批量运行命令等功能。

Ansible 是基于 paramiko 开发的，并且基于模块化工作，本身没有批量部署的能力。真正具有批量部署的是 ansible 所运行的模块，ansible 只是提供一种框架。ansible 不需要在远程主机上安装 client/agents，因为它们是基于 ssh 来和远程主机通讯的。ansible 目前已经已经被红帽官方收购，是自动化运维工具中大家认可度最高的，并且上手容易，学习简单，是每位运维工程师必须掌握的技能之一。



参考资料：https://www.cnblogs.com/keerya/p/7987886.html



### 一、主要特点

- 部署简单，只需在主控端部署 Ansible 环境，被控端无需做任何操作；
- 默认使用 SSH 协议对设备进行管理；
- 有大量常规运维操作模块，可实现日常绝大部分操作；
- 配置简单、功能强大、扩展性强；
- 支持 API 及自定义模块，可通过 Python 轻松扩展；
- 通过 Playbooks 来定制强大的配置、状态管理；
- 轻量级，无需在客户端安装 agent，更新时，只需在操作机上进行一次更新即可；
- 提供一个功能强大、操作性强的 Web 管理界面和 REST API 接口 - AWX 平台。



### 二、执行过程

- 加载自己的配置文件，默认 `/etc/ansible/ansible.cfg`；
- 查找对应的主机配置文件，找到要执行的主机或者组；
- 加载自己对应的模块文件，如 command；
- 通过 ansible 将模块或命令生成对应的临时 py 文件（python脚本）， 并将该文件传输至远程服务器；
- 对应执行用户的家目录的 `.ansible/tmp/XXX/XXX.PY` 文件；
- 给文件 +x 执行权限；
- 执行并返回结果；
- 删除临时 py 文件，`sleep 0` 退出；



### 三、准备工作

由于 Ansible 使用 ssh 进行远程主机控制，所以要避免在程序操作过程中因为密码限制而中断程序操作，这里需要在控制主机上生成 ssh 公钥，并使用 ssh-copy-id 命令将 **公钥复制到远程主机** 上，以便后续 Ansible 可以无感操作远程主机。

1、首先在控制主机上生成 ssh 公钥*（一直回车即可，不要设置密码，不然每次 ssh 操作都需要输入这个密码）*。

```shell
# 生成公钥
# ssh-keygen -t rsa
$ ssh-keygen
Generating public/private rsa key pair.
Enter file in which to save the key (/root/.ssh/id_rsa): 【回车】
Enter passphrase (empty for no passphrase): 【回车】
Enter same passphrase again: 【直接回车】
Your identification has been saved in /root/.ssh/id_rsa.
Your public key has been saved in /root/.ssh/id_rsa.pub.
The key fingerprint is:
SHA256:Ai79DHmq4JeOPK6ZxL2jbHglVCZs8vRFhJfTQPKwulc root@tangxbai
The key's randomart image is:
+---[RSA 2048]----+
| .  o==+         |
|. = +== .        |
| = =oo..         |
|  o+.o           |
| .o = E S        |
|. oo.B .         |
|oo.++ o          |
|**o*.            |
|BO*o.            |
+----[SHA256]-----+

# 验证是否生成成功
$ cat ~/.ssh/id_rsa      # 私钥
$ cat ~/.ssh/id_rsa.pub  # 公钥

# 移除远程主机公钥
$ ssh-keygen -R <host>
```

2、将公钥拷贝到远程主机

```shell
# 解释：
# sshpass -p <pwd>：跳过手动输入远程主机密码的步骤，直接静默密码登入；
# ssh-copy-id：ssh 提供的远程公钥复制组件；
# -o StrictHostKeyChecking=no：ssh-copy-id 组件的参数，可以跳过确认步骤直接以确认放置执行；
# sshpass ... ssh-copy-id ...
$ sshpass -p <pwd> ssh-copy-id -o StrictHostKeyChecking=no <host>
```

3、测试主机（测试完毕后记得退出远程主机，避免操作混乱）

```shell
$ ssh <host>
$ exit
```



### 四、程序安装

1、基础软件安装

```shell
yum install epel-release ansible -y

# 基础命令格式
ansible <host-pattern> [-f forks] [-m module] [-a args]
```

2、配置远程主机列表

```shell
cat >> /etc/ansible/hosts << EOF
[test]
192.168.1.101
192.168.1.102
EOF
```

3、测试主机连通性（只有在连通状态，后面的操作才可以正常进行）

```shell
# 对 hosts 里面的某个主机进行操作
$ ansible 192.168.1.101 -m ping 
# <host> | SUCCESS => {
#    "changed": false, 
#    "ping": "pong"
# }

# 对 hosts 里面的某个分组进行操作
$ ansible test -m ping 
# <host> | SUCCESS => {
#    "changed": false, 
#    "ping": "pong"
# }
```



### 五、常用操作

1、command 模块

命令模块接受命令名称，后面是空格分隔的列表参数。给定的命令将在所有选定的节点上执行。它不会通过 shell 进行处理，比如 $HOME 和操作如 "<"，">"，"|"，";"，"&" 工作（需要使用（shell）模块实现这些功能）。注意，该命令不支持 | 管道命令，以下是一些常用命令。

```shell
$ chdir       # 在执行命令之前，先切换到该目录
$ executable  # 切换shell来执行命令，需要使用命令的绝对路径
$ free_form   # 要执行的Linux指令，一般使用Ansible的-a参数代替。
$ creates     # 一个文件名，当这个文件存在，则该命令不执行,可以用来做判断
$ removes     # 一个文件名，这个文件不存在，则该命令不执行

# 示例例一：
# 先切换到 /data/ 目录，再执行 “ls” 命令
$ ansible <host|group> -m command -a 'chdir=/data/ ls'

# 示例二：
# 如果 ~/.ssh/authorized_keys 存在，则不执行 “ls” 命令
$ ansible <host|group> -m command -a 'creates=~/.ssh/authorized_keys ls'

# 示例三：
# 如果 ~/.ssh/authorized_keys 存在，则执行 “cat ~/.ssh” 命令
$ ansible <host|group> -m command -a 'removes=~/.ssh/authorized_keys cat ~/.ssh/authorized_keys'
```

2、shell 模块

此模块可以在远程主机上调用 shell 解释器运行命令，支持 shell 的各种功能，例如管道等。

```shell
$ ansible <host|group> -m shell -a 'cat ~/.ssh/authorized_keys | grep "ssh-rsa"'
```

3、copy 模块

这个模块用于将文件复制到远程主机，同时支持给定内容生成文件和修改权限等。

```shell
$ src　           # 被复制的源文件。可以是绝对路径或相对路径。如果路径是一个目录，则会递归复制，用法类似于 "rsync"
$ content　　　    # 用于替换 "src"，可以直接指定文件的值
$ dest            # 必选项，将源文件复制到的远程主机的绝对路径
$ backup          # 当文件内容发生改变后，在覆盖之前把源文件备份，备份文件包含时间信息
$ directory_mode  # 递归设定目录的权限，默认为系统默认权限
$ force　         # 当目标主机包含该文件，但内容不同时，设为"yes"，表示强制覆盖；设为"no"，表示目标主机的目标位置不存在该文件才复制。默认为"yes"
$ others          # 所有的 file 模块中的选项可以在这里使用

# 示例一：
$ ansible <host|group> -m copy -a 'src=~/hello dest=/data/hello'

# 示例二：
$ ansible <host|group> -m copy -a 'content="Hello Ansible" dest=/data/name mode=666'
```















