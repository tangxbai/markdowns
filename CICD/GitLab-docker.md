### 一、废话简介

GitLab 是一个强大的版本控制和协作平台，用于管理代码仓库、项目、问题跟踪、持续集成和部署等软件开发任务。下面是一个详细的GitLab 使用教程，包括创建项目、添加成员、管理代码、问题跟踪和持续集成等方面的基本步骤。



CI/CD 是一种通过在应用开发阶段引入**自动化**来频繁向客户交付应用的方法。

CI/CD 的核心概念是 **持续集成、持续交付和持续部署**。它是作为一个面向开发和运营团队的解决方案，主要针对在集成新代码时所引发的问题（也称为：“集成地狱”）。

CI/CD 可让持续自动化和持续监控贯穿于应用的整个生命周期（从集成和测试阶段，到交付和部署）。



### 二、部署代码仓库

阿里云镜像地址：[https://mirrors.aliyun.com/gitlab-ce/yum/el7](https://mirrors.aliyun.com/gitlab-ce/yum/el7)

清华大学镜像地址：[https://mirrors.tuna.tsinghua.edu.cn/gitlab-ce/yum/el7](https://mirrors.tuna.tsinghua.edu.cn/gitlab-ce/yum/el7)

1、系统环境准备

```shell
# 安装依赖
# postfix: 用于发送邮件
# openssh-server: 提供 ssh 服务
# policycoreutils-python: ssh 的依赖
yum install -y curl policycoreutils-python openssh-server postfix

# 开启服务
systemctl enable sshd && systemctl start sshd
systemctl enable postfix && systemctl start postfix

# 配置系统防火墙
firewall-cmd --add-service=ssh --permanent	# 开放 ssh 服务端口，permanent 代表永久生效
firewall-cmd --add-service=http --permanent	# 开放 http 服务端口，permanent 代表永久生效
systemctl reload firewalld
systemctl restart firewalld

# 如果找不到防火墙则执行以下命令
# yum install -y firewalld systemd
```

2、下载程序并安装

```shell
mkdir -p /usr/local/gitlab
wget -O /usr/local/gitlab/gitlab-ce.rpm https://mirrors.aliyun.com/gitlab-ce/yum/el7/gitlab-ce-10.0.0-ce.0.el7.x86_64.rpm
rpm -i /usr/local/gitlab/gitlab-ce.rpm
rm -rf /usr/local/gitlab
```

3、更改配置

```shell
vim /etc/gitlab/gitlab.rb
#---------------------------------#
# exteral_url "http://ip:9000"
#---------------------------------#

# 更新防火墙（开放 gitlab 服务端口，即上面修改的 ip:port ）
firewall-cmd --zone=public --add-port=9000/tcp --permanent
firewall-cmd --zone=public --remove-port=5672/tcp --permanent
firewall-cmd --reload

# 装载配置
gitlab-ctl reconfigure 
gitlab-ctl restart
```

4、开机自启

```shell
systemctl enable gitlab-runsvdir
systemctl start gitlab-runsvdir
```

5、访问：http://ip:port，顺利进入 gitlab 页面则算成功。



### 三、部署 Runner

注意：runner 机器建议和仓库代码分开部署，但实在资源紧张的话，配置到位，也可以和代码仓库部署在一台机子上，酌情考虑吧。

这里说一下安装这些软件（服务）的原因：

- git：runner 被触发后需要去拉取代码仓库的代码；
- maven：runner 运行过程中，需要使用 mvn 打包项目；
- jdk：maven 要用；

##### # 安装 JDK

```shell
# 下载安装包（等待数分钟。。。）
mkdir -p /usr/local/java
wget -O /usr/local/java/jdk.tar.gz https://repo.huaweicloud.com/java/jdk/8u202-b08/jdk-8u202-linux-x64.tar.gz --no-check-certificate
tar -zxvf /usr/local/java/jdk.tar.gz --strip-components=1
rm -f /usr/local/java/jdk.tar.gz

# 修改环境变量
vim /etc/profile
# ------------------------------------------------------------------------------#
export JAVA_HOME=/usr/local/java
export CLASSPATH=$CLASSPATH:$JAVA_HOME/lib
export PATH=$PATH:$JAVA_HOME/bin
# ------------------------------------------------------------------------------#
source /etc/profile

# 测试
java -version
javac -version
# ----------------------------------- OUTPUT -----------------------------------#
# java version "1.8.0_221"
# Java(TM) SE Runtime Environment (build 1.8.0_221-b11)
# Java HotSpot(TM) 64-Bit Server VM (build 25.221-b11, mixed mode)
# ------------------------------------------------------------------------------#
```



##### # 安装 Maven

1、下载并解压（版本选择：[https://archive.apache.org/dist/maven/maven-3](https://archive.apache.org/dist/maven/maven-3)）

```sh
mkdir -p /usr/local/maven
wget -O /usr/local/maven/maven.tar.gz https://archive.apache.org/dist/maven/maven-3/3.8.8/binaries/apache-maven-3.8.8-bin.tar.gz --no-check-certificate
tar -zxvf /usr/local/maven/maven.tar.gz --strip-components=1
rm -f /usr/local/maven/maven.tar.gz
```

2、创建仓库目录（需要给指定的目录相应权限，这里直接给最高权限，以免打包时出现权限不足的情况）

```shell
mkdir -p /opt/mvn-repository
chmod -R 777 /opt/mvn-repository
```

3、修改 maven 配置

```shell
vim /usr/local/maven/conf/settings.xml
```

4、更改 jar 本地仓库存放目录

```xml
<localRepository>/opt/mvn-repository</localRepository>
```

5、添加国内镜像仓库，这里使用阿里云的开放仓库。

```xml
<mirrors>
    <mirror>
        <id>alimaven</id>
        <name>aliyun maven</name>
        <url>http://maven.aliyun.com/nexus/content/groups/public</url>
        <mirrorOf>central</mirrorOf>
    </mirror>
</mirrors>
```

6、指定 jdk 版本，避免打包出现版本不一致导致编译异常的问题

```xml
<profile>
    <id>jdk-1.8</id>
    <activation>
        <activeByDefault>true</activeByDefault>
        <jdk>1.8</jdk>
    </activation>
    <properties>
        <maven.compiler.source>1.8</maven.compiler.source>
        <maven.compiler.target>1.8</maven.compiler.target>
        <maven.compiler.compilerVersion>1.8</maven.compiler.compilerVersion>
    </properties>
</profile>
```

7、配置系统环境变量

```shell
vim /etc/profile
# ------------------------------------------------------------------------------#
export MVN_HOME=/usr/local/maven
export PATH=$PATH:$MVN_HOME/bin
# ------------------------------------------------------------------------------#
source /etc/profile
```

8、测试

```shell
mvn -v
# ----------------------------------- OUTPUT -----------------------------------#
# Apache Maven 3.8.7 (b89d5959fcde851dcb1c8946a785a163f14e1e29)
# Maven home: /usr/local/maven/apache-maven-3.8.7
# Java version: 1.8.0_221, vendor: Oracle Corporation, runtime: /usr/local/java/jdk1.8.0_221/jre
# Default locale: zh_CN, platform encoding: UTF-8
# OS name: "linux", version: "3.10.0-1160.81.1.el7.x86_64", arch: "amd64", family: "unix"
# ------------------------------------------------------------------------------#
```



##### # 安装 Git

1、最简单的安装方式，直接使用 `yum` 命令安装（但是可能无法安装指定版本的，yum 库中貌似版本很低）：

```shell
yum install -y git
```

2、源码安装

```shell
# 安装依赖
yum install -y gcc-c++ zlib-devel perl-ExtUtils-MakeMaker
# 否则可能遇到以下错误：
# make[1]: *** [perl.mak] Error 2
# make: *** [perl/perl.mak] Error 2

# 下载安装包（等待数分钟。。。）
mkdir -p /usr/local/git
wget -O /usr/local/git/git.tar.gz https://www.kernel.org/pub/software/scm/git/git-2.9.0.tar.gz --no-check-certificate

# 如果上面的下载地址太慢，可以试试这个，估计两者区别不是很大。。。
wget -O /usr/local/git/git.tar.gz https://mirrors.edge.kernel.org/pub/software/scm/git/git-2.9.0.tar.gz --no-check-certificate

tar -zxvf /usr/local/git/git.tar.gz --strip-components=1
rm -f /usr/local/git/git.tar.gz
cd /usr/local/git
./configure
make && make install
```

3、测试

```shell
git --version
# ----------------------------------- OUTPUT -----------------------------------#
# git version 1.8.3.1
# ------------------------------------------------------------------------------#
```



##### # 安装 Gitlab-runner

Runner 执行的大致流程图如下：

![流程图](https://p3-juejin.byteimg.com/tos-cn-i-k3u1fbpfcp/b001c9c1b2154be5b35a5b201a895521~tplv-k3u1fbpfcp-zoom-in-crop-mark:4536:0:0:0.awebp)

1、下载程序包*（参考地址：[官方参考地址](https://docs.gitlab.com/runner/install/)）*

```shell
# Linux x86-64（大部分应该是这个）
mkdir -p /usr/local/gitlab-runner
wget -O /usr/local/gitlab-runner/runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64

# Linux x86
mkdir -p /usr/local/gitlab-runner
wget -O /usr/local/gitlab-runner/runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-386

# Linux arm
mkdir -p /usr/local/gitlab-runner
wget -O /usr/local/gitlab-runner/runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-arm
```

2、给程序包添加执行权限，避免出现权限的情况

```shell
chmod +x /usr/local/gitlab-runner/runner
```

3、添加软链接，方便直接使用命令

```shell
ln -s -f /usr/local/gitlab-runner/runner /usr/bin/gitlab-runner
```

4、添加一个 gitlab-runner 用户*（任意目录执行都可以）*

```shell
useradd --comment 'GitLab Runner' -a -G root --create-home gitlab-runner --shell /bin/bash

# groups gitlab-runner
# usermod -a -G root gitlab-runner
# groups gitlab-runner
```

5、安装并启动服务

```shell
gitlab-runner install --user=gitlab-runner --working-directory=/opt/gitlab-runner
gitlab-runner start
```

6、服务设置为开机自启动

```shell
systemctl enable gitlab-runner
```

7、测试

```shell
gitlab-runner -v

# ------------------------------- OUTPUT ---------------------------------------#
# Version:      13.8.0
# Git revision: 775dd39d
# Git branch:   13-8-stable
# GO version:   go1.13.8
# Built:        2021-01-20T13:32:47+0000
# OS/Arch:      linux/amd64
# ------------------------------------------------------------------------------#
```

8、基础命令

```sh
gitlab-runner list				# 查看注册的 runner 列表
gitlab-runner status			# 查看注 gitlab-runner 运行状态
gitlab-runner start				# 开启服务
gitlab-runner stop				# 停止服务
gitlab-runner restart			# 重启服务
gitlab-runner register			# 注册 runner
gitlab-runner unregister --all-runners
gitlab-runner verify --delete	# 删除无效 runner
```

9、部分参考命令

```shell
COMMANDS:
   exec                  execute a build locally
   list                  list all configured runners
   run                   run multi runner service
   register              register a new runner
   reset-token           reset a runner's token
   install               install service
   uninstall             uninstall service
   start                 start service
   stop                  stop service
   restart               restart service
   status                get status of a service
   run-single            start single runner
   unregister            unregister specific runner
   verify                verify all registered runners
   artifacts-downloader  download and extract build artifacts (internal)
   artifacts-uploader    create and upload build artifacts (internal)
   cache-archiver        create and upload cache artifacts (internal)
   cache-extractor       download and extract cache artifacts (internal)
   cache-init            changed permissions for cache paths (internal)
   health-check          check health for a specific address
   help, h               Shows a list of commands or help for one command
```





### 四、配置 gitlab 与 runner

##### 



### 五、CICD 语法解析

