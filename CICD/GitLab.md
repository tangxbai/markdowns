### 一、废话简介

GitLab 是一个强大的版本控制和协作平台，用于管理代码仓库、项目、问题跟踪、持续集成和部署等软件开发任务。下面是一个详细的GitLab 使用教程，包括创建项目、添加成员、管理代码、问题跟踪和持续集成等方面的基本步骤。



CI/CD 是一种通过在应用开发阶段引入**自动化**来频繁向客户交付应用的方法。

CI/CD 的核心概念是 **持续集成、持续交付和持续部署**。它是作为一个面向开发和运营团队的解决方案，主要针对在集成新代码时所引发的问题（也称为：“集成地狱”）。

CI/CD 可让持续自动化和持续监控贯穿于应用的整个生命周期（从集成和测试阶段，到交付和部署）。



### 二、部署代码仓库

小提示：代码仓库建议单独部署一个机子，保证代码的稳定和安全性。

阿里云镜像地址：[https://mirrors.aliyun.com/gitlab-ce/yum/el7](https://mirrors.aliyun.com/gitlab-ce/yum/el7)

清华大学镜像地址：[https://mirrors.tuna.tsinghua.edu.cn/gitlab-ce/yum/el7](https://mirrors.tuna.tsinghua.edu.cn/gitlab-ce/yum/el7)

1、系统环境准备

```shell
# 安装依赖
# postfix: 用于发送邮件
# openssh-server: 提供 ssh 服务
# policycoreutils-python: ssh 的依赖
yum install -y curl openssh policycoreutils-python openssh-server postfix

# 开启服务
systemctl enable sshd && systemctl start sshd
systemctl enable postfix && systemctl start postfix

# 配置系统防火墙
firewall-cmd --add-service=ssh --permanent	# 开放 ssh 服务端口，permanent 代表永久生效
firewall-cmd --add-service=http --permanent	# 开放 http 服务端口，permanent 代表永久生效
systemctl reload firewalld
systemctl restart firewalld

# 如果找不到防火墙则执行以下命令
# yum install -y firewalld systemd
```

2、下载程序并安装

```shell
mkdir -p /usr/local/gitlab
wget -O /usr/local/gitlab/gitlab-ce.rpm https://mirrors.aliyun.com/gitlab-ce/yum/el7/gitlab-ce-10.0.0-ce.0.el7.x86_64.rpm
rpm -i /usr/local/gitlab/gitlab-ce.rpm
rm -rf /usr/local/gitlab
```

3、更改配置

```shell
vim /etc/gitlab/gitlab.rb
#---------------------------------#
# exteral_url "http://ip:9000"
#---------------------------------#

# 更新防火墙（开放 gitlab 服务端口，即上面修改的 ip:port ）
firewall-cmd --zone=public --add-port=9000/tcp --permanent
firewall-cmd --zone=public --remove-port=5672/tcp --permanent
firewall-cmd --reload

# 装载配置
gitlab-ctl reconfigure 
gitlab-ctl restart
```

4、开机自启

```shell
systemctl enable gitlab-runsvdir
systemctl start gitlab-runsvdir
```

5、访问：http://ip:port，顺利进入 gitlab 页面则算成功，默认账号：root，没有密码，需要自己手动设置密码。



### 三、部署 Runner

注意：runner 机器建议和仓库代码分开部署，但实在资源紧张的话，配置到位也可以和代码仓库部署在一台机子上，但可能会造成机子卡顿，处理不过来的情况，需要酌情考虑。

这里说一下安装这些软件（服务）的原因：

- git：runner 被触发后需要去拉取代码仓库的代码；
- git-runner：用于处理 gitlab 的请求命令；
- maven：runner 运行过程中，需要使用 mvn 打包项目；
- jdk：maven 要用；

##### # 安装 JDK

```shell
# 下载安装包（等待数分钟。。。）
mkdir -p /usr/local/java
wget -O /usr/local/java/jdk.tar.gz https://repo.huaweicloud.com/java/jdk/8u202-b08/jdk-8u202-linux-x64.tar.gz --no-check-certificate
tar -zxvf /usr/local/java/jdk.tar.gz --strip-components=1
rm -f /usr/local/java/jdk.tar.gz

# 修改环境变量
vim /etc/profile
# ------------------------------------------------------------------------------#
export JAVA_HOME=/usr/local/java
export CLASSPATH=$CLASSPATH:$JAVA_HOME/lib
export PATH=$PATH:$JAVA_HOME/bin
# ------------------------------------------------------------------------------#
source /etc/profile

# 测试
java -version
javac -version
# ----------------------------------- OUTPUT -----------------------------------#
# java version "1.8.0_221"
# Java(TM) SE Runtime Environment (build 1.8.0_221-b11)
# Java HotSpot(TM) 64-Bit Server VM (build 25.221-b11, mixed mode)
# ------------------------------------------------------------------------------#
```



##### # 安装 Maven

1、下载并解压（版本选择：[https://archive.apache.org/dist/maven/maven-3](https://archive.apache.org/dist/maven/maven-3)）

```sh
mkdir -p /usr/local/maven
wget -O /usr/local/maven/maven.tar.gz https://archive.apache.org/dist/maven/maven-3/3.8.8/binaries/apache-maven-3.8.8-bin.tar.gz --no-check-certificate
tar -zxvf /usr/local/maven/maven.tar.gz --strip-components=1
rm -f /usr/local/maven/maven.tar.gz
```

2、创建仓库目录（需要给指定的目录相应权限，这里直接给最高权限，以免打包时出现权限不足的情况）

```shell
mkdir -p /opt/mvn-repository
chmod -R 777 /opt/mvn-repository
```

3、修改 maven 配置

```shell
vim /usr/local/maven/conf/settings.xml
```

4、更改 jar 本地仓库存放目录

```xml
<localRepository>/opt/mvn-repository</localRepository>
```

5、添加国内镜像仓库，这里使用阿里云的开放仓库。

```xml
<mirrors>
    <mirror>
        <id>alimaven</id>
        <name>aliyun maven</name>
        <url>http://maven.aliyun.com/nexus/content/groups/public</url>
        <mirrorOf>central</mirrorOf>
    </mirror>
</mirrors>
```

6、指定 jdk 版本，避免打包出现版本不一致导致编译异常的问题

```xml
<profile>
    <id>jdk-1.8</id>
    <activation>
        <activeByDefault>true</activeByDefault>
        <jdk>1.8</jdk>
    </activation>
    <properties>
        <maven.compiler.source>1.8</maven.compiler.source>
        <maven.compiler.target>1.8</maven.compiler.target>
        <maven.compiler.compilerVersion>1.8</maven.compiler.compilerVersion>
    </properties>
</profile>
```

7、配置系统环境变量

```shell
vim /etc/profile
# ------------------------------------------------------------------------------#
export MVN_HOME=/usr/local/maven
export PATH=$PATH:$MVN_HOME/bin
# ------------------------------------------------------------------------------#
source /etc/profile
```

8、测试

```shell
mvn -v
# ----------------------------------- OUTPUT -----------------------------------#
# Apache Maven 3.8.7 (b89d5959fcde851dcb1c8946a785a163f14e1e29)
# Maven home: /usr/local/maven/apache-maven-3.8.7
# Java version: 1.8.0_221, vendor: Oracle Corporation, runtime: /usr/local/java/jdk1.8.0_221/jre
# Default locale: zh_CN, platform encoding: UTF-8
# OS name: "linux", version: "3.10.0-1160.81.1.el7.x86_64", arch: "amd64", family: "unix"
# ------------------------------------------------------------------------------#
```



##### # 安装 Git

1、最简单的安装方式，直接使用 `yum` 命令安装（但是可能无法安装指定版本的，yum 库中貌似版本很低）：

```shell
yum install -y git
```

2、源码安装

```shell
# 安装依赖
yum install -y gcc-c++ curl-devel libcurl-devel zlib-devel perl-ExtUtils-MakeMaker
# 否则可能遇到以下错误：
# make[1]: *** [perl.mak] Error 2
# make: *** [perl/perl.mak] Error 2

# 下载安装包（等待数分钟。。。）
mkdir -p /usr/local/git
wget -O /usr/local/git/git.tar.gz https://www.kernel.org/pub/software/scm/git/git-2.9.0.tar.gz --no-check-certificate

# 如果上面的下载地址太慢，可以试试这个，估计两者区别不是很大。。。
wget -O /usr/local/git/git.tar.gz https://mirrors.edge.kernel.org/pub/software/scm/git/git-2.9.0.tar.gz --no-check-certificate

tar -zxvf /usr/local/git/git.tar.gz --strip-components=1
rm -f /usr/local/git/git.tar.gz
cd /usr/local/git
./configure
make && make install
```

3、测试

```shell
git --version
# ----------------------------------- OUTPUT -----------------------------------#
# git version 1.8.3.1
# ------------------------------------------------------------------------------#
```



##### # 安装 Gitlab-runner

Runner 执行的大致流程图如下（这是偷来的图，请不要介意）：

![流程图](https://p3-juejin.byteimg.com/tos-cn-i-k3u1fbpfcp/b001c9c1b2154be5b35a5b201a895521~tplv-k3u1fbpfcp-zoom-in-crop-mark:4536:0:0:0.awebp)

1、下载程序包*（参考地址：[官方参考地址](https://docs.gitlab.com/runner/install/)）*

```shell
# Linux x86-64（大部分应该是这个）
mkdir -p /usr/local/gitlab-runner
wget -O /usr/local/gitlab-runner/runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64 --no-check-certificate

# Linux x86
mkdir -p /usr/local/gitlab-runner
wget -O /usr/local/gitlab-runner/runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-386 --no-check-certificate

# Linux arm
mkdir -p /usr/local/gitlab-runner
wget -O /usr/local/gitlab-runner/runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-arm --no-check-certificate
```

2、给程序包添加执行权限，避免出现权限的情况

```shell
chmod +x /usr/local/gitlab-runner/runner
```

3、添加软链接，方便直接使用命令

```shell
ln -s -f /usr/local/gitlab-runner/runner /usr/bin/gitlab-runner
```

4、添加一个 gitlab-runner 用户*（任意目录执行都可以）*

```shell
useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash
groups gitlab-runner
usermod -a -G root gitlab-runner

# 查看是否添加成功
getent passwd
```

5、安装并启动服务

```shell
mkdir -p /opt/gitlab-runner
# 直接用 root 吧，创建的用户容易权限不足
gitlab-runner install --user=gitlab-runner --working-directory=/opt/gitlab-runner
gitlab-runner start
```

6、服务设置为开机自启动

```shell
systemctl enable gitlab-runner
systemctl status gitlab-runner
```

7、测试

```shell
gitlab-runner -v

# ------------------------------- OUTPUT ---------------------------------------#
# Version:      13.8.0
# Git revision: 775dd39d
# Git branch:   13-8-stable
# GO version:   go1.13.8
# Built:        2021-01-20T13:32:47+0000
# OS/Arch:      linux/amd64
# ------------------------------------------------------------------------------#
```

8、基础命令

```sh
gitlab-runner list				# 查看注册的 runner 列表
gitlab-runner status			# 查看注 gitlab-runner 运行状态
gitlab-runner start				# 开启服务
gitlab-runner stop				# 停止服务
gitlab-runner restart			# 重启服务
gitlab-runner register			# 注册 runner
gitlab-runner unregister --all-runners
gitlab-runner verify --delete	# 删除无效 runner
```

9、部分参考命令

```shell
COMMANDS:
   exec                  execute a build locally
   list                  list all configured runners
   run                   run multi runner service
   register              register a new runner
   reset-token           reset a runner's token
   install               install service
   uninstall             uninstall service
   start                 start service
   stop                  stop service
   restart               restart service
   status                get status of a service
   run-single            start single runner
   unregister            unregister specific runner
   verify                verify all registered runners
   artifacts-downloader  download and extract build artifacts (internal)
   artifacts-uploader    create and upload build artifacts (internal)
   cache-archiver        create and upload cache artifacts (internal)
   cache-extractor       download and extract cache artifacts (internal)
   cache-init            changed permissions for cache paths (internal)
   health-check          check health for a specific address
   help, h               Shows a list of commands or help for one command
```





### 四、配置 Runner

1、访问你搭建好的 gitlab 网页，去到以下位置：` {project}/Settings/CICD/Runners settings`，点击 `Expand` 打开 runners 配置，找到 `Specific Runners` 下面的配置信息，看起来大致如下：

------

- Install a Runner compatible with GitLab CI (checkout the GitLab Runner section for information on how to install it).

- Specify the following URL during the Runner setup: 

  `http://192.168.110.50:9000/`  - <font color="green">这个是访问URL，注册 runner 会用到</font>

- Use the following registration token during setup: 

  `Vi8Jx-QT-vzouPaXgSy-` - <font color="green">这个是权限token，注册 runner 会用到</font>

- Start the Runner!

------

2、注册 gitlab-runner

```shell
gitlab-runner register

# ----------------------------------- OUTPUT -----------------------------------#
# Runtime platform         arch=amd64 os=linux pid=97097 revision=6d480948 version=15.7.1
# Running in system-mode.                            
#                                                  
# Enter the GitLab instance URL (for example, https://gitlab.com/):
>> http://192.168.110.50:9000/ # 上面的URL
#
# Enter the registration token:
>> Vi8Jx-QT-vzouPaXgSy-		  # 上面的Token
#
# Enter a description for the runner:
[master]: >> spring-runner	  # Runner 的名字
#
# Enter tags for the runner (comma-separated):
>> dev,deploy,prod			  # Runner 标签，.gitlab-ci.yaml 中会用到
# 
# Enter optional maintenance note for the runner:
>> spring-runner             # 起一个名字，每个项目不能相同
#
# Registering runner... succeeded                     runner=Vi8Jx-QT
# Enter an executor: docker-ssh, shell, ssh, docker+machine, instance, custom, docker, 
# docker-ssh+machine, kubernetes, parallels, virtualbox:
>> shell	                 # shell 或者 docker
#
# Runner registered successfully. Feel free to start it, but if it's running already the # config should be automatically reloaded!
#
# Configuration (with the authentication token) was saved in "/etc/gitlab-runner/config.toml"
# ------------------------------------------------------------------------------#
```

3、启动 runner，通常注册 runner 后会自动启动该 runner，如果未自启动，那么使用以下命令启动它：

```shell
gitlab-runner run
```

3、删除 runner

```shell
# 删除指定名字的 runner，同名删除第一个
gitlab-runner unregister --name <your-runner>

# 删除同时满足 url 和 token 的 runner
gitlab-runner unregister --url <https://ip:port> --token <gitlab-token>

# 删除所有无效 runner
gitlab-runner verify --delete
```

4、查看注册结果

```shell
gitlab-runner list

# ----------------------------------- OUTPUT -----------------------------------#
# Runtime platform        arch=amd64 os=linux pid=20766 revision=6d480948 version=15.7.1
# Listing configured runners ConfigFile=/etc/gitlab-runner/config.toml
# spring-runner              Executor=shell Token=<gitlab-token> URL=http://<ip:port>/
# ------------------------------------------------------------------------------#
```



### 五、脚本语法解析

其实 gitlab 中 ci/cd 的核心就是这个 `.gitlab-ci.yaml` 脚本文件了，当我们本地代码提交到服务端仓库时，gitlab 准备就绪后会去查找项目根目录下是否包含这个文件，如果有，则去解析脚本内容并将内容交给配置的 runner 执行，并将脚本执行结果在 `Pipelines` 中展示出来，每一次操作都会有相应的记录。

1、语法结构*（脚本内容主要采用 `yaml` 文件格式来描述脚本内容）*

```yaml
# @导入配置
# include 在一个文件可以出现多次
# include 引入的文件如果变量相同会进行覆盖，不会多次执行
# include 的写法很灵活，后面是 loacl 和 remote 可以直接省略
# include 可以使用定义好的变量
# ---------------------------------------------------
# local：表示引入当前项目的文件
# remote：yml 文件的仓库必须是 public 的，不然就报错，这就代表着可以将 yml 文件单独存放在一个仓库，通过引用获取
# project：表示引用当前用户可访问的项目的 yml 文件
# template：表示可以使用 Gitlab 定义好的 yml 文件，官方的公用模版
include: <string|array-string>
include: "/path/to/your/template.yml" # (URL 和本地文件路径都可以直接写)
include:
  - remote: "http://your/url/tmeplate.yml"
  - file: "/path/to/your/template.yml"
  - local: "/path/to/your/template.yml"
  - template: "/path/to/your/template.yml"
  - ...

# 使用 Docker 镜像
image: <namespace>/<image>:<tag>

# @流程节点
# 不定义的话默认 [ build | test | deploy ] 三个流程节点
stages:
  - <the-stage-name>
  - <the-stage-name>
  - ...

# @定义变量
# 符合变量命名即可，需要避开语法关键字，值可以使用其他变量值，也可以使用内置变量，但是嵌套使用会有问题
# KEY: <VALUE>
# KEY: $CI_JOB_ID-$CI_PROJECT_NAME-<VALUE>
variables:
  <KEY>: <VALUE>
  <KEY>: <VALUE>
  ...

# @触发流程
# workflow 决定何时触发 Pipeline  或者禁止 Pipeline 
# if 结合预定义变量判断条件为真，则触发 pipeline 或禁止
workflow:
  rules:
    - if: <condition>
      changes: "指定文件发生变化"
      exists: "指定文件存在"
      variables:
        <KEY>: <VALUE>
        <KEY>: <VALUE>
        ...
      when: "never" | "always" | "manual"
    - when: always

# @缓存配置
# 用来指定需要在 job 之间缓存的文件或目录，只能使用该项目工作空间内的路径
cache:
  key: <cache-key>
  files:
     - <the-change-file>
     - <the-change-file>
  paths: 
    - <target-dir-path>
    - <target-dir-path>
    - ...
  policy: "pull" | "pull-push" | "push"
  unprotect: <booelan>
  untracked: <booelan>

# @前置脚本
# 用于在每个 job 脚本执行前执行特定的逻辑，可以是 shell 脚本文件路劲。
before_script:
  - <shell>
  - <shell>
  - ...

# @后置脚本
# 用于在每个 job 脚本执行后执行特定的逻辑，可以是 shell 脚本文件路劲。
after_script:
  - <shell>
  - <shell>
  - ...
 
# @模板配置
# 其他地方可以通过 “<<: *link” 的方式导入配置，这是 yaml 的语法，不是 .gitlab-ci.yml 的语法
.template: @<link>
  <var>: <value>
  <var>: <value>
  ...

# @核心任务
# 每个项目的 .gitlab-ci.yml 中至少需要有一个Job
# 每个项目通过定义配置字段实现不同的功能，如单元测试，静态检查，构建镜像等
# 一个任务中，除了 script，其他都是非必需的
# 这些变量很多可以直接声明在全局中，如果任务中有重名，会进行覆盖
# 对于临时不想执行的 job，可以在定义前面加个英文句号 [.]，这样可以跳过此 job。
[.]<your-job-name>:
  stage: <the-stage-name>
  tags: [ <gitlab-runner-tags>, ... ]
  
  # 导入模板配置，相当于直接替换此处的语法
  <<: *link
  
  # 继承其他配置信息
  extends: <public-job-configuration>
  
  # Docker 配置
  image: <docker-image>
  services:
    - name: <docker-image-name>
      alias: <image-alias>
      entrypoint: [ <shell>, ... ]
      command: [ <shell>, ... ]
      
  # 同全局变量含义一样，申明在每个 job 中会覆盖全局配置
  cache: <same-as-root>
  rules: <same-as-root>
  variables: <same-as-root>
  before_script: <same-as-root>
  after_script: <same-as-root>
  
  # 当前 job 构建失败是否继续整个流程
  # 默认失败终止整个构建流程，改为 false 则当前节点失败也继续后面流程
  allow_failure: [ true|false ]
  artifacts: 
  timeout: <timeout(Number)>
  parallel: <job-number(Number)>
  trigger: <other-ci-pipeline>
  environment: 
   - <your-key>: <your-value>
   - <your-key>: <your-value>
   - ...
  script: 
    - <shell>
    - <shell>
    - ...
  only|except:
    - branches: "当一个分支被 push 上来"
    - tags: "当一个打了 tag 的分支被push上来"
    - api: "当被 piplines api 所触发调起，详见 piplines api"
    - external: "当使用了 GitLab 以外的 CI 服务"
    - pipelines: "针对多项目触发器而言，当使用CI_JOB_TOKEN并使用gitlab所提供的api创建多个pipelines的时候"
    - pushes: "当pipeline被用户的git push操作所触发的时候"
    - schedules: "针对预定好的pipline而言（每日构建一类）"
    - triggers: "用token创建piplines的时候"
    - web: "在GitLab页面上Pipelines标签页下，你按了run pipline的时候"
  retry:
    max: <retry-times(Number)>
    when:
      - always: "在发生任何故障时重试（默认）"
      - unknown_failure: "当失败原因未知时"
      - script_failure: "脚本失败时重试"
      - api_failure: "API失败重试"
      - stuck_or_timeout_failure: "作业卡住或超时时"
      - runner_system_failure: "运行系统发生故障"
      - missing_dependency_failure: "如果依赖丢失"
      - runner_unsupported: "Runner不受支持"
      - stale_schedule: "无法执行延迟的作业"
      - job_execution_timeout: "脚本超出了为作业设置的最大执行时间"
      - archived_failure: "作业已存档且无法运行"
      - unmet_prerequisites: "作业未能完成先决条件任务"
      - scheduler_failure: "调度程序未能将作业分配给运行scheduler_failure"
      - data_integrity_failure: "检测到结构完整性问题"
  
  # 触发时机
  when:
    - on_success: "当前一阶段的所有作业运行成功后，才会运行该作业【默认】"
    - on_failure: "当前面有一个作业运行失败才会运行该作业 "
    - always: "不管前面作业的状态如何，都会运行"
    - never: "绝不运行该作业"
    - delayed: "延迟运行该作业"
    - manual: "手动触发作业"
```

变量注意事项：

- 变量有优先级，如果相同会被覆盖；
- 变量定义的地方很多，包括全局，jobs中，scripts、gitlab项 目或组的变量页面（页面常常用来配置安全性较高的密钥），都可以在配置文件中直接使用；
- gitlab 每次迭代会增加不同的变量，在使用一些不常用的变量时，要注意当前版本是否支持；
- 变量在 scripts 中使用：`$CI_BUILDS_DIR`；
- 内置变量：[官方变量文档](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html)；



### 六、GitLab Api

gitlab 并没有提供 UI 界面直接给我们操作这些数据，可能是觉得不安全或者其他原因，暂时不探讨这个问题，不过 gitlab 提供了 api 的方式来供我们使用，操作需要用到 `access token` 值，你可以到 `User Settings/Access Tokens` 中生成一个访问令牌，这个令牌生成后一定妥善保存，因为它没有提供查看的地方，遗失的话只有重新生成一个新的。

比如：查看 token 名下拥有的所有项目列表（其他更多的就是不说了，这个不是很重要）：

```shell
curl --header "PRIVATE-TOKEN:<your-access-token>" http://127.0.0.1:9000/api/v4/projects
```