## 自动化部署实操流程 - 微服务版本



本文自动化部署方案：centos + gitlab + maven + git + gitlab-runner + docker + k8s + rancher;


> 提醒，建议所有都用 root 账号登录，否则以下命令最好全部加上 `sudo` 关键字，不然容易出现权限相关问题。



### 一、服务器配置

- 192.168.110.10：docker( gitlab )
- 192.168.110.11：docker( gitlab-runner + git + maven + jdk )
- 192.168.110.12：docker( mysql + redis )
- ...
- 192.168.110.100：docker( node )
- 192.168.110.101：docker( node )
- ...
