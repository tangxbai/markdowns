### 一、相关简介

Docker 是一个开源的应用容器引擎，基于 Go 语言并遵从 Apache2.0 协议开源。Docker 可以让开发者打包他们的应用以及依赖包到一个轻量级、可移植的容器中，然后发布到任何流行的 Linux 机器上，也可以实现虚拟化。

容器是完全使用沙箱机制的，相互之间不会有任何接口（类似 iphone 的 app），更重要的是容器性能开销极低。Docker 从 17.03 版本之后分为 CE（Community Edition：社区版） 和 EE（Enterprise Edition：企业版），一般我们用社区版就可以了。



### 二、基础概念

- 镜像（Image）：docker 的镜像就相当于是一个 root 文件系统。比如官方镜像 ubuntu:16.04 就包含了完整的一套 ubuntu16.04 最小系统的 root 文件系统。
- 容器（Container）：镜像（image）和容器（container）的关系，就像是面向对象程序设计中的类和实例一样，镜像是静态的定义，容器是镜像运行时的实体。容器可以被创建、启动、停止、删除、暂停等。
- 仓库（Repository）：仓库可看成一个代码控制中心，用来保存镜像。



### 三、相关地址

- 官方仓库：https://hub.docker.com
- 菜鸟教程：https://www.runoob.com/docker/docker-tutorial.html
- 官方安装：https://docs.docker.com/engine/install/centos



### 四、卸载 Docker

```sh
# 第一种方式：删除清单中安装过的所有组件
$ yum remove docker \
  docker-client \
  docker-client-latest \
  docker-common \
  docker-latest \
  docker-latest-logrotate \
  docker-logrotate \
  docker-selinux \
  docker-engine-selinux \
  docker-engine
                
# 第二种方式，使用匹配符直接删除全部
# 推荐使用第二种方式，无需罗列所有的安装组件
$ yum remove docker docker-*

# 删除注册的服务
$ rm -rf /etc/systemd/system/docker.service.d

# 删除 docker 工作目录
$ rm -rf /var/lib/docker*
$ rm -rf /var/run/docker*

# 删除配置文件
# 直接清空这个文件夹，下面所有配置都将被清空
$ rm -rf /etc/docker
#----------------------------#
# /etc/docker/daemon.json    #
# /ect/docker/key.json       #
#----------------------------#

# 查找残留的 docker
$ rpm -qa|grep docker
```



### 五、安装 Docker

```sh
# 第一步，需要安装一些依赖组件
# 安装所需的软件包。yum-utils 提供了 yum-config-manager，
# 并且 device mapper 存储驱动程序需要 device-mapper-persistent-data 和 lvm2。
$ yum install -y yum-utils device-mapper-persistent-data lvm2

# 使用以下命令来获得稳定的仓库
# 阿里云源地址速度相对比较快，这里是设置源地址为阿里云的镜像仓库地址。
$ yum-config-manager --add-repo http://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo

# 使用以下命令查看镜像仓库
# yum repolist all      # 显示所有资源库
# yum repolist enabled  # 显示所有已启动的资源库
# yum repolist disabled # 显示所有被禁用的资源库

# 安装 Docker Engine-Community
$ yum install -y docker-ce docker-ce-cli containerd.io

# 将当前用户添加到 docker 用户组
$ usermod -aG docker ${USER}

# docker 默认未启动，需要手动重启 docker 服务
$ systemctl restart docker
$ systemctl start docker
$ systemctl stop docker

# 切换当前会话到 docker 组
$ newgrp - docker
```



### 六、镜像加速

```shell
# 查看当前 docker 镜像源
$ docker info|grep Mirrors -A 1

# 添加镜像源
$ sudo mkdir -p /etc/docker
$ sudo tee /etc/docker/daemon.json <<-'EOF'
{
    "registry-mirrors": [
        "https://dockerproxy.com",
        "https://docker.mirrors.ustc.edu.cn",
        "https://docker.nju.edu.cn"
    ]
}
EOF

# 如果已经添加过镜像源，直接修改内容即可
$ vim /etc/docker/daemon.json
$ ...

# 重新载入配置信息
$ sudo systemctl daemon-reload
$ sudo systemctl restart docker
```



### 七、基础命令

```shell
# 检测信息
$ docker info
$ docker version

# 登录/登出仓库（如果未指明仓库地址，默认官方仓库）
$ docker login -u <user> -p <pwd> <?server>
$ docker logout <?server>

# 查看容器中运行的进程信息
$ docker top <container>
# 阻塞运行直到容器停止，然后打印出它的退出代码
$ docker wait <container> 
# 列出指定的容器的端口映射
$ docker port <container>
# 暂停/恢复容器进程
$ docker pause/unpause <container...>

# 杀掉一个运行中的容器
$ docker kill <container...>
$ docker kill -s [SIGKILL|SIGTERM|KILL] <container...> # -s：以指定信号量发送给容器

# 【镜像】保存/载入归档文件
$ docker save <image> > output.tar # 完整备份，包含所有信息
$ docker load < image.tar          # 原封不动的导入到 docker
# 【容器】导出/导入归档文件
$ docker export <container> > output.tar # 将容器导出为基础镜像
$ docker import image.tar <image>        # 导入基础镜像，仅创建基础镜像，不包含元数据等；
# 提交镜像
$ docker commit <container> <image

# 拷贝资源（dest 目录必须要添加一个斜杠，否则会被重命名为文件夹最后一层的名字）
# docker cp [OPTIONS] CONTAINER:SRC_PATH DEST_PATH|-
# docker cp [OPTIONS] SRC_PATH|- CONTAINER:DEST_PATH
$ docker cp /file/path/to/host <container>:/path/to/container/

# 从仓库搜索镜像
$ docker search <image>
$ docker search -f <conditions...> <image>

# 从镜像仓库中拉取或者更新指定镜像
$ docker pull <image>    # 拉取最新版本的镜像
$ docker pull -a <image> # 拉取指定镜像的不区分 tag 的所有镜像
$ docker pull -a <image --disable-content-trust # 忽略镜像的校验，默认开启

# 将本地的镜像上传到镜像仓库（提示：如果没有事先登录 docker 仓库会推送到默认仓库）
$ docker push <image>
$ docker push <image> --disable-content-trust # 忽略镜像的校验，默认开启

# 启动/停止/重启容器
$ docker start/stop/restart <container...>

# 以命令行的方式进入容器内部
$ docker exec -it <container> /bin/bash # 新开一个终端窗口进入容器，exit 命令不会停止运行中的容器；
$ docker attach <container> # 进入容器终端窗口，exit 命令会停止容器，如果想不停止容器退出可以使用 【Ctrl+P+Q】；

# 列出容器
$ docker ps       # 仅列出所有运行中的容器
$ docker ps -a    # 列出所有状态下的容器
$ docker ps -a -q # 列出所有容器的 ID
# docker ps [OPTIONS: {
# -a, --all      **     Show all containers (default shows just running)
# -q, --quiet    **     Only display numeric IDs
# -f, --filter filter   Filter output based on conditions provided
# -n, --last int        Show n last created containers (includes all states) (default -1)
# -l, --latest          Show the latest created container (includes all states)
# -s, --size            Display total file sizes
#     --format string   Pretty-print containers using a Go template
#     --no-trunc        Don't truncate output
# }]

# 列出本地镜像
$ docker images       # 列出本地 “可用” 镜像列表
$ docker images -a    # 列出本地 “所有” 镜像列表
$ docker images -a -q # 列出本地所有镜像，并只显示镜像 ID；
# docker images [OPTIONS: {
# -a, --all     **      Show all images (default hides intermediate images)
# -q, --quiet   **      Only show numeric IDs
# -f, --filter filter   Filter output based on conditions provided
#     --digests         Show digests
#     --format string   Pretty-print images using a Go template
#     --no-trunc        Don't truncate output
# }] [REPOSITORY[:TAG]]

# 删除本地一个或多个镜像
$ docker rmi <image...>
$ docker rmi -f <image...>
$ docker rmi -f --no-prune <image...> # 不移除该镜像的过程镜像，默认移除；

# 删除一个或多个容器
$ docker rm <container...>    # 普通删除
$ docker rm -f <container...> # 强制删除
# docker rm [OPTIONS: {
# -f, --force     Force the removal of a running container (uses SIGKILL)
# -l, --link      Remove the specified link
# -v, --volumes   Remove anonymous volumes associated with the container
# }] CONTAINER [CONTAINER...]

# 创建/运行容器
$ docker run/create -itd --name <image-name> <image> /bin/bash
$ docker run/create -itd --name <image-name> -p 8000:9000 -v /data/app:/data/app <image> /bin/bash
# docker run/create [OPTIONS: {
#     --name string     **           Assign a name to the container
# -i, --interactive     **           Keep STDIN open even if not attached (With: "-t/-d")
# -t, --tty             **           Allocate a pseudo-TTY (With: "-i/-d")
# -d, --detach          **           Run container in background and print container ID (With: "-i/-t")
# -p, --publish list    **           Publish a container's port(s) to the host (<host-port>:<port>)
# -P, --publish-all     **           Publish all exposed ports to random ports
# -h, --hostname string **           Container host name
# -v, --volume list     **           Bind mount a volume (</path/to/host>:</path/to/container>)
# -e, --env list                     Set environment variables (<key>=<value>)
#     --env-file list                Read in a file of environment variables
# -l, --label list                   Set meta data on a container
# -w, --workdir string               Working directory inside the container
#     --restart string               Restart policy to apply when a container exits (on-failure/on-failure:n/always/unless-stopped, default "no")
#     --entrypoint string            Overwrite the default ENTRYPOINT of the image
#     --privileged                   Give extended privileges to this container
# -----------------------------------------------------------------------------------------------------
#     --add-host list                Add a custom host-to-IP mapping (host:ip)
# -a, --attach list                  Attach to STDIN, STDOUT or STDERR
#     --blkio-weight uint16          Block IO (relative weight), between 10 and 1000, or 0 to disable (default 0)
#     --blkio-weight-device list     Block IO weight (relative device weight) (default [])
#     --cap-add list                 Add Linux capabilities
#     --cap-drop list                Drop Linux capabilities
#     --cgroup-parent string         Optional parent cgroup for the container
#     --cidfile string               Write the container ID to the file
#     --cpu-period int               Limit CPU CFS (Completely Fair Scheduler) period
#     --cpu-quota int                Limit CPU CFS (Completely Fair Scheduler) quota
#     --cpu-rt-period int            Limit CPU real-time period in microseconds
#     --cpu-rt-runtime int           Limit CPU real-time runtime in microseconds
# -c, --cpu-shares int               CPU shares (relative weight)
#     --cpus decimal                 Number of CPUs
#     --cpuset-cpus string           CPUs in which to allow execution (0-3, 0,1)
#     --cpuset-mems string           MEMs in which to allow execution (0-3, 0,1)
#     --detach-keys string           Override the key sequence for detaching a container
#     --device list                  Add a host device to the container
#     --device-cgroup-rule list      Add a rule to the cgroup allowed devices list
#     --device-read-bps list         Limit read rate (bytes per second) from a device (default [])
#     --device-read-iops list        Limit read rate (IO per second) from a device (default [])
#     --device-write-bps list        Limit write rate (bytes per second) to a device (default [])
#     --device-write-iops list       Limit write rate (IO per second) to a device (default [])
#     --disable-content-trust        Skip image verification (default true)
#     --dns list                     Set custom DNS servers
#     --dns-option list              Set DNS options
#     --dns-search list              Set custom DNS search domains
#     --domainname string            Container NIS domain name
#     --expose list                  Expose a port or a range of ports
#     --gpus gpu-request             GPU devices to add to the container ('all' to pass all GPUs)
#     --group-add list               Add additional groups to join
#     --health-cmd string            Command to run to check health
#     --health-interval duration     Time between running the check (ms|s|m|h) (default 0s)
#     --health-retries int           Consecutive failures needed to report unhealthy
#     --health-start-period duration Start period for the container to initialize before starting health-retries countdown (ms|s|m|h) (default 0s)
#     --health-timeout duration      Maximum time to allow one check to run (ms|s|m|h) (default 0s)
#     --init                         Run an init inside the container that forwards signals and reaps processes
#     --ip string                    IPv4 address (e.g., 172.30.100.104)
#     --ip6 string                   IPv6 address (e.g., 2001:db8::33)
#     --ipc string                   IPC mode to use
#     --isolation string             Container isolation technology
#     --kernel-memory bytes          Kernel memory limit
#     --label-file list              Read in a line delimited file of labels
#     --link list                    Add link to another container
#     --link-local-ip list           Container IPv4/IPv6 link-local addresses
#     --log-driver string            Logging driver for the container
#     --log-opt list                 Log driver options
#     --mac-address string           Container MAC address (e.g., 92:d0:c6:0a:29:33)
# -m, --memory bytes                 Memory limit
#     --memory-reservation bytes     Memory soft limit
#     --memory-swap bytes            Swap limit equal to memory plus swap: '-1' to enable unlimited swap
#     --memory-swappiness int        Tune container memory swappiness (0 to 100) (default -1)
#     --mount mount                  Attach a filesystem mount to the container
#     --network network              Connect a container to a network (bridge/host/none/container)
#     --network-alias list           Add network-scoped alias for the container
#     --no-healthcheck               Disable any container-specified HEALTHCHECK
#     --oom-kill-disable             Disable OOM Killer
#     --oom-score-adj int            Tune host's OOM preferences (-1000 to 1000)
#     --pid string                   PID namespace to use
#     --pids-limit int               Tune container pids limit (set -1 for unlimited)
#     --read-only                    Mount the container's root filesystem as read only
#     --rm                           Automatically remove the container when it exits
#     --runtime string               Runtime to use for this container
#     --security-opt list            Security Options
#     --shm-size bytes               Size of /dev/shm
#     --sig-proxy                    Proxy received signals to the process (default true)
#     --stop-signal string           Signal to stop a container (default "SIGTERM")
#     --stop-timeout int             Timeout (in seconds) to stop a container
#     --storage-opt list             Storage driver options for the container
#     --sysctl map                   Sysctl options (default map[])
#     --tmpfs list                   Mount a tmpfs directory
#     --ulimit ulimit                Ulimit options (default [])
# -u, --user string                  Username or UID (format: <name|uid>[:<group|gid>])
#     --userns string                User namespace to use
#     --uts string                   UTS namespace to use
#     --volume-driver string         Optional volume driver for the container
#     --volumes-from list            Mount volumes from the specified container(s)
# }] IMAGE [COMMAND] [ARG...]

# 标记本地镜像，将其归入某一仓库
# docker tag SOURCE_IMAGE[:TAG] TARGET_IMAGE[:TAG]
$ docker tag centos:15.10 xbai/centos:v1

# 通过 Dockerfile 创建镜像
$ docker build .                 # 使用当前目录的 Dockerfile 构建镜像
$ docker build -t <image> .      # 使用当前目录的 Dockerfile 构建镜像并指定镜像标签
$ docker build -f <dockerfile> . # 使用指定 Dockerfile 构建镜像
$ docker build <url>             # 使用 URL 指向的 Dockerfile 构建镜像
# docker build [OPTIONS: {
# -f, --file string **          Name of the Dockerfile (Default is 'PATH/Dockerfile')
# -t, --tag list    **          Name and optionally a tag in the 'name:tag' format
# -q, --quiet       **          Suppress the build output and print image ID on success
#     --add-host list           Add a custom host-to-IP mapping (host:ip)
#     --build-arg list          Set build-time variables  (key=value)
#     --cache-from strings      Images to consider as cache sources
#     --cgroup-parent string    Optional parent cgroup for the container
#     --compress                Compress the build context using gzip
#     --cpu-period int          Limit the CPU CFS (Completely Fair Scheduler) period
#     --cpu-quota int           Limit the CPU CFS (Completely Fair Scheduler) quota
# -c, --cpu-shares int          CPU shares (relative weight)
#     --cpuset-cpus string      CPUs in which to allow execution (0-3, 0,1)
#     --cpuset-mems string      MEMs in which to allow execution (0-3, 0,1)
#     --disable-content-trust   Skip image verification (default true)
#     --force-rm                Always remove intermediate containers
#     --iidfile string          Write the image ID to the file
#     --isolation string        Container isolation technology
#     --label list              Set metadata for an image
# -m, --memory bytes            Memory limit
#     --memory-swap bytes       Swap limit equal to memory plus swap: '-1' to enable unlimited swap
#     --network string          Set the networking mode for the RUN instructions during build (default "default")
#     --no-cache                Do not use cache when building the image
#     --pull                    Always attempt to pull a newer version of the image
#     --rm                      Remove intermediate containers after a successful build (default true)
#     --security-opt strings    Security options
#     --shm-size bytes          Size of /dev/shm, default is '64M'.
#     --target string           Set the target build stage to build.
#     --ulimit ulimit           Ulimit options (default [])
# }] PATH | URL | -

# 查看指定镜像的创建历史
$ docker history <image>
# docker docker history [OPTIONS: {
# -q, --quiet           Only show numeric IDs
# -H, --human           Print sizes and dates in human readable format (default true)
#     --format string   Pretty-print images using a Go template
#     --no-trunc        Don't truncate output
# }] IMAGE

# 获取容器/镜像的元数据
$ docker inspect <image>/<container>
$ docker inspect -f '{{EXPRESSION}}' <image>/<container>
$ docker inspect -f '{{.NetworkSettings.IPAddress}}' <container> # 查看运行中的容器的 IP 地址
# docker inspect [OPTIONS: {
# -f, --format string   Format the output using the given Go template
# -s, --size            Display total file sizes if the type is container
#     --type string     Return JSON for specified type
# }] NAME|ID [NAME|ID...]-

# 获取容器的实时事件
$ docker events --since="1467302400"
# docker events [OPTIONS: {
# -f, --filter filter   Filter output based on conditions provided
#     --format string   Format the output using the given Go template
#     --since string    Show all events created since timestamp
#     --until string    Stream events until this timestamp
# }]

# 获取容器的日志
$ docker logs -f <container> # 跟踪日志输出
$ docker logs --since="2024-03-12" --tail=10 <container> # 查看从指定日期后的最新10条日志
# docker logs [OPTIONS: {
# -f, --follow       **   Follow log output
# -t, --timestamps   ** Show timestamps
#     --tail string  ** Number of lines to show from the end of the logs (default "all")
#     --since string ** Show logs since timestamp (e.g. 2013-01-02T13:23:37) or relative (e.g. 42m for 42 minutes)
#     --details         Show extra details provided to logs
#     --until string    Show logs before a timestamp (e.g. 2013-01-02T13:23:37) or relative (e.g. 42m for 42 minutes)
# }] CONTAINER

# 显示容器资源的使用情况，包括：CPU、内存、网络 I/O 等
$ docker stats -a
$ docker stats <container> --no-stream --format "{{json .}}"
# docker stats [OPTIONS:{
# -a, --all             Show all containers (default shows just running)
#     --format string   Pretty-print images using a Go template
#     --no-stream       Disable streaming stats and only pull the first result
#     --no-trunc        Do not truncate output
# }] [CONTAINER...]
```



### 八、批量操作

```sh
# 容器相关
$ docker start/stop/restart $(docker ps -aq) # 批量启动/停止/重启所有容器
$ docker kill $(docker ps -q)   # 杀死所有运行中的容器
$ docker rm -f $(docker ps -aq -f status=exited) # 删除所有退出状态的容器
$ docker rm -f $(docker ps -aq) # 删除所有未运行的容器1（运行中的容器无法被删除，相当于所有未运行的容器）；
$ docker container prune -f     # 删除所有未运行的容器2（需要 Docker >= 1.13）；

# 镜像相关
$ docker rmi -f <image>             # 强制删除指定的镜像
$ docker rmi -f $(docker images -q) # 删除所有镜像
$ docker rmi $(docker images -q -f dangling=true) # 删除所有未打 dangling 标签的镜
$ docker image prune -f  # 删除所有未被 tag 标记的镜像（Docker >= 1.13）；
$ docker image prune -af # 删除所有未被容器使用的镜像（Docker >= 1.13）；

# 删除所有未被挂在的卷
# WARNING! This will remove all local volumes not used by at least one container.
$ docker volume prune -f

# 删除所有网络
# WARNING! This will remove all networks not used by at least one container.
$ docker network prune -f

# 删除所有未使用的资源（未运行的镜像也会被删除！）
$ docker system prune -f
# WARNING! This will remove:
#   - all stopped containers
#   - all networks not used by at least one container
#   - all dangling images
#   - all dangling build cache
```
