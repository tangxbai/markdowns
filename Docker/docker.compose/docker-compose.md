### 一、基础介绍

Compose 是用于定义和运行多容器 Docker 应用程序的工具。通过 Compose，你可以使用 YML 文件来配置应用程序需要的所有服务。然后，使用一个命令就可以从 YML 文件配置中创建并启动所有服务。

Compose 使用的三个步骤：

- 使用 Dockerfile 定义应用程序的环境；
- 使用 "docker-compose.yml" 定义构成应用程序的服务，这样它们可以在隔离环境中一起运行；
- 最后，执行  `docker-compose up`  命令来启动并运行整个应用程序；



### 二、安装插件

```shell
# 1) 下载 docker-compose 插件
# 由于 github 具有不稳定性，可能导致下载过于缓慢，所以可以提前使用其他工具下载好，然后复制到目标主机即可；
$ sudo curl -L "https://github.com/docker/compose/releases/download/1.24.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose

# 1) 如果提前下载好了 docker-compose 二进制文件，直接上传到服务器相关目录即可；
$ cp docker-compose /usr/local/bin/docker-compose

# 2) 增加文件可执行权限
$ sudo chmod +x /usr/local/bin/docker-compose

# 3) 创建软连接，这样可以直接在任何地方使用 docker-compose 命令
$ sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose

# 4) 测试是否安装成功
$ docker-compose --version

# 5) 卸载
$ rm -f /usr/local/bin/docker-compose
```



### 三、常用命令

*注意*：docker-compose.yaml 必须和命令在同一目录，否则将无法执行 docker-compose 相关命令，这里仅罗列常用的一些命令，更多命令及参数请使用 `docker-compose --help` 和 `docker-compose <command> --help` 查询。


```sh
$ docker-compose --help                          # 查看 docker-compose 的所有信息
$ docker-compose <command> --help                # 查看某个具体命令的完整信息

$ docker-compose config -q                       # 验证文件配置，当配置正确时不输出任何内容，否则输出错误信息；

$ docker-compose build <service> ...             # 构建镜像 
$ docker-compose build --no-cache <service> ...  # 不带缓存的构建

$ docker-compose up -d                           # 构建服务并后台启动所有服务
$ docker-compose -f docker-compose.yaml up -d    # 指定编排文件构建服务并后台启动所有服务

$ docker-compose images                          # 列出 Compose 文件包含所有镜像
$ docker-compose ps                              # 列出 Compose 管理的所有容器列表，同 docker ps；

$ docker-compose down -v                         # 此命令将会停止 up 命令所启动的容器、网络等
$ docker-compose exec <service> bash             # 进入到容器中，同 docker exec；
$ docker-compose run -it <service> <command>     # 在容器中执行一个命令
$ docker-compose rm <service>                    # 删除停止的容器（如果删前未停止，可以使用 -f 强制删除）

# docker-compose scale <contaner>=<num>...       # 扩容（已过时）
$ docker-compose up --scale <service>=<num>...   # 扩容（后续版本替代用法）

$ docker-compose stop <service> ...              # 停止容器
$ docker-compose start <service> ...             # 启动容器
$ docker-compose restart <service>               # 重新启动容器
$ docker-compose pause <service>                 # 暂停容器
$ docker-compose unpause <service>               # 恢复容器
$ docker-compose kill <service>                  # 在容器中执行一个命令

$ docker-compose top                             # 查看各个服务容器内运行的进程 
$ docker-compose logs -f <service>               # 查看服务的实时日志
$ docker-compose port <service>                  # 查看服务使用的端口信息
$ docker-compose events --json <service>         # 以 json 的形式输出 nginx 的 docker 日志
```



### 四、指令解析

##### 1）基础结构

- version：必须指定，并且位于文件的首行，定义文件格式的版本（指定以什么版本的 API 来解析文件内容）；
- services：编排要运行的应用服务；
- networks：指定 Docker  的网络模式；
- volumes：配置数据挂载卷；



##### 2）环境变量

Compose 模板文件支持动态读取主机的 **系统环境变量** 和当前目录下的 `.env` 文件中的变量。格式：${ENV_VAR_NAME:-DEFAULT}；

支持 shell 环境变量：

```shell
$ export YOUR_VAR_1=VAR_VALUE
$ export YOUR_VAR_2=VAR_VALUE
$ export NGINX_VERSION=1.20
```

从 .env 文件中读取变量（如果当前目录下存在 .env 的文件，构建的时候会自动载入这些配置信息）：

```properties
YOUR_VAR_1=VAR_VALUE
YOUR_VAR_2=VAR_VALUE
YOUR_VAR_3=VAR_VALUE
NGINX_VERSION=1.20
```

使用方式：

```yaml
version: '3'
services:
  image: 'nginx:${NGINX_VERSION:-1.10}'
  environment:
    YOUR_VAR_1: ${YOUR_VAR_1:-DEFAULT}
    YOUR_VAR_2: ${YOUR_VAR_2:-DEFAULT}
```



### 五、语法一览

```yaml
version: '3'
services:
  <define-your-service-name>:
  
    # 默认情况下从仓库拉取此镜像
    image: <define-your-image>
    
    # 容器名
    container_name: <container>
    
    # 通过 Dockerfile 构建镜像，如果此配置不存在，则直接通过 image 拉取镜像；
    # 当 build 和 image 同时存在时，将使用 image 为 build 打包的镜像命名；
    build: /your/path/of/dockerfile/dir # 默认写法
    
    # 也可以指定 Dockerfile
    # build:
    #   context: <context-path>
    #   dockerfile: /your/path/of/dockerfile
    #   ...
    
    # 指定容器中工作目录
    working_dir: /your/working/dir
    
    # 重启模式
    # 该命令对保持服务始终运行十分有效，在生产环境中推荐配置为 always 或者 unless-stopped；
    restart: always | on-failure | unless-stopped | no
    
    # 覆盖容器启动后默认执行的命令
    command: executable param ... | [ "executable", "param", ...]
    
    # 配置端口映射，与 expose 不同的是，此属性真实的会映射具体的端口；
    ports: [ "<host-port>:<container-port>", "<host>:<container>", ... ]
    
     # 暴露端口，但不映射到宿主机
    # 不要写数字的形式，可能会被解析成 16 进制，最好用字符串；
    expose: [ "<port>", "<port>", ... ]
    
    # 解决容器依赖问题，在构建此服务前先启动其他服务
    depends_on: [ "other-service", "other-service", ... ]
    
    # 为容器添加 Docker 元数据（metadata）信息
    labels:
      <key>: <value>
      <key>: <value>
      
    # 创建挂载卷
    # host:container | host:container:ro -> ro: readonly
    volumes: [ "/the/host/dir:/the/container/dir", "/the/host/dir:/the/container/dir:ro", ... ]
    
    # 从另一个服务活动容器挂载数据卷
    volumes_from: [ "<server-name>", "<server-name>", ... ]
    
    # 覆盖 Dockerfile 中的 ENTRYPOINT 定义
    entrypoint: executeable param ... | [ "executeable", "param", ... ] | /sheel.sh
    
    # 环境变量
    environment:
      <KEY>: <VALUE>
      <KEY>: <VALUE>
      ...
    # 在 docker-compose.yml 中可以定义一个专门存放变量的文件；
    # 如果通过 docker-compose -f FILE 指定配置文件，则 env_file 中路径会使用配置文件路径；
    # 如果有变量名称与 environment 指令冲突，则以后者为准。
    env_file: ".env" | [ "your/env/name.env", "your/env/name.env", ... ]
    
    # 健康检查
    # 状态一栏 State 会显示 “Up (healthy)”
    healthcheck:
      test: curl --fail http://localhost:8080 || exit 1 # string|array
      interval: 10s # 格式：_h_m_s，默认为 30s；
      timeout: 10s  # 格式：_h_m_s，默认为 30s；
      retries: 3    # 数字，默认为 3；
      
    # 附加主机配置，用于配置 hosts 文件，内部可直接通过对应的主机名访问对应的机器；
    extra_hosts: [ "<server-name>:<ip>", "<server-name>:<ip>", ... ]
    
    # 自定义 DNS 列表
    dns: [ "<ip>", "<ip>", ... ]
    
    # 配置 DNS 搜索域
    dns_search: [ "<domain>", "<domain>", ... ]
    
    # 设置 pid 模式
    # 跟主机系统共享进程命名空间。打开该选项的容器之间，以及容器和宿主机系统之间可以通过进程 ID 来相互访问和操作。
    pid: "<model>"
     
    # 修改容器的内核能力
    cap_add: [ "ALL", ... ]
    cap_drop: [ "NET_ADMIN", ... ]
    cgroup_parent: "<group>"
    
    # 指定设备映射关系
    devices: [ "your/host/device:your/container/device", ... ]
    
    # 基于其它模板文件进行扩展，相当于 JAVA 中的子类继承父类（但是继承有限制，推荐仅继承一些公用信息）；
    extends:
      file: your-docker-compose-common.yml
      service: <your-service-name>
    
    # 链接到 docker-compose.yml 外部的容器，可以是非 compose 管理的外部容器
    external_links: [ "<non-docker-compose-container>", .... ]
    
    # 链接到其它服务中的容器）（注意：不推荐使用该指令）
    links: [ "<service-name>", "<service-name>", ... ]
    
    # 指定日志驱动类型
    log_driver: "json-file | syslog | none"
    log_opt: 
      syslog-address: "<URL>"
    
    # 一些状态开关
    tty: <boolean>        # 开启一个伪终端
    read_only: <boolean>  # 以只读的方式运行容器（可能会导致一些写操作异常）；
    stdin_open: <boolean> # 打开标准输入
    privileged: <boolean> # 允许容器中运行一些特权命令
    
    # 系统级别参数
    domainname: <domain>
    hostname: <server-name>
    mac_address: <mac-address>
    user: "<user>" | "<user>:<group>" | "<uid>:<gid>"
    
    # 指定容器模板标签（label）机制的默认属性（用户、角色、类型、级别等）
    security_opt: [ "<label>:<user>:<role>", ... ]
    
    # 配置容器连接的网络
    network_mode: "bridge" | "host" | "none"
    networks: [ "network-a", "network-b", ... ]
    
    # 外部配置
    configs:
      - source: <config-alias-name>
        target: /your/container/location/path
    
    # 敏感信息配置
    secrets:
      - source: <secret-alias-name>
        target: /your/container/location/path
    
    # 部署信息配置
    deploy:
      replicas: 3
      resources:
        limits:
          cpus: '0.5'
          memory: '256M'
        reservations:
          cpus: '0.25'
          memory: '128M'

# 配置网络，服务中可以直接使用
networks:
  network-a:
    driver: "bridge" | "host" | "none"
  network-b:
    driver: "bridge" | "host" | "none"

# 一些配置文件
configs:
  <config-alias-name>:
    file: /your/config/file

# 一些安全性的文件
secrets:
  <secret-alias-name>:
    file: /your/secret/file
```