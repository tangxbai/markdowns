# 镜像源
# 使用上面打包的那个作为基础镜像，可以加速打包过程；
FROM openjdk:8

# 维护人信息
MAINTAINER tangxbai@hotmail.com
LABEL maintainer="tangxbai@hotmail.com"

# 设置环境变量
# 可使用 -e key=value ... 修改变量值
ENV PROFILE="dev" SERVER_PORT=8800 WORKDIR="/home/jar"
ENV JAVA_OTPS="\
-Djava.security.egd=file:/dev/./urandom \
-Dspring.profile.active=$PROFILE \
-Duser.timezone=${TIMEZONE:-Asia/Shanghai} \
-Dserver.port=$SERVER_PORT"

# 切换工作目录
WORKDIR $WORKDIR

# 将 jar 拷贝到镜像包中
COPY app.jar .

# 健康检查
# --interval：设置健康检查的间隔时间，默认为 30 秒；
# --timeout：设置健康检查命令的超时时间，默认为 30 秒；
# --start-period：设置容器启动后的等待时间，等待一定时间后开始进行健康检查，默认为 0 秒；
# --retries：设置健康检查失败后重试的次数，默认为 3 次；
HEALTHCHECK --interval=5s --timeout=3s CMD curl -f http://localhost:$SERVER_PORT || exit 1

# 申明暴露出去的端口，如果没有特别配置映射端口，那么此端口会被当做默认端口映射到宿主机
# 可以通过 docker run-p 80:8800 更改端口映射
EXPOSE $SERVER_PORT

# 执行 docker run 的默认命令
# EXEC 模式：这种方式无法获取变量值，会被当成普通值执行，导致程序无法启动！
# ENTRYPOINT [ java", "-jar", "$JAVA_OTPS", "$JAR" ]
# SHELL 模式：可以正常解析变量参数值
ENTRYPOINT java -jar $JAVA_OTPS app.jar