### 一、基础概念

Dockerfile 是一个包含用于组合镜像的命令的文本文档，可以使用在命令行中调用任何命令， Docker 通过读取文件中的指令自动生成镜像。



### 二、指令解析

Docker 以从上到下的顺序运行 Dockerfile 的指令。为了指定基本镜像，第一条指令必须是 **FROM**。一个声明以 `＃` 字符开头则被视为注释，"&&" 符号用以连接多条命令，这样执行后只会创建 1 层镜像；

- **FROM** [ image:tag ]：指定基础镜像，用于后续的指令构建；
- ~~MAINTAINER：指定 Dockerfile的 作者/维护者（已弃用，推荐使用 LABEL 指令）~~；
- **RUN** [ "executable", "param", ...] ：构建镜像过程中执行的命令，每一次RUN都会构建一层，换行符 `\`；
- **VOLUME**：定义数据挂载卷，如果没有定义则使用默认（*避免重要数据存储在容器中，尽量挂在到宿主机上*）；
- USER：指定后续执行的用户组和用户*（用户和用户组必须提前创建好）*；
- **WORKDIR**：指定当前执行的工作目录，如不存在会自动创建*（多次构建都会使用此目录）*；
- HEALTHCHECH：健康检测指令；
- ARG  [ key=value ]：定义构建过程中的临时环境变量（*仅在构建阶段生效，可用 --build-arg 覆盖）*；
- ENV  [ key=value ]：设置环境变量（容器内部也会起作用）；
- LABEL [ key=value ]：添加元数据；
- **EXPOSE** [ port/protocol ]：声明容器中使用的网络端口*（仅仅是声明）*；
- **ADD**：将文件/目录/URL复制到镜像中*（提示：压缩文件以解压的形式复制到容器内部，如果仅是复制的话建议用 `COPY` ）；*
- COPY：将文件/目录复制到镜像中（文件仅复制不会解压）；
- ONBUILD：当该镜像被用作另一个构建过程的基础时被触发；
- STOPSIGNAL：容器退出时发送给系统的调用信号；
- HEALTHCHECK：定义容器健康状态的命令（多次以最后一个为准）；
- SHELL：覆盖 Docker 中默认的 shell，用于 RUN、CMD 和 ENTRYPOINT 指令；
- **CMD** [ "executable", "param", ... ]：容器启动时执行的命令，如果有多个则以最后一个为准，并且会被 `docker run` 后面的参数覆盖，也可以为 ENTRYPOINT 提供参数（**多次以最后一个为准**）；
- **ENTRYPOINT**：容器进入时执行的命令（不会被覆盖，除非使用 `--entrypoint` 指令）（**多次以最后一个为准**）；



### 三、指令一览

注意：指令中的每一行都会创建一层镜像，如果指令过多，创建后的镜像会逐渐庞大，废层镜像也会越来越多，这里建议减少不必要的行，可以写成一行的尽量写在一行处理，比如：RUN / VOLUME / ENV / ADD / COPY / ENTRYPOINT / CMD 等等；

```dockerfile
# 指定基础镜像，用于后续的指令构建，必须位于第一行！
FROM <image>
FROM <image>:<tag>

# 指定 Dockerfile 的作者/维护者（已弃用，推荐使用 LABEL 指令）
# LABEL author email@abc.com
MAINTAINER <author>

# 指定后续执行的用户组和用户（用户和用户组必须提前创建好）
USER <user>
USER <user>:<group>

# 指定当前执行的工作目录，如不存会自动创建（多次构建都会使用此目录）；
# 容器运行期间也会基于此目录，"docker exec ..." 默认也会进入此目录；
# ADD / COPY 复制文件或目录时如果基于此目录可以直接用 “." 表示；
WORKDIR /data/server

# 覆盖 Dockerfile 中默认的 shell 模式，用于 RUN、CMD 和 ENTRYPOINT 指令；
SHELL executable param ...   
SHELL [ "executable", "param", ...]

# 构建镜像过程中执行的命令，每一次 RUN 都会构建一层，换行符 `\`；
# 此命令最容易造成镜像过大，多条件命令可以使用 ”&&“ 进行链接，以减小镜像大小；
RUN executable param ...   
RUN [ "executable", "param", ...]

# 定义数据挂载卷，如果没有定义则使用默认（避免重要数据存储在容器中，尽量挂在到宿主机上）；
VOLUME /host/data/dir /container/data/dir

# 定义构建过程中的临时环境变量（仅在构建阶段生效，可用 --build-arg 覆盖）；
# 此命令不支持多个同时书写，如果出现多个只能有多少写多少行！
ARG <KEY>=<VALUE>

# 设置环境变量（容器内部也会起作用，相当于也给容器设置了一个系统环境变量）；
# 多个推荐使用空格隔开，如果不方便阅读，可以使用反斜杠 '\' 换行处理，也是可以的；
ENV <KEY> <VALUE>
ENV <KEY>=<VALUE> <KEY>=<VALUE> ...

# 添加元数据
# 多个推荐使用空格隔开，如果不方便阅读，可以使用反斜杠 '\' 换行处理，也是可以的；
LABEL <KEY>=<VALUE> <KEY>=<VALUE> ...

# 将 "文件/目录/URL" 复制到镜像中（提示：压缩文件会以解压的形式复制到容器内部，如果仅是复制的话建议用 COPY ）；
ADD /host/file /container/dir
ADD /host/compress.tar.gz /container/dir

# 将 "文件/目录/URL" 复制到镜像中（仅复制）
COPY /host/file /container/dir

# 声明容器中使用的网络端口（仅仅是声明）
EXPOSE <port> <port> ...

# 当镜像被用作另一个构建过程的基础时被触发
ONBUILD executable param ...   
ONBUILD [ "executable", "param", ...]

# 容器退出时发送给系统的调用信号，只要是符合底层系统的停止信号均可；
STOPSIGNAL <SIGNAL>

# 健康检查命令，容器状态列会展示：Up( healthy )
# --interval：设置健康检查的间隔时间，默认为 30 秒；
# --timeout：设置健康检查命令的超时时间，默认为 30 秒；
# --start-period：设置容器启动后的等待时间，等待一定时间后开始进行健康检查，默认为 0 秒；
# --retries：设置健康检查失败后重试的次数，默认为 3 次；
HEALTHCHECK --interval=5s --timeout=3s CMD curl -f http://localhost || exit 1

# 容器进入时执行的命令（不会被覆盖，除非使用 `--entrypoint` 指令）（多次以最后一个为准）；
ENTRYPOINT executable param ...   
ENTRYPOINT [ "executable", "param", ...]

# 容器启动时执行的命令，如果有多个则以最后一个为准，并且会被 `docker run` 后面的参数覆盖；
# 也可以为 ENTRYPOINT 提供参数（多次以最后一个为准）
CMD executable param ...   
CMD [ "executable", "param", ...]
```



### 四、完整脚本示例

构建自己的基础镜像（jdk + 必要的环境配置 + 其他）：

```dockerfile
# 镜像源
# 稳定小巧精悍的镜像（~100M）,不过仅包含基础的 jdk 环境，无其他附属组件（如：无 curl，需要校正时区等问题）；
FROM openjdk:8-jdk-alpine

# 维护人信息
# 过时了，推荐使用 LABEL 的方式申明，虽然 MAINTAINER 已过时，但是会添加 Author 属性值，可以两个都留下；
MAINTAINER tangxbai@hotmail.com
LABEL maintainer="tangxbai@hotmail.com"

# 设置环境变量，其他基于此镜像构建的应用都可以直接使用这些环境变量；
# 可使用 -e key=value ... 修改变量值
ENV TIMEZONE="Asia/Shanghai" 

# 安装必要软件并设置时区（默认 openjdk 镜像包时区不是 +8，需要在打包时设置时区）
RUN apk add curl tzdata && cp /usr/share/zoneinfo/$TIMEZONE /etc/localtime && echo "$TIMEZONE" > /etc/timezone && apk del tzdata && rm -rf /var/cache/apk/*
```

构建基础镜像（假设上面文件名为：“Dockerfile.openjdk8”）：

```shell
$ docker build -t openjdk:8 -f Dockerfile.openjdk8 .
```

构建基础应用镜像：

```dockerfile
# 镜像源
# 使用上面打包的那个作为基础镜像，可以加速打包过程；
FROM openjdk:8

# 维护人信息
MAINTAINER tangxbai@hotmail.com
LABEL maintainer="tangxbai@hotmail.com"

# 设置环境变量
# 可使用 -e key=value ... 修改变量值
ENV PROFILE="dev" SERVER_PORT=8800 WORKDIR="/home/jar"
ENV JAVA_OTPS="\
-Djava.security.egd=file:/dev/./urandom \
-Dspring.profile.active=$PROFILE \
-Duser.timezone=${TIMEZONE:-Asia/Shanghai} \
-Dserver.port=$SERVER_PORT"

# 切换工作目录，如不存在会自动创建，无序提前创建；
WORKDIR $WORKDIR

# 将 jar 拷贝到镜像包中
# app.jar 位于当前目录下，“.” 代表基于 WORKDIR 的相对目录；
COPY app.jar .

# 健康检查
# --interval：设置健康检查的间隔时间，默认为 30 秒；
# --timeout：设置健康检查命令的超时时间，默认为 30 秒；
# --start-period：设置容器启动后的等待时间，等待一定时间后开始进行健康检查，默认为 0 秒；
# --retries：设置健康检查失败后重试的次数，默认为 3 次；
HEALTHCHECK --interval=5s --timeout=3s CMD curl -f http://localhost:$SERVER_PORT || exit 1

# 申明暴露出去的端口，如果没有特别配置映射端口，那么此端口会被当做默认端口映射到宿主机
# 可以通过 docker run-p 80:8800 更改端口映射
EXPOSE $SERVER_PORT

# 执行 docker run 的默认命令
# EXEC 模式：这种方式无法获取变量值，会被当成普通值执行，导致程序无法启动！
# ENTRYPOINT [ java", "-jar", "$JAVA_OTPS", "$JAR" ]
# SHELL 模式：可以正常解析变量参数值
ENTRYPOINT java -jar $JAVA_OTPS app.jar
```



### 五、构建命令

以下示例，通过目录下的 Dockerfile 构建一个指定标签的镜像。

```sh
docker build -t <image>:<tag> .                          # 默认使用 Dockerfile
docker build -f /path/to/a/dockerfile -t <image>:<tag> . # 使用其他文件名的 Dockerfile
```

命令最后一个 “.” 指的是上下文路径，是指 Docker 在构建镜像，有时候想要使用到本机的文件（比如复制），docker build 命令得知这个路径后，会将路径下的所有内容打包。

**解析**：由于 Docker 的运行模式是 C/S。我们本机是 C，Docker 引擎是 S。实际的构建过程是在 Docker 引擎下完成的，所以这个时候无法用到我们本机的文件。这就需要把我们本机的指定目录下的文件一起打包提供给 Docker 引擎使用。

**注意**：上下文路径下不要放无用的文件，因为会一起打包发送给 Docker 引擎，如果文件过多会造成过程缓慢。



### 六、.dockerignore

**.dockerignore** 是一个描述类的文本文件，用于排除构建上下文中不需要的文件或文件夹，这与 “.gitignore” 很相似。

.dockerignore 文件同样需要放在构建上下文的根目录下，在 **Docker Client** 发送构建请求之前，会先检查这个文件是否存在，如果该文件存在，就会解析这个文件，然后从构建上下文中去除多余的文件资源，然后将构建上下文中剩下的文件发送到 **Docker daemon** 中。这样做不会减小最后生成的镜像大小，但是可以加快 Docker 打包镜像的时间，因为发送给 Docker 的文件体积变小了，那么传输过程就会变得快一些，也避免占用不必要的带宽。

以下是部分内容示例（针对 JAVA 项目结构）：

```dockerfile
# --------------------------------------------------------- #
# 以 # 开头的行是备注，不会被解析为匹配规则                        #
# 支持 ? 通配符，匹配单个字符                                   #
# 支持 * 通配符，匹配多个字符，只能匹配单级目录                     #
# 支持 ** 通配符，可匹配多级目录                                 #
# 支持 ! 匹配符，声明某些文件资源不需要被排除                       #
# --------------------------------------------------------- #
# 资源文件
src
bin
doc
shell

# 打包生成的本地资源
target/classes
target/generated-sources
target/maven-archiver
target/test-classes
target/*.jar.original

# Git
.git
.gitignore

# Idea
*.iml
*.idea

# Eclipse
.settings
.eclipse
.project
.classpath
.myeclipse

# Resource
*.md
*.yml
*.properties

# License
LICENSE
NOTICE

# maven
pom.xml
```

