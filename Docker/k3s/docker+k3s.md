## Docker + K3s + Rancher



### 一、固定 IP

```shell
# 设置静态IP
vim /etc/sysconfig/network-scripts/ifcfg-ens33
# ------------------------------------------------------------------------------#
BOOTPROTO="static"
IPADDR="192.168.x.x"
NETMASK="255.255.255.0"
GATEWAY="192.168.x.x"
DNS1="8.8.8.8
# ------------------------------------------------------------------------------#
# 重启网络
service network restart
```



### 二、部署 Docker

因为每一台机器都会依赖 docker，所以需要提前准备 docker 环境，国外资源很慢，这里直接添加阿里云的 docker-ce 镜像源

```shell
yum-config-manager --add-repo https://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo
```

开始安装

```shell
# 执行安装（依赖 + docker）
yum install -y yum-utils device-mapper-persistent-data lvm2 docker-ce

# 检测安装
docker version
```

服务默认并未开启，需要手动开启并设为自启动，不然重启之后不会自动运行，切记。

```shell
systemctl enable docker && systemctl start docker
```

快速操作命令

```shell
# 删除所有未运行的容器
docker rm $(docker ps -aq -f dangling=true)
# 删除所有退出状态的容器
docker rm $(docker ps -aq -f status=exited)
```



### 三、部署 Rancher

在所有机器上安装完了 docker 后，准备 rancher 调度机器，rancher 部署相对比较简单，按照以下脚本内容执行即可，没有需要特别注意的地方，如果重新部署 rancher，需要彻底清理 rancher 环境，可以参照官网脚本内容执行。

```shell
# 可以先拉取，也可以直接执行 docker run ...
docker pull rancher/rancher:v2.4.8

# 运行 rancher 镜像
# 与 2.4.x 或之前的版本相比，使用 docker run 命令安装 Rancher 2.5.x 时，需要添加 --privileged 标志变量，启用特权模式安装 Rancher。
docker run -d -p 8088:80 -p 8443:443 --name rancher-server --privileged --restart=unless-stopped -v /data/docker/rancher-server/var/lib/rancher/:/var/lib/rancher/ rancher/rancher:v2.4.8

# 运行阿里云镜像
docker run -d -p 8088:80 -p 8443:443 --name rancher-server --privileged --restart=unless-stopped -e CATTLE_AGENT_IMAGE="registry.cn-hangzhou.aliyuncs.com/rancher/rancher-agent:v2.4.8" <image-id>
```

开放服务端口，即上面修改的 ip:port

```shell
firewall-cmd --zone=public --add-port=8443/tcp --permanent
firewall-cmd --zone=public --add-port=8088/tcp --permanent
firewall-cmd --reload
```

密码相关操作

```shell
# 查看初始密码
docker logs <container-id> 2>&1 | grep "Bootstrap Password:"
# 重置密码
docker exec -it <container-id> reset-password
```

优雅的使用 Rancher，使用国内网站加速访问

```shell
# 系统设置
system-default-registry: registry.cn-hangzhou.aliyuncs.com

# 商店设置	
helm3-library: 	https://gitee.com/rancher/helm3-charts
library: 		https://gitee.com/rancher/charts
system-library:	https://gitee.com/rancher/system-charts
```

清理 rancher，参考官网：https://docs.rancher.cn/docs/rancher2/cluster-admin/cleaning-cluster-nodes/_index



### 四、部署 K3S

参考文档：

- 官方：https://docs.rancher.cn/docs/k3s/installation/_index
- 官方：https://docs.k3s.io/zh/
- 引用：https://www.cnblogs.com/ff111/p/15012854.html
- 引用：https://blog.bwcxtech.com/posts/61aaf06f/#%E6%9E%B6%E6%9E%84%E5%9B%BE

机器环境配置

```shell
# 关闭防火墙
systemctl stop firewalld && systemctl disable firewalld

# 更改主机名
hostnamectl --static set-hostname <name> && hostnamectl set-hostname <name>

# 配置主机名和 IP 映射
echo "<ip>   $(hostname)" >> /etc/hosts

# 关闭 SeLinux
setenforce 0
sed -i "s/SELINUX=enforcing/SELINUX=disabled/g" /etc/selinux/config

# 关闭 swap
swapoff -a
yes | cp /etc/fstab /etc/fstab_bak
cat /etc/fstab_bak |grep -v swap > /etc/fstab

# 修改 /etc/sysctl.conf
# 如果有配置，则修改，没有的话就追加
echo "net.ipv4.ip_forward = 1" >> /etc/sysctl.conf
echo "net.bridge.bridge-nf-call-ip6tables = 1" >> /etc/sysctl.conf
echo "net.bridge.bridge-nf-call-iptables = 1" >> /etc/sysctl.conf
echo "net.ipv6.conf.all.disable_ipv6 = 1" >> /etc/sysctl.conf
echo "net.ipv6.conf.default.disable_ipv6 = 1" >> /etc/sysctl.conf
echo "net.ipv6.conf.lo.disable_ipv6 = 1" >> /etc/sysctl.conf
echo "net.ipv6.conf.all.forwarding = 1"  >> /etc/sysctl.conf
# 执行命令以应用
sysctl -p
```



##### # 准备 K3s-server

一键安装*（亲测基本没什么问题，但是如果一直下载不下来，可以尝试手动安装）*

```shell
curl -sfL https://rancher-mirror.rancher.cn/k3s/k3s-install.sh | INSTALL_K3S_MIRROR=cn INSTALL_K3S_VERSION="v1.19.16+k3s1" INSTALL_K3S_EXEC="server --docker --disable traefik --node-taint k3s-controlplane=true:NoExecute" sh -s -

# 参数含义解释
# -------------------------------------------------------------
# server # 指定当前服务为 server 端
# --docker # 用 docker 代替 containerd
# --kube-proxy-arg value # 自定义 kube-proxy 进程的参数
# --write-kubeconfig value # 将管理客户端的 kubeconfig 写入这个文件
# --write-kubeconfig-mode # 用这种模式编写 kubeconfig，例如：644
# --node-taint value # 用一组污点注册 kubelet
# -------------------------------------------------------------
```

手动下载安装

```shell
# 文件下载（上传）存放位置
"k3s-airgap-images-amd64.tar" >> /var/lib/rancher/k3s/agent/images
"k3s" >> /usr/local/bin/k3s
"install.sh" >> /tmp/install.sh

# 下载程序包（鉴于下载可能会很慢，这几个文件可以先用迅雷提前下载好，然后上传至服务器）
wget -P /var/lib/rancher/k3s/agent/images https://github.com/k3s-io/k3s/releases/download/v1.19.16+k3s1/k3s-airgap-images-amd64.tar
wget -P /usr/local/bin/k3s https://github.com/k3s-io/k3s/releases/download/v1.19.16+k3s1/k3s
wget -O /tmp/install.sh https://get.k3s.io

# 赋予执行权限
chmod +x /tmp/install.sh
chmod +x /usr/local/bin/k3s
chmod +x /var/lib/rancher/k3s/agent/images/k3s-airgap-images-amd64.tar

# 执行安装
INSTALL_K3S_SKIP_DOWNLOAD=true INSTALL_K3S_MIRROR=cn INSTALL_K3S_EXEC="server --docker --node-taint k3s-controlplane=true:NoExecute" /tmp/install.sh

# 在 docker 中导入 k3s 镜像（rancher需要用到的镜像）
docker load -i /var/lib/rancher/k3s/agent/images/k3s-airgap-images-amd64.tar
```

系统操作命令

```shell
systemctl daemon-reload	# 重新加载新的 unit 配置
systemctl start k3s		# 启动服务
systemctl stop k3s		# 停止服务
systemctl restart k3s	# 重启服务
systemctl status k3s	# 查看服务状态
```

更改 Node 节点角色

```shell
# 添加角色（Server 节点上执行）
kubectl label node k3s-worker node-role.kubernetes.io/worker=worker
# Delete label
kubectl describe node k3s-worker |grep Labels
kubectl label nodes k3s-worker1 k3s-worker-
```

驱逐 Node

```shell
kubectl drain <node> --delete-local-data --ignore-daemonsets --force
```

卸载脚本*（安装之后才会生成此脚本）*

```shell
/usr/local/bin/k3s-uninstall.sh
```

查看 TOKEN

```shell
cat /var/lib/rancher/k3s/server/node-token
```

Node 安装后查看节点状态

```shell
kubectl get nodes -o wide
```



##### # 准备 K3s-worker

一键安装*（亲测基本没什么问题，但是如果一直下载不下来，可以尝试手动安装）*

```shell
# K3S_URL 参数会使 K3S 以 Worker 模式开始安装
curl -sfL https://rancher-mirror.rancher.cn/k3s/k3s-install.sh | INSTALL_K3S_MIRROR=cn INSTALL_K3S_VERSION="v1.19.16+k3s1" K3S_URL=https://<ip>:<port> K3S_TOKEN=<token> INSTALL_K3S_EXEC="--docker --disable traefik" sh -

curl -sfL https://rancher-mirror.rancher.cn/k3s/k3s-install.sh | INSTALL_K3S_MIRROR=cn INSTALL_K3S_VERSION="v1.19.16+k3s1" K3S_URL=https://192.168.110.70:6443 K3S_TOKEN=K100ecdb5c7806eb0d265c4f2056cad111de154abc197627d4da3cf439757a4b081::server:9c0e37c73a691400225e6e49f3f6883e INSTALL_K3S_EXEC="--docker" sh -
# --------------------------------------------------------------
# --docker # 用 docker 代替 containerd
# --kube-proxy-arg value # 自定义 kube-proxy 进程的参数
# --------------------------------------------------------------
```

手动下载安装

```shell
# 文件下载（上传）存放位置
"k3s-airgap-images-amd64.tar" >> /var/lib/rancher/k3s/agent/images
"k3s" >> /usr/local/bin/k3s
"install.sh" >> /tmp/install.sh

# 下载程序包（鉴于下载可能会很慢，这几个文件可以先用迅雷提前下载好，然后上传至服务器）
wget -P /var/lib/rancher/k3s/agent/images https://github.com/k3s-io/k3s/releases/download/v1.19.16+k3s1/k3s-airgap-images-amd64.tar
wget -P /usr/local/bin/k3s https://github.com/k3s-io/k3s/releases/download/v1.19.16+k3s1/k3s
wget -O /tmp/install.sh https://get.k3s.io

# 赋予权限
chmod 755 /tmp/install.sh
chmod 755 /usr/local/bin/k3s
chmod 755 /var/lib/rancher/k3s/agent/images/k3s-airgap-images-amd64.tar

# 执行安装
cd /tmp
INSTALL_K3S_MIRROR=cn K3S_URL=https://<ip>:<port> K3S_TOKEN=<token> INSTALL_K3S_VERSION="v1.19.16+k3s1" INSTALL_K3S_EXEC="--docker --kube-proxy-arg proxy-mode=ipvs" ./install.sh

# 在 docker 中导入 k3s 镜像（rancher需要用到的镜像）
docker load -i /var/lib/rancher/k3s/agent/images/k3s-airgap-images-amd64.tar
```

卸载脚本（安装之后才会生成此脚本）

```shell
/usr/local/bin/k3s-agent-uninstall.sh
```

错误解决方案

```shell
# 错误1
level=error msg="Node password rejected, duplicate hostname or contents of '/etc/rancher/node/password' may not match server nod
e-passwd entry, try enabling a unique node name with the --with-node-id flag"

# server
cat /var/lib/rancher/k3s/server/cred/node-passwd # 和 agent 中的密码对比是否一致
# agent
cat /etc/rancher/node/password # 确保 password 和 server 中对应的密码一致
hostname # 确保 hostname 唯一不重复

# 解决方式：
# 1）手动在 agent 上创建 password，内容和 server 中存储保持一致；
# 2）修改了 server 中的原始内容，让 password 和 agent 上新生成的保持一致；
# 3）可以试试 agent 注册时使用 --with-node-id，这样 server 中认为这完全是新 node；
```

