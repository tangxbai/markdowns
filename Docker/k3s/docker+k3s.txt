
rancher:
	0）系统资源先更新到最新（yum update -y）
	1）然后安装 docker，并设置开机自启
	2）拉取 rancher 镜像，启动并设置开机自启
	3）不能关闭防火墙，关闭防火墙 docker 可能无法启动 rancher
	4）防火墙开通 443、80 两个通信端口，可以更改为其他的
	5）导入 k3s 集群

k3s-server:
	0）系统资源先更新到最新（yum update -y）
	1）设置唯一主机名，必须要设置
	2）关闭 selinux
	3）关闭 swap
	4）修改 /etc/sysctl.conf
	5）更改 ip -> 主机名映射
	6）禁用防火墙
	7）安装docker，且开机自启
	8）安装 k3s server 端，并指定容器使用 docker 
	9）得到 token（/var/lib/rancher/k3s/server/node-token）
	10）导入 rancher 集群

k3s-agent:
	0）系统资源先更新到最新（yum update -y）
	1）设置唯一主机名，必须要设置
	2）关闭 selinux
	3）关闭 swap
	4）修改 /etc/sysctl.conf
	6）更改 ip -> 主机名映射
	7）禁用防火墙
	8）安装docker，且开机自启
	9）安装 k3s 工作节点，并设定 server 的 url 和 token