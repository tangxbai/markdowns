

### 一、K8S 简介

**kubernetes**，简称 k8s，是用 8 代替 8 个字符 “kubernetes” 而成的缩写。是一个开源的，用于管理云平台中多个主机上的容器化的应用，Kubernetes 的目标是让部署容器化的应用简单并且高效（powerful），Kubernetes 提供了应用部署，规划，更新，维护的一种机制。
传统的应用部署方式是通过插件或脚本来安装应用。这样做的缺点是应用的运行、配置、管理、所有生存周期将与当前操作系统绑定，这样做并不利于应用的升级更新/回滚等操作，当然也可以通过创建虚拟机的方式来实现某些功能，但是虚拟机非常重，并不利于可移植性。
新的方式是通过部署容器方式实现，每个容器之间互相隔离，每个容器有自己的文件系统 ，容器之间进程不会相互影响，能区分计算资源。相对于虚拟机，容器能快速部署，由于容器与底层设施、机器文件系统解耦的，所以它能在不同云、不同版本操作系统间进行迁移。
容器占用资源少、部署快，每个应用可以被打包成一个容器镜像，每个应用与容器间成一对一关系也使容器有更大优势，使用容器可以在build或release 的阶段，为应用创建容器镜像，因为每个应用不需要与其余的应用堆栈组合，也不依赖于生产环境基础结构，这使得从研发到测试、生产能提供一致环境。类似地，容器比虚拟机轻量、更“透明”，这更便于监控和管理。



**参考文献**：

- Kubord：https://www.kuboard.cn/install/install-dashboard.html
- Kubord: https://kuboard.cn/install/install-k8s.html
- Kubernetes：https://www.kubernetes.org.cn/k8s
- RKE2：https://docs.rke2.io/zh



### 二、概述

Kubernetes 是 Google 开源的一个容器编排引擎，它支持自动化部署、大规模可伸缩、应用容器化管理。在生产环境中部署一个应用程序时，通常要部署该应用的多个实例以便对应用请求进行负载均衡。

在 Kubernetes 中，我们可以创建多个容器，每个容器里面运行一个应用实例，然后通过内置的负载均衡策略，实现对这一组应用实例的管理、发现、访问，而这些细节都不需要运维人员去进行复杂的手工配置和处理。



### 三、特点

- 可移植：支持公有云，私有云，混合云，多重云（multi-cloud）；
- 可扩展：模块化，插件化，可挂载，可组合；
- 自动化：自动部署，自动重启，自动复制，自动伸缩/扩展；



### 四、相关名词

- fluentd：是一个守护进程，可提供 cluster-level logging；
- etcd：Kubernetes 提供默认的存储系统，保存所有集群数据，使用时需要为 etcd 数据提供备份计划；
- kubelet：主要的节点代理，它会监视已分配给节点的 pod；
- kube-proxy：通过在主机上维护网络规则并执行连接转发来实现 kubernetes 服务抽象；
- kube-apiserver：用于暴露 kubernetes api。任何的资源请求/调用操作都是通过 kube-apiserver 提供的接口进行；
- kube-scheduler：监视新创建没有分配到 Node 的 Pod，为 Pod 选择一个 Node；
- kube-controller-manager：运行管理控制器，它们是集群中处理常规任务的后台线程。逻辑上，每个控制器是一个单独的进程，但为了降低复杂性，它们都被编译成单个二进制文件，并在单个进程中运行；
- cloud-controller-manager：云控制器管理器负责与底层云提供商的平台交互；



### 五、配置要求

- 2 核 4G 以上；
- CPU 必须为 x86 架构，暂时未适配 arm 架构的 CPU；
- CentOS 7.8，CentOS 7.9，Ubuntu 20.04；
- 不少于 10G 磁盘空余空间；
- 已经安装好 docker；



### 六、系统环境配置

##### # 改静态 IP

```shell
vim /etc/sysconfig/network-scripts/ifcfg-ens33
# ------------------------------------------------------------------------------#
BOOTPROTO="static"
DNS1="8.8.8.8
NETMASK="255.255.255.0"
GATEWAY="192.168.x.x"
IPADDR="192.168.x.x"
# ------------------------------------------------------------------------------#
systemctl restart network
```

##### # 镜像加速

```shell
# 覆盖基础镜像地址
mv /etc/yum.repos.d/CentOS-Base.repo /etc/yum.repos.d/CentOS-Base.repo.bak
wget -O /etc/yum.repos.d/CentOS-Base.repo http://mirrors.aliyun.com/repo/Centos-7.repo
sed -i "s/http/https/g" /etc/yum.repos.d/CentOS-Base.repo

# 更新镜像
yum clean all
yum makecache

# 查看镜像仓库配置列表
yum repolist
```

##### # 安装必要软件

```shell
yum install -y vim gcc wget curl telnet
yum install -y net-tools http-tools yum-utils epel-release
yum install -y lrzsz glibc openssl openssl-devel
```

##### # 更新内核

```shell
# 查看当前系统内核版本
uname -r

# 导入 RPM GPG public key
rpm --import https://www.elrepo.org/RPM-GPG-KEY-elrepo.org
# 安装 ELRepo
yum install -y https://www.elrepo.org/elrepo-release-7.el7.elrepo.noarch.rpm
# 查询可用的内核软件包
yum --disablerepo="*" --enablerepo="elrepo-kernel" list available

# 安装新版本内核
# 在 ELRepo 中有两个内核选项，一个是 kernel-lt（长期支持版），一个是 kernel-ml（主线最新版本），采用长期支持版本（kernel-lt），更加稳定一些。
# - kernel-ml: 根据 Linux Kernel Archives 的主线稳定分支提供的源构建的。内核配置基于默认的 RHEL-7 配置，
#              并根据需要启用了添加的功能。这些软件包有意命名为 kernel-ml，以免与 RHEL-7 内核发生冲突。
#              因此，它们可以与常规内核一起安装和更新。
# - kernel-lt: 从 Linux Kernel Archives 提供的源代码构建的，就像 kernel-ml 软件包一样。 
#              不同之处在于 kernel-lt 基于长期支持分支，而 kernel-ml 基于主线稳定分支。
yum --enablerepo=elrepo-kernel install kernel-lt -y

# 查看系统默认内核
grubby --default-kernel
# 查看当前系统默认内核
grubby --info=ALL | grep ^kernel
# 修改系统默认内核为新内核
grubby --set-default "/boot/vmlinuz-5.4.273-1.el7.elrepo.x86_64"

# 删除旧内核
# 1、查询目前系统中有哪些内核
rpm -q kernel
# 2、查询当前系统正在使用的内核
uname -a
# 3、删除当前系统没有使用的内核
rpm -e $(rpm -q kernel)

# 重启系统，使用新内核引导系统
reboot
```

##### # 主机名设置

**注意**：每台主机必须具有唯一的主机名。如果你的主机没有唯一的主机名，请在 `config.yaml` 文件中设置 `node-name` 参数，并为每个节点提供一个有效且唯一的主机名。（来自：[https://docs.rke2.io/zh/install/quickstart](https://docs.rke2.io/zh/install/quickstart)）

```shell
# server
hostnamectl --static set-hostname k8s-server-01 # 192.168.1.100
hostnamectl --static set-hostname k8s-server-02 # 192.168.1.101
hostnamectl --static set-hostname k8s-server-03 # 192.168.1.102
...

# agent
hostnamectl --static set-hostname k8s-agent-01 # 192.168.1.103
hostnamectl --static set-hostname k8s-agent-02 # 192.168.1.104
hostnamectl --static set-hostname k8s-agent-03 # 192.168.1.105
...
```

##### # 配置 hosts

```shell
# server
cat >> /etc/hosts << EOF
192.168.1.100 k8s-server-01
192.168.1.101 k8s-server-02
192.168.1.102 k8s-server-03
EOF

# agent
cat >> /etc/hosts << EOF
192.168.1.103 k8s-agent-01
192.168.1.104 k8s-agent-02
192.168.1.105 k8s-agent-03
EOF
```

##### # 其他配置

```shell
# 关闭 SeLinux 临时关闭 setenforce 0
getenforce
setenforce 0
sed -i "s/SELINUX=enforcing/SELINUX=disabled/g" /etc/selinux/config
getenforce

# 关闭 swap 临时关闭  swapoff -a 此步完成后需要重启虚拟机
swapoff -a
sed -ri 's/.*swap.*/#&/' /etc/fstab

# 配置 br_netfilter 和 overlay 模块
cat <<EOF | sudo tee /etc/modules-load.d/k8s.conf
overlay
br_netfilter
EOF
sudo modprobe overlay
sudo modprobe br_netfilter

# 设置所需的 sysctl 参数，参数在重新启动后保持不变
cat <<EOF | sudo tee /etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-iptables  = 1
net.bridge.bridge-nf-call-ip6tables = 1
net.ipv4.ip_forward                 = 1
EOF

# 应用 sysctl 参数而不重新启动
sudo sysctl --system

# 通过运行以下指令确认 br_netfilter 和 overlay 模块被加载
lsmod | grep br_netfilter
lsmod | grep overlay
```

##### # 配置 K8S 的 yum 源

```shell
cat <<EOF > /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=http://mirrors.aliyun.com/kubernetes/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=0
repo_gpgcheck=0
gpgkey=http://mirrors.aliyun.com/kubernetes/yum/doc/yum-key.gpg
       http://mirrors.aliyun.com/kubernetes/yum/doc/rpm-package-key.gpg
EOF
```

##### # 网络配置

```shell
# NetworkManager 设置
cat >> /etc/NetworkManager/conf.d/rke2-canal.conf << EOF
[keyfile]
unmanaged-devices=interface-name:cali*;interface-name:flannel*
EOF

# 重启服务
systemctl daemon-reload
systemctl restart NetworkManager

# 查看状态
systemctl status NetworkManager
```

##### # 防火墙

```shell
# 关闭防火墙
systemctl stop firewalld
systemctl disable firewalld

# 有 iptables 服务就一起执行，没有可以不用管它
systemctl stop iptables
systemctl disable iptables
```

##### # 时间同步

```shell
# 安装时区同步服务
yum -y install ntp

# 开机启动 & 启动服务
systemctl enable ntpd
systemctl start ntpd

# 查看时钟同步状态
ntpq -p
```

##### # ipvs 配置

```shell
lsmod | grep ip_vs
ls /lib/modules/$(uname -r)/kernel/net/netfilter/ipvs|grep -o "^[^.]*" > /etc/modules-load.d/ip_vs.conf

# 添加 ipvs 配置
cat > /etc/sysconfig/modules/ipvs.modules <<EOF
#!/bin/bash
modprobe -- ip_vs
modprobe -- ip_vs_rr
modprobe -- ip_vs_wrr
modprobe -- ip_vs_sh
modprobe -- nf_conntrack_ipv4
EOF

# 执行脚本
chmod 755 /etc/sysconfig/modules/ipvs.modules 
bash /etc/sysconfig/modules/ipvs.modules 
lsmod | grep -e ip_vs -e nf_conntrack_ipv4

# 安装 ipset ipvsadm
yum install ipset ipvsadm -y
```

##### # 最后

```shell
systemctl daemon-reload
sysctl -p
```



### 七、安装 Kubernetes

文档参考地址：[https://docs.rke2.io/zh](https://docs.rke2.io/zh)

##### # 简介

RKE2，也称为 RKE Government，是 Rancher 的下一代 Kubernetes 发行版。

它是一个[完全合规的 Kubernetes 发行版](https://landscape.cncf.io/card-mode?selected=rke-government)，专注于安全和合规性。

为了实现这些目标，RKE2 会：

- 提供[默认值和配置选项](https://docs.rke2.io/zh/security/hardening_guide)，让集群能使用最少的人员干预通过 CIS Kubernetes Benchmark [v1.6](https://docs.rke2.io/zh/security/cis_self_assessment16) 或 [v1.23](https://docs.rke2.io/zh/security/cis_self_assessment123)；
- 启用 [FIPS 140-2 合规](https://docs.rke2.io/zh/security/fips_support)；
- 在我们的构建流水线中使用 [trivy](https://github.com/aquasecurity/trivy) 定期扫描组件以查找 CVE；



##### # 高可用

如何安装高可用 (HA) 的 RKE2 集群。一个高可用的 RKE2 集群由以下部分组成：

- 一个**固定的注册地址**，放在 Server 节点的前面，允许其他节点注册到集群；
- 运行 etcd、Kubernetes API 和其他 control plane 服务的奇数个（推荐三个）**Server 节点；**
- 零个或多个 **Agent 节点**，用于运行你的应用和服务；

Agent 通过固定注册地址注册。然而，当 RKE2 启动 kubelet 并且必须连接到 Kubernetes api-server 时，它通过 `rke2 agent` 进程进行连接，该进程充当客户端负载均衡器。

设置 HA 集群需要以下步骤：

1. 配置固定注册地址；
2. 启动第一个 Server 节点；
3. 加入额外的 Server 节点；
4. 加入 Agent 节点；



##### # 准备配置文件

```shell
# 创建K8S集群节点配置，对于目录：/etc/rancher/rke2/config.yaml
# 详细配置见地址：https://docs.rke2.io/zh/reference/server_config 以下内容为配置文件内容

# 查看 server token
# cat /home/rancher/rke2/server/token
mkdir -p /etc/rancher/rke2
cat >> /etc/rancher/rke2/config.yaml << EOF

# 其他 Server 节点与第一个节点非常相似，只是你必须指定 server 和 token 参数，
# 以便它们可以成功连接到初始 Server 节点。
server: https://192.168.1.100:9345
token: K10543d47be0df78aa0dc6e98442adc88bd70c1159d9617a37c19f9e1bff0375963::server:d6dc65b3d26c0b434929f2657fec5f35

# 网络模块
cni: calico

# 程序安装目录
data-dir: /home/rancher/rke2

# 集群的名字
cluster-domain: "cluster.k8s"

# 默认情况下，Server 节点是可调度的，因此你的工作负载可以在它们上启动。
# 如果你希望拥有一个不会运行用户工作负载的专用 control plane，你可以使用污点（taint）。
# node-taint 参数允许你配置带有污点的节点。
# node-taint: [ "CriticalAddonsOnly=true:NoExecute" ]

# 配置写入模式（无需更改）
write-kubeconfig-mode: "0644"
cluster-cidr: 192.168.60.0/24
service-cidr: 192.168.61.0/24

# 开放的服务端口，需要的话，可以适当调整。
service-node-port-range: 30000-32767

# 你的所有节点主机列表
tls-san:
  - 127.0.0.1
  - 192.168.1.100
  - 192.168.1.101
  - 192.168.1.102
  - k8s-server-01
  - k8s-server-02
  - k8s-server-03

# 基础配置
kube-proxy-arg:
  - proxy-mode=ipvs
disable:
  - rke2-ingress-nginx
disable-cloud-controller: true
selinux: false
debug: false
EOF
```



##### # 镜像仓库配置

Containerd 可以配置为连接到私有镜像仓库，并使用仓库在每个节点上拉取私有镜像。

启动时，RKE2 会检查 `/etc/rancher/rke2/` 中是否存在 `registries.yaml` 文件，并指示 containerd 使用该文件中定义的镜像仓库。如果你想使用私有的镜像仓库，你需要在每个使用镜像仓库的节点上以 root 身份创建这个文件。

请注意，server 节点默认是可以调度的。如果你没有在 server 节点上设置污点，而且希望在 server 节点上运行工作负载，请确保在每个 server 节点上创建 `registries.yaml` 文件。

**注意**：在 RKE2 v1.20 之前，初始 RKE2 节点引导不支持 containerd 镜像仓库配置，仅可用于节点加入集群后启动的 Kubernetes 工作负载。如果你计划使用此 containerd 镜像仓库功能来引导节点，请参阅 [离线安装文档](https://docs.rke2.io/zh/install/airgap)。

Containerd 中的配置可以用于通过 TLS 连接到私有镜像仓库，也可以与启用验证的镜像仓库连接。下一节将解释 `registries.yaml` 文件，并给出在 RKE2 中使用私有镜像仓库配置的不同例子。

参考地址：[https://docs.rke2.io/zh/install/containerd_registry_configuration](https://docs.rke2.io/zh/install/containerd_registry_configuration)

```shell
cat >> /etc/rancher/rke2/registries.yaml << EOF
mirrors:
  registry.example.com:
    endpoint:
      - "https://registry.example.com:5000"
configs:
  "registry.example.com:5000":
    auth:
      username: xxxxxx # this is the registry username
      password: xxxxxx # this is the registry password
EOF
```



##### # 开始安装

注意：你的 Server 节点总数必须为奇数，否则无法保证高可用状态，这是强制要求的。

使用此方法安装 RKE2 时，你可以使用以下环境变量来配置安装：

```shell
curl -sfL https://rancher-mirror.rancher.cn/rke2/install.sh | KEY=VALUE ... sh -
```

| 环境变量                 | 描述                                                         |
| ------------------------ | ------------------------------------------------------------ |
| INSTALL_RKE2_VERSION     | 从 GitHub 下载的 RKE2 版本。如果未指定，将尝试从 `stable` channel 下载最新版本。如果在基于 RPM 的系统上安装并且 `stable` channel 中不存在所需的版本，则也应设置 `INSTALL_RKE2_CHANNEL`。地址：[https://github.com/rancher/rke2](https://github.com/rancher/rke2) |
| INSTALL_RKE2_TYPE        | 要创建的 systemd 服务类型，可以是 "server" 或 "agent"，默认值是 "server"。 |
| INSTALL_RKE2_CHANNEL_URL | 用于获取 RKE2 下载 URL 的 Channel URL。默认为 `https://update.rke2.io/v1-release/channels`。 |
| INSTALL_RKE2_CHANNEL     | 用于获取 RKE2 下载 URL 的 Channel。默认为 `stable`。可选项：`stable`、`latest`、`testing`。 |
| INSTALL_RKE2_METHOD      | 安装方法。默认是基于 RPM 的系统 `rpm`，所有其他系统都是 `tar`。 |
| INSTALL_RKE2_MIRROR      | 区域                                                         |

```shell
# 安装 Server
curl -sfL https://rancher-mirror.rancher.cn/rke2/install.sh | INSTALL_RKE2_MIRROR=cn INSTALL_RKE2_VERSION=v1.29.1+rke2r1 sh -
# 启用 rke2-server 服务
systemctl enable rke2-server
systemctl start rke2-server

# 安装 Agent
curl -sfL https://rancher-mirror.rancher.cn/rke2/install.sh | INSTALL_RKE2_MIRROR=cn INSTALL_RKE2_VERSION=v1.29.1+rke2r1 INSTALL_RKE2_TYPE=agent sh -
# rke2-agent
systemctl enable rke2-agent
systemctl start rke2-agent

# 配置 K8S 默认配置文件
# 启动 rke2-server | rke2-agent 后等几十秒执行此部分
mkdir ~/.kube/
# /etc/rancher/rke2/rke2.yaml 这个配置文件相当于集群的钥匙，管理整个集群
cp /etc/rancher/rke2/rke2.yaml ~/.kube/config
chmod 600 ~/.kube/config

# 如有需要，可以查看日志
journalctl -u rke2-server -f
journalctl -u rke2-agent -f

# 环境变量设置
# 默认情况下无需执行此步骤，因为上面配置文件中更改了脚本安装的默认路径，所以此时 kubectl 是不生效的
# 所以最好的版本就是将 bin 目录下的脚本通过环境变量映射出去，以便于直接使用目录下的脚本命令。
cat >> /etc/profile << EOF
export PATH=$PATH:/home/rancher/rke2/bin
EOF
source /etc/profile

# 执行如下命令，等待 3-10 分钟，直到所有的容器组处于 Running 状态
watch kubectl get pod -n kube-system -o wide

# 查看 master 节点初始化结果
kubectl get nodes -o wide
kubectl get nodes -o wide

# 查看所有 pods 状态
kubectl get pods -A
```



##### # 卸载

要从系统中卸载通过 RPM 安装的 RKE2，只需以 root 用户或通过 `sudo` 运行你的 RKE2 版本对应的命令。这将关闭 RKE2 进程，删除 RKE2 RPM，并清理 RKE2 使用的文件。

```shell
# /usr/bin/rke2-killall.sh
/usr/bin/rke2-uninstall.sh
```



##### # 配置参考项

```yaml
# (logging) Turn on debug logs [$RKE2_DEBUG]
debug: <boolean>

# (listener) rke2 bind address (default: 0.0.0.0)
bind-address: <ipaddress: "0.0.0.0">

# (listener) 
# IPv4 address that apiserver uses to advertise to members of the cluster 
# (default: node-external-ip/node-ip)
advertise-address: <ipaddress: node-external-ip/node-ip>

# (listener) 
# Add additional hostnames or IPv4/IPv6 addresses as Subject Alternative Names on the server TLS cert
tls-san: <array-string>

# (data) 
# Folder to hold state (default: "/var/lib/rancher/rke2")
# The path where the installer is stored
data-dir: <dir: "/var/lib/rancher/rke2">

# (networking) CNI Plugins to deploy, one of none, calico, canal, cilium; optionally with multus as the first value to enable the multus meta-plugin (default: canal) [$RKE2_CNI]
cni: <string: "canal"["none" | "calico" | "canal" | "cilium"]>
# (networking) IPv4/IPv6 network CIDRs to use for pod IPs (default: 10.42.0.0/16)
cluster-cidr: <string: "10.42.0.0/16">
# (networking) IPv4/IPv6 network CIDRs to use for service IPs (default: 10.43.0.0/16)
service-cidr: <string: "10.43.0.0/16">
# (networking) 
# Port range to reserve for services with NodePort visibility (default: "30000-32767")
service-node-port-range: <string: "30000-32767">
# (networking) 
# IPv4 Cluster IP for coredns service. Should be in your service-cidr range (default: 10.43.0.10)
cluster-dns: <array-string: "10.43.0.10">
# (networking) Cluster Domain (default: "cluster.local")
cluster-domain: <string: "cluster.local">

# (cluster) Shared secret used to join a server or agent to a cluster [$RKE2_TOKEN]
# you can use this command to get: "cat /${data-dir}/server/token"
token: <string>
# (cluster) File containing the cluster-secret/token [$RKE2_TOKEN_FILE]
token-file: <dir>
# (client) Write kubeconfig for admin client to this file [$RKE2_KUBECONFIG_OUTPUT]
write-kubeconfig: <file>
# (client) Write kubeconfig with this mode [$RKE2_KUBECONFIG_MODE]
write-kubeconfig-mode: <string: "0644">

# (flags) Customized flag for kube-apiserver process
kube-apiserver-arg: <array-string>
# (flags) Customized flag for etcd process
etcd-arg: <array-string>
# (flags) Customized flag for kube-controller-manager process
kube-controller-manager-arg: <array-string>
# (flags) Customized flag for kube-scheduler process
kube-scheduler-arg: <array-string>

# (db) Expose etcd metrics to client interface. (Default false)
etcd-expose-metrics: <boolean>
# (db) Disable automatic etcd snapshots
etcd-disable-snapshots: <boolean>
# (db) Set the base name of etcd snapshots. 
# Default: etcd-snapshot-<unix-timestamp> (default: "etcd-snapshot")
etcd-snapshot-name: <string: "etcd-snapshot">
# (db) Snapshot interval time in cron spec. 
# eg. every 5 hours '0 */5 * * *' (default: "0 */12 * * *")
etcd-snapshot-schedule-cron: <string: "0 */12 * * *">
# (db) Number of snapshots to retain Default: 5 (default: 5)
etcd-snapshot-retention: <number: 5>
# (db) Directory to save db snapshots. (Default location: ${data-dir}/db/snapshots)
etcd-snapshot-dir: <dir: "${data-dir}/db/snapshots">

# (db) Enable backup to S3
etcd-s3: <boolean>
# (db) S3 endpoint url (default: "s3.amazonaws.com")
etcd-s3-endpoint: <string: "s3.amazonaws.com">
# (db) S3 custom CA cert to connect to S3 endpoint
etcd-s3-endpoint-ca: <string>
# (db) Disables S3 SSL certificate validation
etcd-s3-skip-ssl-verify: <string>
# (db) S3 access key [$AWS_ACCESS_KEY_ID]
etcd-s3-access-key: <string>
# (db) S3 secret key [$AWS_SECRET_ACCESS_KEY]
etcd-s3-secret-key: <string>
# (db) S3 bucket name
etcd-s3-bucket: <string>
# (db) S3 region / bucket location (optional) (default: "us-east-1")
etcd-s3-region: <string: "us-east-1">
# (db) S3 folder
etcd-s3-folder: <dir>
# (db) Path to snapshot file to be restored
cluster-reset-restore-path: <file>

# (agent/node) Node name [$RKE2_NODE_NAME]
node-name: <hostname>
# (agent/node) Registering and starting kubelet with set of labels
node-label: <array-string>
# (agent/node) Registering kubelet with set of taints
node-taint: <array>
node-taint: [ "CriticalAddonsOnly=true:NoExecute" ] # Server only
# (agent/node) Enable SELinux in containerd [$RKE2_SELINUX]
selinux: <boolean>
# (agent/node) Local port for supervisor client load-balancer. 
# If the supervisor and apiserver are not colocated an additional port 1 less than this port will also be used for the apiserver client load-balancer. (default: 6444) [$RKE2_LB_SERVER_PORT]
lb-server-port: <number: 6444>
# (agent/node) 
# The path to the directory where credential provider plugin binaries are located 
# (default: "/var/lib/rancher/credentialprovider/bin")
image-credential-provider-bin-dir: <file: "/var/lib/rancher/credentialprovider/bin">
# (agent/node) 
# The path to the credential provider plugin config file 
# (default: "/var/lib/rancher/credentialprovider/config.yaml")
image-credential-provider-config: <file: "/var/lib/rancher/credentialprovider/config.yaml">
# (agent/node) Kernel tuning behavior. 
# If set, error if kernel tunables are different than kubelet defaults.
protect-kernel-defaults: <string>

# (agent/runtime) Disable embedded containerd and use alternative CRI implementation
container-runtime-endpoint: <boolean>
# (agent/runtime) Override default containerd snapshotter (default: "overlayfs")
snapshotter: <string: "overlayfs">
# (agent/runtime) Private registry configuration file (default: "/etc/rancher/rke2/registries.yaml")
private-registry: <string: "/etc/rancher/rke2/registries.yaml">

# (agent/networking) IPv4/IPv6 addresses to advertise for node
node-ip: <ipaddress>
# (agent/networking) IPv4/IPv6 external IP addresses to advertise for node
node-external-ip: <ipaddress>
# (agent/networking) Kubelet resolv.conf file [$RKE2_RESOLV_CONF]
resolv-conf: <file>

# (agent/flags) Customized flag for kubelet process
kubelet-arg: <array-string>
# (agent/flags) Customized flag for kube-proxy process
kube-proxy-arg: <array-string>

# (experimental/cluster) Server to connect to, used to join a cluster [$RKE2_URL]
server: <ipaddress>
# (experimental/cluster) Shared secret used to join agents to the cluster, 
# but not servers [$RKE2_AGENT_TOKEN]
agent-token: <string>
# (experimental/cluster) File containing the agent secret [$RKE2_AGENT_TOKEN_FILE]
agent-token-file: <file>
# (experimental/cluster) Forget all peers and become sole member of a new cluster [$RKE2_CLUSTER_RESET]
cluster-reset: <boolean>

# (experimental/agent) Override kubelet binary path [$RKE2_KUBELET_PATH]
kubelet-path: <dir>

# (cloud provider) Cloud provider name [$RKE2_CLOUD_PROVIDER_NAME]
cloud-provider-name: <string>
# (cloud provider) Cloud provider configuration file path [$RKE2_CLOUD_PROVIDER_CONFIG]
cloud-provider-config: <string>

# (security) Validate system configuration against the selected benchmark (valid items: cis-1.6, cis-1.23 ) [$RKE2_CIS_PROFILE]
profile: <string>
# (security) Path to the file that defines the audit policy configuration [$RKE2_AUDIT_POLICY_FILE]
audit-policy-file: <file>

# (image) Private registry to be used for all system images [$RKE2_SYSTEM_DEFAULT_REGISTRY]
system-default-registry: <string>
# (image) Override image to use for kube-apiserver [$RKE2_KUBE_APISERVER_IMAGE]
kube-apiserver-image: <string>
# (image) Override image to use for kube-controller-manager [$RKE2_KUBE_CONTROLLER_MANAGER_IMAGE]
kube-controller-manager-image: <string>
# (image) Override image to use for kube-proxy [$RKE2_KUBE_PROXY_IMAGE]
kube-proxy-image: <string>
# (image) Override image to use for kube-scheduler [$RKE2_KUBE_SCHEDULER_IMAGE]
kube-scheduler-image: <string>
# (image) Override image to use for pause [$RKE2_PAUSE_IMAGE]
pause-image: <string>
# (image) Override image to use for runtime binaries (containerd, kubectl, crictl, etc) [$RKE2_RUNTIME_IMAGE]
runtime-image: <string>
# (image) Override image to use for etcd [$RKE2_ETCD_IMAGE]
etcd-image: <string>

# (components) 
# Do not deploy packaged components and delete any deployed components
# (valid items: rke2-coredns, rke2-ingress-nginx, rke2-metrics-server)
disable: <components: "rke2-coredns" | "rke2-ingress-nginx" | "rke2-metrics-server">
# (components) Disable Kubernetes default scheduler
disable-scheduler: <boolean>
# (components) Disable rke2 default cloud controller manager
disable-cloud-controller: <boolean>
# (components) Disable running kube-proxy
disable-kube-proxy: <boolean>
# (components) Disable running etcd
disable-etcd: <boolean>

# (components) Control Plane resource requests [$RKE2_CONTROL_PLANE_RESOURCE_REQUESTS]
control-plane-resource-requests: <?>
# (components) Control Plane resource limits [$RKE2_CONTROL_PLANE_RESOURCE_LIMITS]
control-plane-resource-limits: <?>
# (components) etcd extra environment variables [$RKE2_ETCD_EXTRA_ENV]
etcd-extra-env: <?>   
# (components) etcd extra volume mounts [$RKE2_ETCD_EXTRA_MOUNT]
etcd-extra-mount: <?>
# (components) kube-apiserver extra environment variables [$RKE2_KUBE_APISERVER_EXTRA_ENV]
kube-apiserver-extra-env: <?>
# (components) kube-apiserver extra volume mounts [$RKE2_KUBE_APISERVER_EXTRA_MOUNT]
kube-apiserver-extra-mount: <?>
# (components) kube-proxy extra environment variables [$RKE2_KUBE_PROXY_EXTRA_ENV]
kube-proxy-extra-env: <?>
# (components) kube-proxy extra volume mounts [$RKE2_KUBE_PROXY_EXTRA_MOUNT]
kube-proxy-extra-mount: <?>
# (components) kube-scheduler extra environment variables [$RKE2_KUBE_SCHEDULER_EXTRA_ENV]
kube-scheduler-extra-env: <?>
# (components) kube-scheduler extra volume mounts [$RKE2_KUBE_SCHEDULER_EXTRA_MOUNT]
kube-scheduler-extra-mount: <?>
# (components) 
# kube-controller-manager extra environment variables [$RKE2_KUBE_CONTROLLER_MANAGER_EXTRA_ENV]
kube-controller-manager-extra-env: <?>
# (components) kube-controller-manager extra volume mounts [$RKE2_KUBE_CONTROLLER_MANAGER_EXTRA_MOUNT]
kube-controller-manager-extra-mount: <?>
# (components) 
# cloud-controller-manager extra environment variables [$RKE2_CLOUD_CONTROLLER_MANAGER_EXTRA_ENV]
cloud-controller-manager-extra-env: <?>
# (components) 
# cloud-controller-manager extra volume mounts [$RKE2_CLOUD_CONTROLLER_MANAGER_EXTRA_MOUNT]
cloud-controller-manager-extra-mount: <?>
```



### 八、Kubectl 基础命令

```shell
# 执行如下命令，等待 3-10 分钟，直到所有的容器组处于 Running 状态
watch kubectl get pod -n kube-system -o wide

# 查看 master 节点初始化结果
kubectl get nodes -o wide
kubectl get nodes -o wide

# 驱逐 Node
kubectl drain <node> --delete-local-data --ignore-daemonsets --force

# 查看所有 pods 状态
kubectl get pods -A
```



### 七、UI 管理工具

##### # Kuboard

参考文档：[https://press.demo.kuboard.cn/install/v3/install-in-k8s.html](https://press.demo.kuboard.cn/install/v3/install-in-k8s.html)

```shell
# 在线安装
kubectl apply -f https://addons.kuboard.cn/kuboard/kuboard-v3.yaml
# 执行 Kuboard v3 的卸载
kubectl delete -f https://addons.kuboard.cn/kuboard/kuboard-v3.yaml
rm -rf /usr/share/kuboard
```

##### # Rancher

略微复杂，没有实际研究过，有需要的话，请参照 [Rancher 官方在线文档](https://ranchermanager.docs.rancher.com/zh/getting-started/installation-and-upgrade/install-upgrade-on-a-kubernetes-cluster)。

