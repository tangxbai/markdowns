```shell
# 查看所有 pod 列表, -n 后跟 namespace, 查看指定的命名空间
kubectl get pod
kubectl get pod -n kube  
kubectl get pod -o wide

# 查看 RC 和 service 列表， -o wide 查看详细信息
kubectl get rc,svc
kubectl get pod,svc -o wide  
kubectl get pod <pod-name> -o yaml

# 显示 Node 的详细信息
kubectl describe node <node-ip>

# 显示 Pod 的详细信息, 特别是查看 pod 无法创建的时候的日志
kubectl describe pod <pod-name>

# 根据 yaml 创建资源, apply 可以重复执行，create 不行
kubectl create -f pod.yaml
kubectl apply -f pod.yaml

# 基于 xxx.yaml 定义的名称删除 pod 
kubectl delete -f pod.yaml 
# 删除所有包含某个 label 的pod 和 service
kubectl delete pod,svc -l name=<label-name>

# 驱逐 Node
kubectl drain <node> --delete-local-data --ignore-daemonsets --force

# 删除所有 Pod
kubectl delete pod --all

# 查看 endpoint 列表
kubectl get endpoints

# 执行 pod 的 date 命令
kubectl exec <pod-name> -- date
kubectl exec <pod-name> -- bash
kubectl exec <pod-name> -- ping 10.24.51.9

# 通过 bash 获得 pod 中某个容器的 TTY，相当于登录容器
kubectl exec -it <pod-name> -c <container-name> -- bash

# 查看容器的日志
kubectl logs <pod-name>
kubectl logs -f <pod-name> # 实时查看日志
kubectl log <pod-name> -c <container_name> # 若 pod 只有一个容器，可以不加 -c

# 获取所有命名空间
kubectl get namespace
kubectl get ns
```