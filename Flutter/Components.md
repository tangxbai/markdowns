## Flutter

Flutter 是 Google 推出并开源的移动端开发框架，主打跨平台、高保真、高性能。开发者可以通过 Dart 语言开发 App，一套代码可以同时运行在 iOS 和 Android 平台。

2018 年 12 月，Google 发布 Flutter 1.0。从那时候开始，Flutter 以迅雷不及掩耳之势，迅速崛起，并稳固了其在市场上的地位。

如今，Flutter 相关资源和社区都已渐渐成熟，得到了很多开发者和企业的信任。另外因为公司业务扩张，准备引入 Flutter 开发项目，所以学习 Flutter 势在必行，下面就跟着我的脚步进行 Flutter 的学习吧。



### 1、Container

```dart
Container({
  super.key,
  this.alignment,
  this.padding,
  this.color,
  this.decoration,
  this.foregroundDecoration,
  double? width,
  double? height,
  BoxConstraints? constraints,
  this.margin,
  this.transform,
  this.transformAlignment,
  this.child,
  this.clipBehavior = Clip.none,
})
```



### 2、Decoration

### 3、SizedBox

### 4、线性布局

#### 4.1、Row

#### 4.2、Column

### 5、弹性布局

#### 5.1、Flexible

#### 5.2、Expanded

#### 5.3、Spacer

### 6、流式布局（Wrap）

### 7、层叠布局（Stack）

#### 7.1、Stack

#### 7.2、IndexedStatck

### 8、宽高比（AspectRatio）

### 9、布局约束

#### 9.1、FittedBox

#### 9.2、FractionallySizedBox

#### 9.3、ContainedBox





