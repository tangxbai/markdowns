### 一、废话简介

SpringCloud Gateway 是 Spring 全家桶中一个比较新的项目，是 SpringCloud 团队基于 Spring 5.0、SpringBoot 2.0 和 Project Reactor 等技术开发的高性能 API 网关组件。

该项目借助 Spring WebFlux 的能力，打造了一个基于 JAVA 构造的 API 网关，而 WebFlux 框架底层则使用了高性能的 Reactor 模式通信框架 Netty。旨在提供一种简单而有效的途径来发送 API，并为它们提供横切关注点，例如：安全性，监控/指标和弹性。 

相关特性：

- 基于 Spring Framework 5、Project Reactor 和 Spring Boot 2.0 构建；
- 能够在任意请求属性上匹配路由；
- predicates（断言） 和 filters（过滤器）是特定于路由的；
- 集成了 Hystrix 熔断器；
- 集成了 Spring Cloud DiscoveryClient（服务发现客户端）；
- 易于编写断言和过滤器；
- 能够限制请求频率（请求限流）；
- 能够重写请求路径；



核心概念：

- Route：网关最基本的模块，它由一个 ID、目标 URI、一组断言（Predicate）和一组过滤器（Filter）组成；
- Predicate：路由转发的判断条件，我们可以通过 Predicate 对 HTTP 请求进行匹配，例如请求方式、请求路径、请求头、参数等，如果请求与断言匹配成功，则将请求转发到相应的服务；
- Filter：过滤器，我们可以使用它对请求进行拦截和修改，还可以使用它对上文的响应进行再处理；



工作流程（图片来自网络）：

![Gateway](https://c.biancheng.net/uploads/allimg/211210/101P45T2-1.png)



### 二、使用方式

```xml
<dependencyManagement>
    <dependencies>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-dependencies</artifactId>
            <version>${spring-boot.version}</version>
            <type>pom</type>
            <scope>import</scope>
        </dependency>
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-dependencies</artifactId>
            <version>${spring-cloud.version}</version>
            <type>pom</type>
            <scope>import</scope>
        </dependency>
    </dependencies>
</dependencyManagement>

<dependencies>
    <dependency>
        <groupId>org.springframework.cloud</groupId>
        <artifactId>spring-cloud-starter-gateway</artifactId>
    </dependency>
</dependencies>
```



### 三、静态路由

```yaml

```



### 四、动态路由



### 五、过滤器



### 六、断言器



### 七、拦截器



### 八、请求限流



### 九、Hystrix 熔断器



### 十、负载均衡



### 十一、原理解析



### 十二、配置属性

GatewayProperties

```properties
spring.cloud.gateway.enabled = <boolean: true>
spring.cloud.gateway.streaming-media-types = [ <MediaType> ]
spring.cloud.gateway.routes = [ {id, uri, order, filters[{name, args}], predicates[{name, args}]} ]
spring.cloud.gateway.default-filters = [ {name, args{KEY: VALUE}} ]
```

```properties
spring.cloud.gateway.forwarded.enabled = <boolean: true>
spring.cloud.gateway.observability.enabled = <boolean: true>
spring.cloud.gateway.httpserver = <boolean>
spring.cloud.gateway.metrics = <boolean: true>
spring.cloud.gateway.metrics.tags = <string>
spring.cloud.gateway.metrics.prefix = <string: spring.cloud.gateway>
```

DiscoveryLocatorProperties

```properties
spring.cloud.gateway.discovery.locator.enabled = <boolean>
spring.cloud.gateway.discovery.locator.filters = [ {name, args{KEY: VALUE}} ]
spring.cloud.gateway.discovery.locator.predicates = [ {name, args} ]
spring.cloud.gateway.discovery.locator.route-id-prefix = <string: dynamic>
spring.cloud.gateway.discovery.locator.include-expression = <boolean: true>
spring.cloud.gateway.discovery.locator.url-expression = <string: "'lb://'+serviceId"> 
spring.cloud.gateway.discovery.locator.lower-case-service-id = <boolean>
```

SecureHeadersProperties

```properties
spring.cloud.gateway.filter.secure-headers.content-security-policy = <string: --default>
spring.cloud.gateway.filter.secure-headers.content-type-options = <string: nosniff>
spring.cloud.gateway.filter.secure-headers.download-options = <string: noopen>
spring.cloud.gateway.filter.secure-headers.frame-options = <string: DENY>
spring.cloud.gateway.filter.secure-headers.permitted-cross-domain-policies = <string: none>
spring.cloud.gateway.filter.secure-headers.referrer-policy = <string: no-referrer> 
spring.cloud.gateway.filter.secure-headers.strict-transport-security = <string: "max-age=631138519">
spring.cloud.gateway.filter.secure-headers.xss-protection-header = <string: "1 ; mode=block">
spring.cloud.gateway.filter.secure-headers.disable = [ <string> ]
```

GlobalCorsProperties

```properties
spring.cloud.gateway.globalcors.cors-configurations.KEY = <CorsConfiguration>
spring.cloud.gateway.globalcors.cors-configurations.KEY = <CorsConfiguration>
spring.cloud.gateway.globalcors.cors-configurations.KEY = <CorsConfiguration>
```

HttpClientProperties

```properties
# Basic
spring.cloud.gateway.httpclient.connect-timeout = <int: 45>
spring.cloud.gateway.httpclient.response-timeout = <Duration>
spring.cloud.gateway.httpclient.wiretap = <boolean>

# HttpClientProperties$Pool
spring.cloud.gateway.httpclient.pool.name = <string: proxy>
spring.cloud.gateway.httpclient.pool.type = <PoolType: PoolType.ELASTIC>
spring.cloud.gateway.httpclient.pool.acquire-timeout = <long>
spring.cloud.gateway.httpclient.pool.max-connections = <int>
spring.cloud.gateway.httpclient.pool.eviction-interval = <int: 0>
spring.cloud.gateway.httpclient.pool.max-connections = <int>
spring.cloud.gateway.httpclient.pool.max-idle-time = <int>
spring.cloud.gateway.httpclient.pool.max-life-time = <int>
spring.cloud.gateway.httpclient.pool.metrics = <boolean>

# HttpClientProperties$Proxy
spring.cloud.gateway.httpclient.proxy.host = <string>
spring.cloud.gateway.httpclient.proxy.port = <int>
spring.cloud.gateway.httpclient.proxy.username = <string>
spring.cloud.gateway.httpclient.proxy.password = <string>
spring.cloud.gateway.httpclient.proxy.non-proxy-hosts-pattern = <string>
spring.cloud.gateway.httpclient.proxy.type = <?>

# HttpClientProperties$Ssl
spring.cloud.gateway.httpclient.ssl.close-notify-flush-timeout = <Duration: 3000ms>
spring.cloud.gateway.httpclient.ssl.close-notify-read-timeout = <Duration: 0>
spring.cloud.gateway.httpclient.ssl.handshake-timeout = <Duration: 10000ms>
spring.cloud.gateway.httpclient.ssl.key-password = <string>
spring.cloud.gateway.httpclient.ssl.key-store = <string>
spring.cloud.gateway.httpclient.ssl.key-store-password = <string>
spring.cloud.gateway.httpclient.ssl.key-store-provider = <string>
spring.cloud.gateway.httpclient.ssl.key-store-type = <KeyStroeType: JKS>
spring.cloud.gateway.httpclient.ssl.trusted-x509-certificates = <string>
spring.cloud.gateway.httpclient.ssl.use-insecure-trust-manager = <string>
```

LoadBalancerProperties

```properties
spring.cloud.gateway.loadbalancer.use404 = <boolean>
```

RedisRateLimiter

```properties
spring.cloud.gateway.redis-rate-limiter.burst-capacity-header = <string: X-RateLimit-Burst-Capacity>
spring.cloud.gateway.redis-rate-limiter.include-headers = <boolean: true>
spring.cloud.gateway.redis-rate-limiter.remaining-header = <string: X-RateLimit-Remaining>
spring.cloud.gateway.redis-rate-limiter.replenish-rate-header = <string: X-RateLimit-Replenish-Rate>
spring.cloud.gateway.redis-rate-limiter.requested-tokens-header = <string: X-RateLimit-Requested-Tokens>
# RedisRateLimiter$Config
spring.cloud.gateway.redis-rate-limiter.default-config.replenish-rate = <int>
spring.cloud.gateway.redis-rate-limiter.default-config.-burst-capacity = <int>
```

XForwardedHeadersFilter

```properties
spring.cloud.gateway.x-forwarded.enabled = <boolean: true>
spring.cloud.gateway.x-forwarded.for-append = <boolean: true>
spring.cloud.gateway.x-forwarded.for-enabled = <boolean: true>
spring.cloud.gateway.x-forwarded.host-append = <boolean: true>
spring.cloud.gateway.x-forwarded.host-enabled = <boolean: true>
spring.cloud.gateway.x-forwarded.order = <int: 0>
spring.cloud.gateway.x-forwarded.port-append = <boolean: true>
spring.cloud.gateway.x-forwarded.port-enabled = <boolean: true>
spring.cloud.gateway.x-forwarded.prefix-append = <boolean: true>
spring.cloud.gateway.x-forwarded.prefix-enabled = <boolean: true>
spring.cloud.gateway.x-forwarded.proto-append = <boolean: true>
spring.cloud.gateway.x-forwarded.proto-enabled = <boolean: true>
```

RemoveHopByHopHeadersFilter

```properties
spring.cloud.gateway.filter.remove-hop-by-hop.order = <int: Integer.MAX_VALUE>
spring.cloud.gateway.filter.remove-hop-by-hop.headers = [ <string> ]
```

RequestRateLimiterGatewayFilterFactory

```properties
spring.cloud.gateway.filter.request-rate-limiter.deny-empty-key = <boolean: true>
spring.cloud.gateway.filter.request-rate-limiter.empty-key-status-code = <string>
```

更多的配置项请参照：[https://springdoc.cn/spring-cloud-gateway/appendix.html](https://springdoc.cn/spring-cloud-gateway/appendix.html)。