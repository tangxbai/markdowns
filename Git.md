#### 基础命令

```shell
$ git diff [file] # 比较工作区和暂存区的差异
$ git push --force origin [tag/branch] # 强制推送到服务器
$ git reset --soft HEAD^ # 撤销提交
$ git reset --hard HEAD^ # 丢弃本地暂存区保持和 HEAD 一致
$ git ls-files --others  # 查看没有被 git 管控的文件
```



#### 全局配置

```shell
$ git config --[global/local] user.name [name] # 配置用户用户
$ git config --[global/local] user.email [name] # 配置用户邮箱
$ git config --[global/local]--unset # 卸载用户配置
$ git config --[global/local]--list # 查看用户配置信息
```



#### 分支命令

```shell
$ git branch [branch]      # 创建本地分支
$ git push origin [branch] # 本地分支推送到远程
$ git checkout [branch]    # 切换到指定分支

$ git branch -d [branch] # 删除本地分支
$ git branch -D [branch] # 强行删除本地分支
$ git push origin --delete [branch] # 删除远程分支

$ git branch # 查看本地所有分支
```



#### 标签相关

```shell
git tag # 查看标签信息
git tag --show # 查看标签详细信息

git tag [tag] # 创建无注释标签
git tag -a [tag] -m "Comment" # 创建带注释标签

git push origin [tag] # 推送标签

git tag -d [tag] # 删除本地 tag
git push origin :refs/tags/[tag] # 删除远程 tag
```



#### 远程相关

```shell
git remote prune origin # 同步远程分支
git remote show origin # 查看当前仓库地址
git remote set-url origin [host] # 变更地址
```



#### github 令牌

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 
git remote set-url origin https://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@github.com/tangxbai/poi-lite.git



ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAID0MVGAbzN4Kd/h3eo6eRfZWKuFV6ucSF1K2qEdt8xMa tangxbai@hotmail.com