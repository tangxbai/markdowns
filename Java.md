## JAVA 面试知识点整理



### 1、JVM 内存结构

#### 1.1 堆（Heap)

<font color="green">**线程共享**</font>，Java 内存中最大的一块，所有对象实例、数组都在堆上分配，GC 主要回收的地方。

在堆中，内存又被划分为以下几部分（新生代、老年代、~~永久代~~）：

- 新生代：用来存放新生的对象，一般占据堆空间的 1/3。由于频繁创建对象，所以新生代会频繁触犯 MinorGC 进行垃圾回收。
  - Eden 区：Java 新对象的出生地，如果新创建的对象占用内存很大，则会直接分配到老年代。当 Eden 区内存不够的时候就会触发 MinorGC，对新生代区进行一次垃圾回收；
  - Survivor0 / from：上一次 GC 的幸存者，作为下一次 GC 的被扫描者;
  - Survivor1 / to：保留了一次 MinorGC 过程中的幸存者。
- 老年代：主要存放应用程序中生命周期长的内存对象，相对比较稳定，所以 MajorGC 不会频繁执行；
- ~~永久代：内存的永久保存区域，主要存放 Class  和 Meta 元数据信息，在 java8 以后此区域被移除，更改为元空间（Metaspace）；~~

#### 1.2 虚拟机栈（VM Stack)~~~~

**线程私有**，存放基本数据类型、对象的引用、方法出口等；

#### 1.3 方法区（Method Area）

<font color="green">**线程共享**</font>，储存类信息、常量、静态变量，即时编译器编译后的代码数据等，回收目标主要是常量池和类型的卸载。

#### 1.4 本地方法栈（Native Method Stack）

**线程私有**，和虚拟机栈类似，只不过服务于 Native 方法。

#### 1.5 程序计数器

**线程私有**，程前线程所执行的字节码的行号指示器，用于记录正在执行的虚拟机字节指定地址。





### 2、Java 中的引用类型

#### 2.1、强引用 

在 Java 中最常见的就是强引用，把一个对象赋值给一个引用变量，这个引用变量就是一个强引用（当一个对象被强引用变量引用时，它处于可达状态，是不能被垃圾回收机制回收的，因此强引用也是造成 Java 内存泄露的主要原因之一）。

#### 2.2、软引用

需要借助 SoftReference 来实现，对于只有软引用的对象来说，当系统内存足够时它不会被回收，当系统内存不足时会被回收。

#### 2.3、弱引用

需要借助 WeakReference 来实现，生存周期比弱引用更短，对于只有弱引用的对象来说，只要垃圾回收机制已运行，不管 JVM 内存空间是否足够，都会对该对象占用的内存进行回收。

#### 2.4、虚引用

需要借助 PhantomReference 来实现，不能单独使用，必须和引用队列一起使用（虚引用的主要作用是跟踪对象被垃圾回收的状态）。



### 3、基础知识

#### 3.1、数据类型

byte(1)、short(2)、char(2)、int(4)、float(4)、double(8)、long(8)、boolean(1)

#### 3.2、面向对象几大特征

- 封装：将一个对象的属性私有化，隐藏具体实现，仅提供给外界必要的访问方法；
- 继承：从已有类得到继承信息创建新类的过程；
- 多态：父类（接口）定义的引用变量可以指向子类（实现）的实例对象；
- 抽象：将一类对象的共同特征抽取出来的过程，并不关注这些行为具体是什么；

#### 3.3、面向对象基本原则

- 单一职责：类的功能要单一化，不能累积过多的功能；
- 开闭原则：对拓展开放，对修改封闭；
- 里氏替换原则：子类能够替换所有父类出现的地方；
- 依赖倒置原则：高层次的模块不应该依赖于低层次的模块，应该依赖于抽象；
- 接口分离原则：提供尽可能小的接口，尽量细化接口，而不是所有接口揉在一个接口里面；

#### 3.4、switch 支持哪些类型

Java5 以前仅支持：byte、int、short、char；

Java7 以后支持：byte、int、short、char、enum、String；

所有版本都不支持长整型（long）；

#### 3.5、访问修饰符

- public：无访问限制，所有类可见（使用对象：类、接口、变量、方法）；
- protected：仅同包和子包可访问（使用对象：变量、方法、内部类）；
- private：仅同类内部可见（使用对象：变量、方法、内部类）；
- default：仅同包内可见（使用对象：类、接口、变量、方法）；

#### 3.6、final 的作用

- 被 final 修饰的方法不可被重载；
- 被 final 修饰的类不可被继承；
- 被 final 修饰的变量不可被更改；

#### 3.7、final finally finalize 的区别？

- final 是访问修饰符，通常用于修饰类、方法、变量等；
- finally 是 try-catch 关键字，一般用于书写一定要执行的代码，通常用于关闭流资源等；
- finalize 是 Object 中的一个方法，该方法一般由垃圾回收器来调用；

#### 3.8、Java 中如何跳出多重循环？

使用 break-lable 语法，label 只要符合变量名定义规则即可，例如：

```java
label: for( int i = 0; i < 10; i ++) {
    for( int k = 0; k < 100; k ++) {
    	if ( i % 5 == 0 ) {
            break label;
        }
	} 
} 
```

#### 3.9、抽象类和接口的区别

相同点：

- 都不能直接实例化；
- 都位于继承的顶端，用于被其他类实现或继承；
- 都包含抽象方法，并且子类必须重写这些抽象方法；

不同点：

- 抽象类是用来描述事物的通用特征的，而接口是抽象方法的集合；
- 从设计层面来说，抽象类是对类的抽象，是一种模板设计，接口是行为的抽象，是一种行为规范；
- 抽象类使用 abstract 修饰，而接口使用 interface 修饰；
- 抽象类可以有构造方法，而接口没有；
- 抽象类中可以有普通方法，而接口中没有；
- 抽象类中的方法可以用任意修饰符修饰，而接口中只能是public 的；
- 一个类只能继承一个抽象类，但是可以实现多接口；
- 抽象类的中的属性可以定义为任意类型，而接口中的类型必须是 public static final 的；

#### 3.10、抽象类和普通类的区别

- 普通类不能有抽象方法，但抽象类可以有普通方法；
- 抽象类不能直接实例化，而普通类可以直接实例化；

#### 3.11、成员变量与局部变量的区别

- 作用域：成员变量针对整个类有效，而局部变量只在某个范围内有效；
- 存储位置：成员变量随对象的创建而产生，存储于堆内存中，而局部变量是在被执行语句块中，随着被调用而产生，存储于栈中；
- 生命周期：成员变量跟随对象而创建随对象消失而消失，而局部变量语句块执行结束自动释放；
- 初始值：成员变量有默认初始值，而局部变量必须设置初始值；

#### 3.12、内部类

在 Java 中，可以将一个类的定义放在另一个类的定义内部，**这就是内部类**， 内部类本身是类中的一个属性，因此与其他属性的定义方式完全一致。

有哪些内部类（静态内部类、成员内部类、局部内部类、匿名内部类）：

- 静态内部类：

  ```java
  // 书写方式
  public class Outer {
      private static int var = 1;
      static class StaticInner {
          public void call() {
              System.out.println( "StaticInner: " + var );
          }
      }
  }
  
  // 实例方式
  Outer.StaticInner inner = new Outer.StaticInner();
  inner.call();
  ```

- 成员内部类

  ```java
  // 创建方式
  public class Outer {
      private int a = 1;
      private static final int b = 2;
      class Inner {
          public void call() {
              System.out.println( String.format( "Outer.a = %d, Outer.b = %d", a, b ) );
          }
      }
  }
  
  // 实例方式
  Outer outer = new Outer();
  Outer.Inner inner = outer.new Inner();
  inner.call();
  ```

- 局部内部类

  ```java
  public class Outer {
      private int outA = 1;
      private static int outB = 2;
      public void call() {
          int innerA = 3;
          class Inner {
              private int innerB = 4;
              public void call() {
                  System.out.println( String.format( "outA: %d, outB: %d, innerA: %d, innerB: %d", outA, outB, innerA, innerB ) );
              }
          }
          Inner inner = new Inner();
          inner.call();
      }
  }
  ```

- 匿名内部类

  ```java
  public class Outer {
      public void call() {
          new A() {
              @Override
              public void call() {
                  System.out.println( "new call.A()" );
              }
          }
      }
      
      interface A {
      	void call();
      }
  }
  ```

#### 3.13、重写与重载

含义：重写是子类覆盖父类或接口中的方法定义，而重载则是发生在同类中方法名相同参数列表不同的方法。

区别：

- 重写发生在子类继承父类或实现接口的过程中，重载发生在同类中；
- 重写的方法的方法名明和参数列表以及返回值必须相同，而重载则只是方法名必须相同，且参数列表不同；
- 重载是编译时的多态性，而重写是运行时的多态性；



### 4、IO / NIO

- IO：
  - 阻塞 IO 模型
  - 非阻塞 IO 模型
  - 多路复用 IO 模型
  - 信号驱动 IO 模型
  - 异步 IO 模型（AIO）
- NIO：Non-blocking IO
  - Channel
  - Buffer
  - Selector



### 5、JVM 类加载器

#### 5.1、加载机制

JVM 类加载机制分为五个部分，分别为：**加载**、**验证**、**准备**、**解析**、**初始化** 等（生命周期中还有使用、卸载，总共七个）；

#### 5.2、类构造器

JVM 一共提供了三种类加载器：**启动类加载器**（Bootstrap ClassLoader）、**扩展类加载器**（Extension ClassLoader）、**应用程序加载器**（Application ClassLoader）；

- 启动类加载器：主要负责 `%JAVA_HOME%/lib` 目录中的且被虚拟机认可的类（按文件名识别，比如 rt.jar 等，可通过 -Xbootclasspath 指定）；
- 扩展类加载器：主要负责 `%JAVA_HOME%/lib/ext` 目录中的类（可以通过 java.ext.dirs 指定）；
- 应用程序类加载器：负责加载用户路径（classpath）中的类库，自行扩展的都属于应用程序加载器。



### 6、集合

集合类存放于 `java.util` 包下，从框架上分为：Collection（集合）、Map（映射），从用途上来说主要分为三大类：Set（集）、List（列表）、Map（映射）。

#### 6.1、List

- ArrayList：最常用的 List 实现类，内部基于数组实现，**随机访问和遍历数据较快**，**但不适用于插入、删操作**；
- LinkedList：内部使用链表的形式存储数据，**动态插入、删除数据较快**，**但随机访问和遍历数据比较慢**；
- Vector：和 ArrayList 一样，内部基于数组实现，唯一区别就是 Vector 支持线程同步操作，但也因此**访问速度慢、插入、删除都慢**，但是能保证线程安全；

#### 6.2、Set

- HashSet：Hash 表，内部基于 HashMap 实现，值无序，且唯一不重复；
- LinkedHashSet：有序的 Hash 表，内部基于 LinkedHashMap 实现，值有序，也是唯一不重复；
- TreeSet：内部基于二叉树原理实现，对加入到集合中的自动完成排序，因此是一个有序的 Set；

#### 6.3、Map

- HashMap：1.7以前基于 **数组+链表** 实现，1.8以后采用 **数组+链表+红黑树** 实现，当元素超过8个后自动将链表转换为红黑树。根据 hashCode 存取数据，大部分情况下可以直接定位到它的值，因此具有 **很快的访问速度**，但是值是无序的；
- LinkedHashMap：是 HashMap 的子类，是有序的 HashMap；
- Hashtable：遗留类，与 HashMap 类似，不过 Hashtable 是线程安全的，但是并发性能不如 ConcurrentHashMap 的分段锁，所以如果不需要线程安全的话使用 HashMap，需要线程安全使用 ConcurrentHashMap 即可；
- TreeMap：**可排序的 Map**，内部实现了 SortedMap，对于放入其中的值始终保持一定顺序；
- ConcurrentHashMap：和 HashMap 原理大致一样，但是 ConcurrentHashMap 是 **线程安全** 的；



### 7、多线程

#### 7.1、线程

7.1.1、创建线程的方式：

- 继承 Thread 类，重写 run 方法，然后调用 start 方法；
- 实现 Runnable 接口，重写 run 方法，然后通过 Thread 构造传入，并调用 Thread 的 start 方法；
- ExecutorService 创建线程池，然后调用 execute / submit 方法；

7.1.2、ExecutorServices 常用的创建线程池的方法：

- newFixedThreadPool：创建一个固定线程数的的线程池；
- newCachedThreadPool：创建一个可缓存的线程池；
- newScheduledThreadPool：创建一个客户延迟执行的线程池；
- newSingleThreadExecutor：创建一个单线程池；

7.1.3、线程的生命周期：

创建（New） → 就绪（Runnable）→ 运行（Running） → 阻塞（blocking） → 死亡（Dead）；

7.1.4、sleep 和 wait 的区别：

- sleep 属于 Thread 类，而 wait 属于 Object 类；
- sleep 方法只是在指定时间内让出 cpu，但是监控状态依然保持，到指定时间后会自动恢复运行状态；
- sleep 方法执行期间不会释放对象锁，而 wait 方法则会放弃对象锁，只有再次调用此对象的 notify 方法后线程才会重新获取对象锁进入运行状态；

7.1.5、start 与 run 方法的区别：

- start 方法是用来启动线程的底层方法，而 run 方法只是一个普通的方法；
- run 方法是用来书写线程业务逻辑的方法，单独执行和普通方法没有区别，并不会执行启动新线程；
- start 方法内部会调用 run 方法，以新线程的方式来执行 run 方法中的逻辑；
- 当 run 方法执行完成后，线程终止；

#### 7.2、锁

7.2.1、乐观锁

乐观锁是一种乐观思想，即认为读多写少，遇到并发写的可能性低，每次拿数据时都认为别人不会修改，所以不会上锁，但是在更新时会比对上一次版本号，相同则进行更改，否则重复（读 → 比较 → 写）的操作。

例如：**java.util.concurrent.atomic**，AtomicInteger，AtomicLong，AtomicReference 等等。

7.2.2、悲观锁

悲观锁即总是假设是最坏的情况，认为所有人都会去修改，所以读写操作时都会上锁，这样别人在拿取数据时就会阻塞直到拿到锁。Java 中典型的悲观锁就是 synchronized 和 ReentrantLock。

7.2.3、自旋锁

如果持有锁的线程横能在短时间内释放锁资源，那么那些灯带竞争的线程就不需要做内核态和用户态之间的切换进入阻塞挂起状态，他们只需要等一等（自旋），等持有锁的线程释放锁后即可立即获取锁，这样就避免用户线程和内核的切换的消耗。

7.2.4、Synchronized 同步锁

属于独占式的悲观锁，同时属于可重入锁，其核心组件有：

- Wait Set：放置被阻塞的线程；
- Contention List：所有请求锁的线程首先会被放置在这个队列中；
- EntryList：Contention List 中有资格成为候选资源的线程将被放置于此；
- OnDeck：当前正在竞争锁资源的线程；
- Onwer：当前已经获取到资源的线程；
- !Owner：当前被释放的线程；

7.2.5、ReentrantLock

实现了 Lock 接口定义的方法，是一种可能重入锁，除了能完成 synchronized 锁能完成的工作外，还提供了诸如可响应终端所、可轮询锁请求、定时锁等米面多线程死锁的方法。

7.2.6、Semaphore 信号量

7.2.7、AtomicInteger

7.2.8、可重入锁（递归锁）

7.2.9、公平锁与非公平锁

7.2.10、读写锁

7.2.11、共享锁和独占锁

7.2.12、重量级锁

7.2.13、轻量级锁

7.2.14、偏向锁

7.2.15、分段锁



### 8、常见的 JVM 参数

#### 8.1、堆栈相关

- -Xms：设置堆内存初始大小（示例：-Xms1024m）；
- -Xmx：设置堆内存最大值（示例：-Xmx128m）；
- -Xmn：设置年轻代的大小（示例：-Xmn2g）；
- -Xss：设置每个线程的堆栈大小（示例：-Xss1m）;
- -XX:PermSize：设置永久代大小（示例：-XX:PermSize=128m）；
- -XX:MaxPermSize：设置永久代空间最大值（示例：-XX:MaxPermSize=1024m）；
- -XX:NewRatio：设置新生代与老年代的大小比值（示例：-XX:NewRatio=4）；
- -XX:SurvivorRatio：设置 Eden 区与 Survivor 区的大小比值（示例：-XX:SurvivorRatio=4）；
- -XX:MaxTenuringThreshold：设置垃圾回收的最大年龄（示例：-XX:MaxTenuringThreshold=0）；

#### 8.2、垃圾收集器相关

- -XX:+UseParallelGC： 选择垃圾收集器为并行收集器；
- -XX:ParallelGCThreads：配置并行收集器的线程数（示例：-XX:ParallelGCThreads=20）；
- -XX:+UseConcMarkSweepGC：设置年老代为并发收集；
- -XX:CMSFullGCsBeforeCompaction：设置运行多少次GC以后对内存空间进行压缩、整理（示例：-XX:CMSFullGCsBeforeCompaction=5）；
- -XX:+UseCMSCompactAtFullCollection：打开对年老代的压缩。可能会影响性能，但是可以消除碎
  片；

#### 8.2、辅助信息相关

- -XX:+PrintGC：打印 GC 垃圾回收简要信息；
- -XX:+PrintGCDetails：打印 GC 垃圾回收详细信息；

