### 一、废话简介

Linux 是一种自由和开放源码的类 UNIX 操作系统。
Linux 英文解释为 **Linux is not Unix**。
Linux 是在 1991 由林纳斯·托瓦兹在赫尔辛基大学上学时创立的，主要受到 Minix 和 Unix 思想的启发。

Linux 遵循 GNU 通用公共许可证（GPL），任何个人和机构都可以自由地使用 Linux 的所有底层源代码，也可以自由地修改和再发布。

由于 Linux 是自由软件，任何人都可以创建一个符合自己需求的 Linux 发行版。



### 二、目录结构

- **/bin** - （Binaries）存放的是一些二进制可执行文件，就是那些 **经常使用的命令**；
- **/boot** - 启动 Linux 系统的核心文件，相当于 **系统盘**；
- /dev - （Device ）存放的是 Linux 的 **外部设备**；
- **/etc** - （Etcetera ）用于存放 **系统管理和配置文件** 等；
- /home - **用户主目录**（~），在 Linux 中每一个用户都有一个自己的主目录，用于存放产生的数据；
- /lib - Library 的缩写，系统最基本的 **动态连接共享库**，类似于 Windows 中的 dll 文件，相当于系统库；
- /lost+found - 此目录一般情况下是空的，当系统非法关机后，这里会存放一些相关文件；
- /media - **系统识别到的设备**，例如：U盘、光驱等，Linux 会把识别到的设备挂载到此目录下；
- /mnt - 为了让用户 **临时挂载其他的文件系统**，将光驱挂载到 /mnt/ 上，即可进入该目录查看光驱内容；
- /mydata - 不重要
- /opt - （Optional）默认是空的，可以存放 **第三方安装的软件**，和 /usr/local/ 很类似；
- /proc - （Process）**虚拟文件系统**，不存在于硬盘上而是在内存中，是系统内存数据的映射；
- /root - **超级权限的主目录**；
- /run - **临时文件系统**，存储系统启动以来的信息；
- **/sbin** - （Super user binaries）超级用户的二进制文件，存放的是系统管理员使用的系统程序；
- /srv - 存放的是一些服务启动之后需要访问的数据目录；
- **/sys** - 系统中的一个特殊目录，用于保存 **内核和设备信息**；
- /temp - 程序临时存放目录（JAVA 中的 "**java.io.tmpdir**" 则指向此目录）；
- **/usr** - （Unix shared resources）**系统应用程序**，自己安装的系统必要软件可以放在 **/usr/local/** 下；
- **/var** - （Variable）放置系统执行过程中经常变化的文件，比如各种 **日志文件等**；

特殊目录说明：

- /opt/ ：非必要的软件安装目录；
- /usr/local ：必要的软件安装目录（jdk/mysql等）；

- /var/logs/ ：可以用于存放程序产生的各种日志；
- "/" ：根目录
- "." ：当前目录（或：./)
- "../" ：上一级目录
- "~" ：当前登录用户的主目录（如以 test 账号登录，~ 则是 /home/test ）



### 安装软件

1、yum 安装

```shell
# 基础命令
yum install <package> -y  # 安装程序（-y：以确认的方式跳过所有需要询问的步骤）
yum remove <package>      # 卸载程序
yum deplist <package>     # 查看程序的依赖情况
yum search <package>      # 搜索软件包
yum clean all             # 清除缓存目录
yum makecache             # 重新生成最新缓存
yum list <pattern>        # 列出指定匹配模式下的安装包

# 更新软件包
yum update                # 更新系统
yum update <package>      # 更新程序

# 检测是否安装某软件包
rpm -qa|grep <package>
yum list installed|grep <package>

# 更换 yum 源
# http://mirrors.aliyun.com/repo/Centos-7.repo        # 阿里
# http://mirrors.163.com/.help/CentOS6-Base-163.repo  # 163网易
# http://mirrors.sohu.com/help/CentOS-Base-sohu.repo  # 搜狐
# https://mirrors.tuna.tsinghua.edu.cn/help/centos/   # 清华镜像
cd /etc/yum.repos.d

# 这里需要先用原始源下载 wget, 不然备份后就不能用 yum 安装东西了
yum install wget -y
# 下载阿里云的 yum 源
mv /etc/yum.repos.d/CentOS-Base.repo /etc/yum.repos.d/CentOS-Base.repo.bak
wget -O /etc/yum.repos.d/CentOS-Base.repo http://mirrors.aliyun.com/repo/Centos-7.repo
sed -i "s/http/https/g" /etc/yum.repos.d/CentOS-Base.repo

# 清除缓存
yum clean all
yum makecache

# 出现 epel.repo 和 epel-test.repo 则下载成功
yum install -y epel-release

# 再次清理缓存
yum clean all
yum makecache
```

2、自编译安装

```shell
$ ./configure  # 检测编译环境 | 选择安装路径 | 配置安装选项   
$ ./configure --enable ... --disable ... --with ... --prefix: <path>
$ make         # 编译程序
$ make install # 安装（复制编译后的二进制文件到指定的目录中）
$ make && make install # 简写
```



### 下载操作

1、wget

```shell
# 安装命令
# rpm -qa|grep wget    # 检测是否安装过
$ yum install wget -y  # 免确认安装 wget

# 操作命令
$ wget [options] <url>
$ wget -O <file> ...        # 重载下载文件名，否则默认以URL最后一个斜杠后的内容为文件名
$ wget -P <path> ...        # 指定下载目录
$ wget -c ...               # 断点续传
$ wget -x ...               # 下载网站内容并简历和服务器上一模一样的目录
$ wget -nd ...              # 下载服务器上所有内容
$ wget -b ...               # 后台下载（可以通过“tail -f wget-log”查看下载进度)
$ wget -tries=<n> ...       # 重试次数
$ wget –limit-rate=300k ... # 限制下载速度
```

2、curl

```shell
# 安装命令
# rpm -qa|grep curl    # 检测是否安装过
$ yum install curl -y  # 免确认安装 curl

# 操作命令
$ curl [option] <url>
$ curl --create-dirs -o <filename> ... # 指定下载文件名
$ curl -O ...          # 以原始文件名下载到本地
```



### 基础命令

```shell
# 基础命令
$ man <cmd> # 查看某个命令的帮助文档
$ ls -l     # 以列表的方式展示目录文件（或：ll）
$ nl <file> # 查看文件并展示行号
$ clear     # 清屏
$ reset     # 重置远程连接
$ exit      # 退出终端命令
$ cd <path> # 进入目录
$ mkdir <path> # 创建目录

# 
$ <cmd> |     # 管道命令
$ <cmd>|grep  # 管道过滤

# 查看命令是内置还是外部的
$ type -a <cmd>    
$ command -V <cmd>

# [mv] 
# 移动文件（剪切&粘贴）
$ mv [options] <source> <target>
$ mv -b ... # 当 target 存在时，在执行覆盖前会为源文件创建一个备份
$ mv -i ... # 如果 target 与 source 同名，则会先询问是否覆盖
$ mv -f ... # 强制覆盖
$ mv -u ... # 当 source 比 target 新或者 target 不存在时，才执行移动操作

# [rm]
# 删除文件
$ rm [options] <source>
$ rm -f
```



### # 关机命令

```shell
$ sync                      # 将内存中的数据同步到硬盘中，任何关机操作尽量先运行此命令

$ man shutdown   			# 获取该命令的帮助文档
$ shutdown -h <m> '提示信息' # m分钟后关机，并且将提示信息展示在登录用户的屏幕上
$ shutdown -h now           # 立马关机（或：poweroff）
$ shutdown -r now 			# 立马重启（或：reboot）
$ shutdown -c               # 取消关机

$ reboot 					# 立马重启（等同于 shutodnw -r now ）；
$ halt 						# 停止所有CPU功能，但是仍然保持通电；
```



### 远程操作

```shell
# [ssh] 
# 远程登录其他机器
$ yum install ssh -y          # 确保安装了 ssh
$ service sshd start          # 启动 ssh 服务
$ ssh -p <port> <user>@<host> # 回车后输入登录密码即可

# [scp] 
# 远程下载到本地
$ scp <user>@<host>:<path> <local-path>
# 本地复制到远程
$ scp <local-path> <user>@<host>:<path>
# 其他选项
# -r：递归子目录
# -P <port>：指定远程 scp 端口号
# 如果路径中有空格，则需要使用双反斜杠进行转义
# scp <user>@<host>:"<path>/ <path>" "<local-path> /<local-path>"
```



### 网络相关

```shell
# 查看网络
$ ifconfig

# [ping]
# 测试网络
$ ping -i <interval:number> -c <count:number> <host>
# 临时操作 ping 功能
$ echo 0 > /proc/sys/net/ipv4/icmp_echo_ignore_all # 临时开启 ping
$ echo 1 > /proc/sys/net/ipv4/icmp_echo_ignore_all # 临时禁用 ping
# 永久操作 ping 功能
$ vim /etc/sysctl.conf
```



### 文件操作

```shell
# [touch]
# 修改 target 的时间属性（如果文件不存在，则创建一个新文件）
$ touch <target> 

# [echo]
# 输出内容到目标文件
$ echo <content> >  <target> # 输出内容到 target（如果 target 不存在，则先创建）
$ echo <content> >> <target> # 追加内容到 target（如果 target 不存在，则先创建）
$ echo -n ... # 不尾随换行符
$ echo -e ... # 启用解释反斜杠功能
$ echo -E ... # 禁用解释反斜杠功能

# [sed]
# 流编辑器（Stream editor）
$ sed [-hnV][-e<script>][-f<script-file>] <target>
```



### 文件权限

```shell
# [chmod] Change mode
# 控制用户对文件的权限的命令
# 说明：u:rwx[421]，g:rwx[421]，a:rwx[421]
$ chmod [-cfVR] [ugoa]=[mode] <target>
$ chmod -R ...   # 递归更改权限
$ chmod +x ...   # 增加执行权限
$ chmod 777 ...  # 分配所有权限
```

