## Redis



### 简介

REmote DIctionary Server(Redis) 是一个由 Salvatore Sanfilippo 写的 **key-value** 存储系统，是跨平台的非关系型数据库。

Redis 是一个开源的使用 ANSI C 语言编写、遵守 BSD 协议、支持网络、可基于内存、分布式、可选持久性的键值对（Key-Value）存储数据库，并提供多种语言的 API。

Redis 通常被称为数据结构服务器，因为值（value）可以是字符串（string）、哈希（hash）、列表（list）、集合（sets）和有序集合（sorted sets）等类型。



### 基础数据类型

- 字符串（string）
- 哈希值（hash）
- 列表（list）
- 集合（set）
- 有序集合（zset）



##### 下载安装

1、自定义下载安装，[点击此处获取最新版](http://redis.io/download)。

```shell
$ mkdir /usr/local/redis
$ wget -P /user/local/redis http://download.redis.io/releases/redis-7.0.14.tar.gz
$ tar -xzvf /user/local/redis/redis-6.0.8.tar.gz
$ cd /user/local/redis
$ make && make install
```

2、从软件商下载最新程序

```shell
# 加载并安装
$ yum install redis -y

# 更改配置文件
$ vim /etc/redis.conf
# -----------------------------------------------------------
bind 0.0.0.0 ::1 		# 改成跟随主机 ip
requirepass redis0   	# 设置访问密码
protected-mode no		# 关闭节点保护模式
# -----------------------------------------------------------

# 设置开机启动并重启服务
systemctl enable redis 
systemctl restart redis
systemctl status redis
```



##### 基础命令

```shell
# 测试服务
$ redis-cli
# -----------------------------------------------------------
$ 127.0.0.1:6379> ping 
(error NOAUTH Authentication required.)
$ 127.0.0.1:6379> auth redis0
OK
$ 127.0.0.1:6379>ping
PONG
# -----------------------------------------------------------

# 基础命令
$ 127.0.0.1:6379>set <name> <value> # 设置一个简单值
$ 127.0.0.1:6379>sadd <name> <value>,... # 设置一个集合
$ 127.0.0.1:6379>get <name>         # 查询
$ 127.0.0.1:6379>keys *             # 获取所有的 key
$ 127.0.0.1:6379>flushdb            # 清空数据库
$ 127.0.0.1:6379>save|bgsave        # 触发 RDB 数据持久化
```





##### 数据持久化

RDB

AOF



##### 主从集群

