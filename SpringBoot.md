#### 基础程序

```java
@SpringBootApplication
public class Application {
	public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
```



#### 启动方式

```java
// 1）
// 静态调用
// 不用自己写任何启动类，使用spring-boot提供的默认启动方式。
// 这种方式基本上就只是为了能让程序跑起来，我们可以看作一个基础的demo，实际上不会使用这种方式。
// 这种方式需要使用命令行参数 --spring.main.sources 来指定程序源。
SpringApplication.main(String[] args);

// 2）
// 静态调用
// 通过指定对应的主要启动类来启动程序，这种方式比较常见，但是无法更改一些偏好设置，
// 当然偏好设置也不是必须的，所以这种方式比较常见。
SpringApplication.run(Class<?> primarySources, String[] args);

// 3）
// 静态调用
// 这种方式和上面无差异，主要是可以指定多个主要配置类。
SpringApplication.run(Class<?>[] primarySources, String[] args);

// 4）
// 手动实例化
// 这种方式启动程序才是最核心的方式，基本上面所有的启动方式内部都是直接实例化对象然后调用run方法，
// 而手动实例化这种方式我们可以对即将启动的容器做一些偏好设置，里面也提供了一些具体操作方法。
SpringApplication app = new SpringApplication(Class<?> ... primarySources);
app.getXXX(); // 获取某种对象
app.setXXX(); // 改变偏好设置
app.addXXX(); // 添加自定义对象
app.run(String[] args); // 启动程序
```



#### Banner

1. 自定义Banner：banner.gif -> banner.jpg -> banner.png -> banner.txt -> defaultBanner；
2. 更改Banner路径：<code>spring.banner.location=PATH</code> / <code>spring.banner.image.location = PATH</code>；
3. 关闭Banner：

   ```java
   // 参数配置
   spring.main.banner-mode=off
   // 手动关闭
   app.setBannerModel(Banner.Mode.OFF);
   ```
4. 以下是方便生成字符画的网址：

   <http://patorjk.com/software/taag>
   <http://www.network-science.de/ascii/>
   <http://www.degraeve.com/img2txt.php>



#### 启动过程

1. 程序开始；
2. 收集各种回调接口 <code>ApplicationContextIntializer</code> / <code>ApplicaitonListener</code>；
3. 创建并准备 Environment，过程中随着监听器的回调会对Environment其进行维护和更新；
4. 初始化 ApplicationContext，过程中会加载各种Bean到容器中；
5. 加载错误分析类，并在发生异常时对错误信息进行分析；
6. 刷新 ApplicationContext，过程中会重新处理容器中的所有 Bean，包括解析类上的注解等；
7. 加载完成；
8. 运行 Runner <code>CommandRunner</code> / <code>ApplicationRunner</code>。

   

#### 扩展机制 - SpringFactoriesLoader

其 springboot 之所以能做到自动化配置和极简化配置的最大功臣源于 <code>SpringFactoriesLoader</code>，这个类其实早在 spring3.2 以后就存在了，其核心在于在运行初期间加载所有 jar 包中处于 META-INF/spring.factories 文件中的配置项，该文件本质上是一个 properties 文件，所以必须遵循 key/value 的配置方式，而这里从类名就可以看出，这个文件主要配置的是 spring 的一些工厂类，我们可以直接使用这个类加载符合条件的所有配置项，然后在合适的时机执行这个工厂类。

比如 springboot 启用自动化装配的主要功臣：<code>@EnableAutoConfiguration</code> 结合 <code>AutoConfigurationImportSelector</code> 类从  SpringFactoriesLoader 中读取了所有配置的 EnableAutoConfiguration 所有自动装配类并通过一定的条件判断，然后注入到 IOC 容器中，供我们使用。

**核心注解**

- @Primary： 设置Bean优先级；
- @Profile： 在指定运行环境下生效；
- @Role： 给Bean定义一个角色；
- @Lazy： 是否延迟加载Bean；
- @Component： 基础组件，Ioc容器中所有组件都是一个组件；
- @ComponentScan： 组件扫描配置，配置基于哪个包进行组件扫描可配合其他组件使用；
- @Repository： DAO接口注解，需要引入相应的依赖；
- @Service： 该注解是@Component注解的一个特例，作用在类上，默认为单例状态；
- @Scope： 用来配置 spring bean 的作用域，它标识 bean 的作用域；
- @Configuration： 配置类，相当于老式xml配置中的根节点，但此类也不是必须的，也是一个组件；
- @SpringBootConfiguration： 官方推荐使用此注解代替@Configuration，作用无异；
- @ConfigurationProperties： 使用注解的方式将配置信息映射到实体bean中；
- @PropertySource： 加载自定义配置文件并映射到实体bean中；
- @EnableXXX： 功能选择开关，内部借助Import实现相应的功能载入；
- @Bean： Spirng Ioc bean对象，与xml配置中的<bean/>对等，方法返回值即bean id，也可属性配置；
- @Conditional： 条件判断，用于判断对应的类或者方法载入时机的判断；
- @Import： 导入配置类，可以导入普通的@Configuration配置类或者实现了ImportSelector/ImportBeanDefinitionRegistrar的类，或者普通的POJO类，spring自动注册到容器中；
- @ImportResource： 导入外部资源可以是properties/xml/groovy文件，但不能是yaml/yml文件；
- @Order： 更改Bean加载的顺序，按优先级加载，数字越小优先级越高；
- @AutoConfigureAfter： 在指定类加载之后执行自动装配；
- @AutoConfigureBefore： 在指定类记载之前执行自动装配；
- @AutoConfigureOrder： 自动装配顺序；
- @ImportAutoConfiguration： 导入自动配置，与@EnableAutoConfiguration有相同之处；
- @SpringBootApplication： 聚合注解，用于标识目标类是否由spring托管并启用对应的功能；

#### 条件判断

- @ConditionalOnClass： classpath中存在该类时生效；
- @ConditionalOnMissingClass： classpath中不存在该类时生效；
- @ConditionalOnBean： 容器中存在该类型Bean时生效；
- @ConditionalOnMissingBean ： 容器中不存在该类型Bean时生效；
- @ConditionalOnSingleCandidate： 容器中该类型Bean只有一个或@Primary的只有一个时生效；
- @ConditionalOnExpression： SpEL表达式结果为true时；
- @ConditionalOnProperty： 参数设置或者值一致时生效；
- @ConditionalOnResource： 指定的文件存在时生效；
- @ConditionalOnJndi： 指定的JNDI存在时生效；
- @ConditionalOnJava： 指定的Java版本存在时生效；
- @ConditionalOnWebApplication： Web应用环境下生效；
- @ConditionalOnNotWebApplication： 非Web应用环境下生效；



#### 比较重要的几个监听接口

ApplicationContextInitializer： 程序初始化监听
SpringApplicationRunListener： 程序运行步骤监听
ApplicationListener： 程序加载完成监听
ConfigurationClassParser： 解析标注了@Configuration的配置类
ConfigurationClassPostProcessor： @Configuration配置类处理器



#### 默认静态文件路径

1. classpath:/static
2. classpath:/public
3. classpath:/resources
4. classpath:/META-INF/resources
5. classpath:/META-INF/resources/webjars/
6. 以上静态资源目录均可以自定义扩展： `spring.resources.static-locations=PATH`



#### 错误处理

- @ControllerAdvice：
  - 优点：可以捕获所有 Controller 层发生的异常，优先级高；
  - 缺点：只能捕获 Controller 层发生的异常，其他系统层面发生的异常则无法捕获；

- BasicErrorController：
  - 优点：可以捕获所有的系统异常和业务异常
  - 缺点：优先级最低，容易被其他异常处理覆盖（但可以处理所有的异常行为）；



#### 话外

通过RuntimeException可以追踪方法调用栈链，以此获取到整个链式调用的所有目标方法。而springboot内部就是借助RuntimeException的方式来智能检测当前运行的主类的。



#### 配置跨域

```java
@Configuration
public class CorsConfiguration implements WebMvcConfigurer {
    
    @Override
    public void addCorsMappings( CorsRegistry registry ) {
        registry.addMapping( "/**" )
                .allowedOrigins( "*" )
                .allowCredentials( true )
                .allowedMethods( "GET", "POST", "PUT", "DELETE", "OPTIONS" )
                .maxAge( 3600 );
    }
    
}
```



#### Session 共享

常用方案：使用 Session + Redis 的方案，将 Session 序列化到 Redis 中，当每个服务中需要获取 Session 时，通过 SessionId 去 Redis 操作对应的 Session 即可。

