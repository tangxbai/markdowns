HttpServletResponse download



```java
@GetMapping( "/download" )
public void download( HttpServletResponse response ) throws IOException {
    
    String charset = StandardCharsets.UTF_8.displayName();
    String fileName = URLEncoder.encode( "文件名.xx", charset );

    response.setCharacterEncoding( charset );
    response.setContentType( "multipart/form-data" );
    response.setHeader( "Content-Disposition", "attachment;fileName=" + fileName );
    ...
    // Flush
    response.flushBuffer();
}
```

