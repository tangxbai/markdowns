### 1、GlobalFilter 与 GatewayFilter 执行顺序？

没有具体的先后顺序，但是可以通过 Order 值进行排序，最后通过 OrderedGatewayFilter 对其进行包装，并将两者合并封装排序，最后按照排序后的循序依次执行。



### 2、核心类

- GatewayAutoConfiguration
- DispatcherHandler
- FilteringWebHandler
- RouteLocator
- RouteDefinitionLocator
- RouteDefinitionWriter
- RouteDefinitionRepository



### 、Filter 有几种？

- GlobalFilter
- GatewayFilter
- HttpHeadersFilter



### 、Predicate

- RoutePredicateFactory
- Before
- After
- Between
- Cookie
- Header
- Host
- Method
- Path
- Query
- ReadBody
- RemoteAddr
- Weight
- CloudFundaryRouteService



### 、Filter

- Retry
- SetPath
- SetStatus
- PrefixPath
- StripPrefix
- RewritePath
- SaveSession
- RequestSize
- RequestHeaderSize
- RedirectTo
- *Hystrix*：
- *FallbackHeaders*：
- RequestHeaderToRequestUri
- ModifyRequestBody
- ModifyResponseBody
- AddRequestParameter
- SetRequestHeader
- MapRequestHeader
- AddRequestHeader
- RemoveRequestHeader
- SecureHeaders
- PreseveHostHeader
- SetResponseHeader
- AddResponseHeader
- RemoteResponseHeader
- RewriteResponseHeader
- DedupeResponseHeader
- RewriteLocationResponseHeader



### 、过滤器和网关的区别？

- 过滤器：仅针对单个服务的请求进行拦截；
- 网关：针对所有服务进行请求拦截；



### 、Zuul 和 Gateway 的区别？

- Zuul：是 Netfilx 旗下的，基于 Servlet 实现，阻塞式 Api，不支持长连接；
- Gateway：是 SpringCloud 开发的微服务网关，基于 Spring5 搭建，能够实现响应式非阻塞式 Api，支持长连接；



### 、Gateway 与 Nginx 的区别？



### 、动态发布路由



### 、请求限流







