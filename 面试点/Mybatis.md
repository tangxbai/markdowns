### 1、Mybatis 是什么？

Mybatis 是一个半 ORM 对象关系映射框架，内部封装了 JDBC 相关操作，开发时只需要关注 SQL 语句本身，不需要花太多精力去处理 JDBC 相关的东西，我们可以直接编写原生态 SQL，严格控制 SQL 执行性能，灵活度比较高。

Mybatis 可以使用 XML 和注解的方式来配置映射信息，将 POJO 映射成数据库中的记录，避免了几乎所有 JDBC 代码和手动设置参数以及处理结果集的操作。



### 2、Mybatis 的优缺点是什么？

优点：

- 基于 SQL 语句编程，很灵活，不会对应用程序或者数据库的现有设计造成任何影响，SQL 写在 XML 文件中，达到了 SQL 与代码解耦的效果，便于同一管理，并且支持动态 SQL 语句，并可重用；
- 减少了大量 JDBC 代码，不需要手动操作数据库；
- 兼容多种数据库；
- 提供映射标签，支持对象与数据库的 ORM 字段关系映射；
- 能很好的与 Spring 集成；

缺点：

- SQL 语句编写工作量大，对开发人员 SQL 的编写有一定的要求；
- SQL 依赖于数据库，移植性差，不能随意更换数据库；



### 3、Mybatis 和 Hibernate 的区别

相同点：都是对 JDBC 的封装，都是持久层框架，都用于 DAO 层的开发；

不同点：

- Mybatis 是半自动映射框架，主要手动书写 SQL，而 Hibernate 是全自动映射框架，内部使用 HQL 屏蔽了 SQL 的差异化；
- Hibernate 由于对 SQL 语句的封装，导致 SQL 优化困难，而 Mybatis 虽然需要自己编写 SQL，但优化相对容易；



###  4、Mybatis 工作原理

读取配置文件 → 加载映射文件 → 创建 SqlSessionFactory → 创建 SqlSession → Executor  → 输入参数映射 → 输出结果映射



### 5、功能架构是怎样的

- API 接口层：提供给外部使用的接口 API，通过这些 API 来操作数据库；
- 数据处理层：负责 SQL 的查找、解析、执行和处理结果映射等；
- 基础支撑层：负责基础的功能，包括数据连接、事务配置和缓存处理等；



### 6、为什么需要预编译？

定义：SQL 预编译指的是数据库驱动在发送 SQL 语句和参数给数据库之前对 SQL 语句进行编译，这样数据库在执行 SQL 时，就不需要编译了，如果没有预编译，则数据库每次执行 SQL 都需要编译，导致性能效率低下。

解答：使用预编译，在执行 SQL 前可以对对 SQL 进行优化，对于复杂的 SQL，在编译阶段可以合并多次操作为一个操作，同时编译后可以重复利用，对于同一个 SQL 再次执行可以利用前一次编辑过的对象直接执行，并且在 SQL 编译阶段可以有效防止 SQL 注入等问题。



### 7、有哪些 Executor 执行器？区别是什么？

- SimpleExecutor：每执行一次 update 或者 select，就开启一个 statement 对象，用完立刻关闭 statement 对象；
- ReuseExecutor：执行 update 或 select 时，以 SQL 作 key 查找 statement 对象，重复使用 statement 对象；
- BatchExecutor：批处理执行器，通过缓存多个 statement 对象，最后统一执行；



### 8、 如何指定其他 Executor？

- 在配置文件中设定：

  ```properties
  mybatis.executor-type = REUSE
  mybatis.configuration.default-executor-type = REUSE
  ```

- 在 SqlSessionFactory 创建 SqlSession 时手动指定：

  ```java
  sqlSessionFactory.openSession( ExecutorType.REUSE );
  ```



### 9、Mybatis 是否支持懒加载？

Mybatis 仅支持 assocication 和 collection 的延迟加载（即一对一，一对多），可通过设置配置中的 lazyLoadingEnabled 属性来启用延迟加载。

```properties
# 配置文件
mybatis.configuration.lazy-loading-enabled = true
# 代码设置
configuration.setLazyLoadingEnabled( true );
```



### 10、#{} 与 ${} 的区别

- #{} 是预编译处理符，而 ${} 是占位符，仅用于字符替换；
- #{} 在处理时，会将传入的参数转换成 ？的形式，通过 statement 的 set 方法来赋值；
- #{} 可以预防 SQL 注入，而 ${} 不能；
- #{} 的变量注入是在 DBMS 中，而 ${} 是在 mybatis 中；



### 11、怎么写 like 模糊查询语句？

- *'%${...}%'：可能引起 SQL注入，不推荐；*

- *“%“#{...}”%”：因为 #{} 在解析时，会自动在变量外侧添加单引号，所以此处需要用双引号 ""，不推荐，太复杂；*

- **CONCAT('%', #{...}, '%')：既能有效防止 SQL 注入，书写又简单，推荐；**

- *使用 bind 标签，不推荐，太复杂；*

  ```xml
  <select id="select" resultType="Abc">
      <bind name="pattern" value="'%' + param + '%'"/>
      select id, ... from table where field like #{pattern}
  </select>
  ```



### 12、Mapper 中如何传递多个参数？

- *顺序传参：在 SQL 中使用下标取值（例如：#{0}， #{1}等等），不建议使用，一旦方法更改或者下标没对上容易出错；*
- **Map传参：方法使用 Map 作为参数，SQL 中使用 #{key} 的方式取值，参数值较多时比较有用，推荐使用；**
- **Bean对象：方法定义中使用 JavaBean 作为参数，SQL 中使用 #{property} 的方式取值，推荐使用；**
- **@Param：在方法参数上使用 @Param 对参数进行标注，然后在 SQL 中使用 #{name} 的方式取值，推荐使用；**



### 13、如何执行批量操作？

- foreach 标签，这种方式需要在 jdbc.url 中开启 **allowMutiQueries=true** 支持：

  ```xml
  <insert id="addBatch">
  	INSERT INTO emp(ename,gender,email,did) VALUES
      <foreach collection="emps" item="emp" separator=",">
      	(#{emp.eName},#{emp.gender},#{emp.email},#{emp.dept.id})
      </foreach>
  </insert>
  ```

- 使用 BatchExecutor



### 14、如何获取生成的主键？

在 XML 标签中使用 **keyProperty** 属性即可（前提：useGeneratedKeys=true，keyColumn=id）



### 15、当实体类属性名和表字段不一致怎么办？

- 更改 SQL 别名，使得表字段与实体类属性一致；
- 通过 resultMap 描述映射关系；



### 16、Mapper 接口调用要求

- mapper 方法名必须和 xml 中定义的每个 sql 的 id 一致；
- mapper 方法参数类型要和 xml 中定义的 sql 的 parameterType 一致；
- mapper 方法返回值类型要和 xml 中 sql 的 resultType 一致；
- mapper 根节点的 namespace 属性必须是 mapper 的接口全路径；



### 17、Mybatis 是否可以映射枚举类？

Mybatis 不仅支持枚举类型，还可以支持任何对象，实现方式为自定义一个 TypeHandler，实现里面的 setParameter 和 getResult 方法，其实在 Mybatis 中所有的数据类型都是通过 TypeHandler 来转换的，只不过 Mybatis 内部仅提供了一些基础的类型处理器，比如基础的 IntegerHandler、FloatHandler、DoubleHandler、StringHandler 等等，比如复杂的或者无法实现具体业务的只能自己实现 TypeHandler 处理自己的业务逻辑了。



### 18、Mybatis 的一级、二级缓存

一级缓存：

**默认开启**，基于 PerpetualCache 的 HashMap 本地缓存，其存储作用域为 Session，当 Session flush 或 close 之后，该 Session 中的所有 Cache 将被清空。但是这里需要要注意一点，**如果项目中整合了 spring 框架的话，一级缓存将不会不生效**，除非在调用方法上添加 @Transactional 注解，至于为什么，请自行查阅相关资料。

二级缓存：

**默认开启**，在没有设定自己的 Cache 的前提下，默认使用 PerpetualCache 的本地缓存，但是作用范围是针对整个 Mapper 有效，使用方式为在 Mapper 映射文件中使用 `<cache/>` 节点或者在 Mapper 接口上使用 `@CacheNamespace` 注解即可。