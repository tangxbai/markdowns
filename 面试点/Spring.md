### 1、Spring 是什么？

Spring 是一个轻量级 Java 开发框架，目的是为了解决企业级应用开发的业务逻辑层和其他各层的耦合问题。它是一个分层的 JavaSE 一站式轻量级开源框架，为开发 Java 应用程序提供全面的基础架构支持。Spring 负责基础架构，因此我们可以专注于应用程序的开发。

Spring 最根本的使命是解决企业级应用开发的复杂性，即简化 Java 开发。

为了降低 Java 开发的复杂性，Spring 采取了以下几种关键策略：

- 基于 POJO 的轻量级和最小入侵性编程；
- 通过依赖注入和面向接口实现松耦合；
- 基于切面和惯例进行声明式编程；
- 通过切面和模板减少样板式代码；



### 2、Spring  的两大核心概念

**IOC**（Invers of Control）控制反转：

控制反转，也叫依赖注入，它不会直接创建对象，只是把对象声明出来，在代码中不直接与对象和服务进行连接，但是在配置文件中描述了哪一项组件需要什么服务，容器将他们组装起来。在一般的 IOC 场景中，容器创建了所有的对象，并设置了必要的属性将他们联系在一起，等到需要使用的时候才把他们声明出来，使用注解就更方便了，容器会自动根据注解把对象组合起来。

**AOP**（Aspect oriented programming）面向切面编程：

面向切面编程是一种编程模式，它允许开发者通过自定义的横切点进行模块化，将那些影响多个类的行为封装到可重用的模块中去。



### 3、Spring 的优缺点是什么？

优点：

- 方便解耦，极大的简化了开发工作，Spring 其实就是一个大工厂，可以将所有对象的创建和依赖关系的维护交给它管理；
- 支持面向切面编程，Spring 提供面向切面编程，可以很方便的实现对程序进行拦截，以实现通用的业务功能；
- 支持声明式事务，只需要通过配置就可以完成对事物的管理，而无需手动管理事务；
- 方便程序的测试，Spring 支持 junit 单元测试，可以通过注解方便的测试 Spring 程序；
- 方便地集成其他技术框架，Spring 内部提供了对这些框架的直接支持，让我们可以很方便的引入其他框架；
- 降低了 JavaEE 的使用难度，Spring 封装了大量的 J2EE 开发中可能会用到的各种API ；

缺点：

- Spring 依赖反射机制，大量的使用反射会有一定的效率影响；
- 使用门槛升高，对于 Java 开发者来说要使用 Spring 来说，入门需要花费一定时间；



### 4、Spring 由那些模块组成？

- spring-core：提供了框架的基本组成成分，包括 IOC 和 DI 功能；
- spring-beans：提供了 BeanFactory，是工厂模式的一个经典实现，Spring 处理 Bean 的基础实现；
- spring-context：提供了中框架式式的对象访问方法；
- spring-aop：提供了面向切面编程的实现，让你可以自定义拦截器、切面等；
- spring-web：提供了针对 Web 的开发集成；
- spring-jdbc：提供了 JDBC 和抽象层，消除了繁琐的 JDBC 编码，简化了大量 JDBC 代码；
- spring-test：为单元测试提供了支持；



### 5、Spring 中用到的设计模式？

- 工厂模式：BeanFactory 就是简单工厂模式的体现，用来创建对象的实例；
- 单例模式：创建的 Bean 模式是单例模式；
- 代理模式：Spring 的 AOP 用到了 JDK 的动态代理和 CGLIB 字节码生成技术；
- 模板方法：用来解决代码重复问题（如：RestTemplate、JmsTemplate、JpaTemplate）;
- 观察者模式：定义对象间一种一对多的依赖关系，当一个对象的状态发生改变时，所有依赖于它的对象都会得到通知被自动更新；
- 策略模式：定义对象间一对多的实例关系，可以根据不同的实现方式调用不同的处理方法；



### 6、Spring 中有哪些不同类型的事件？

- ContextRefreshedEvent：Spring 刷新上下文时被触发；
- ContextStartedEvent：Spring 上下文加载完成后被触发；
- ContextStoppedEvent：Spring 上下文停止时被触发；
- ContextClosedEvent：Spring 上下文关闭完后被触发；
- RequestHandledEvent：在 Web 应用中，当一个 http 请求结束后会触发该事件，如果一个 Bean 实现了 ApplicationListener 接口，则会被发送通知，我们可以在这里记录请求日志等信息；



### 7、什么是 IOC 容器？

- IOC（Inversion of Control）即控制反转，就是对组件对象控制权的转移，从程序代码本身转移到了外部容器；
- IOC 负责创建对象、管理对象、装配对象、配置对象，并且管理这些对象的整个生命周期。



### 8、IOC 的实现机制

IOC 的实现原理就是工厂模式（BeanFactory）加反射机制；



### 9、ApplicationContext 常见的实现有哪些？

- FileSystemXmlApplicationContext：从一个 XML 文件中加载 beans 的定义，主要针对本地文件系统；
- ClassPathXmlApplicationContext：从 classpath 中加载一个 XML 的 beans 定义；
- XmlWebApplicationContext：从 XML 中加载定义了 Web 应用 的所有 bean；



### 10、什么是依赖注入？

依赖注入（即 Dependency Injection），即组件之间的依赖关系由容器来决定，也就是由容器动态地将某种依赖的目标对象实例注入到应用系统中的关联组件中去。



### 11、有哪些依赖注入方式？

- ~~属性注入：由于灵活性和易用性较差，Spring 已不再推荐使用；~~
- 构造器注入：容器在创建组件对象实例时，通过分析组件的构造方法参数列表，将容器中符合参数类型的组件注入到组件中，以达到注入的目的，并且可以设置依赖为 fianl 不可更改，**推荐使用**；
- Setter 注入：容器在实例化组件后，会扫描所有的 Setter 方法，通过分析 Setter 方法的参数来查找对应的依赖组件，并通过 set 方法注入到组件中，**推荐使用**；



### 12、什么是 Spring beans？

Spring beans 是形成 Spring 应用的主要对象，它们被 Spring IOC 容器初始化、装配并管理。



### 13、Spring Bean 的定义包含什么？

包含了容器必须的所有配置元数据，比如如何创建一个 Bean，生命周期以及它的依赖等等。



### 14、Spring 有哪些作用域？

- prototype：一个 bean 可以有多个实例；
- **singleton：单例模式，只会存在同一个实例对象（默认值）；**
- request：每次 http 请求都会创建一个实例，基于 spring web 环境；
- session：在 http session 中每一个 session 会创建一个实例，基于 spring web 环境；
- application：在全局的 http session 中创建一个实例，基于 spring web 环境；



### 15、Spring 中的单例 Bean 是线程安全的吗？

不是，Spring 框架中的单例 Bean 不是线程安全的。



### 16、Bean 的生命周期

实例化 → 属性赋值 → 初始化 → 使用 → 销毁



### 17、@Autowired 是如何自动装配的？

在 Spring 启动时自动装载了一个  AutowiredAnnotationBeanPostProcessor 的处理器，会自动扫描配置了 @Autowired、@Resource 的类，然后容器根据配置的信息查找需要的 Bean，然后装配给该对象。Spring 会首先根据注入的类型进行查找，如果刚好有一个，就直接装配，如果有多个，则根据类型名称进行查找。



### 18、Spring 是如何解决的循环依赖？

Spring 通过引入三级缓存来解决循环依赖。其中一级缓存为单例池（singletonObjects），二级缓存为早期曝光对象 earlySingletonObjects，三级缓存为早期曝光对象工厂（singletonFactories）。当 A、B 两个类发生循环引用时，在 A 完成实例化后，就使用实例化后的对象去创建一个对象工厂，并添加到三级缓存中，如果 A 被 AOP 代理，那么通过这个工厂获取到的就是 A 代理后的对象，如果 A 没有被 AOP 代理，那么这个工厂获取到的就是 A 实例化的对象。当 A 进行属性注入时，会去创建 B，同时 B 又依赖了 A，所以创建 B 的同时又会去调用 getBean(a) 来获取需要的依赖，此时的 getBean(a) 会从缓存中获取，第一步，先获取到三级缓存中的工厂；第二步，调用对象工厂的 getObject 方法来获取对应的对象，得到这个对象后将其注入到 B 中。紧接着 B 会走完它的生命周期流程，包括初始化、后置处理器等。当 B 创建完后，会将 B 再注入到 A 中，此时 A 再完成它的整个生命周期。至此，循环依赖结束！

如何解决循环依赖：

- Bean 必须要是单例的，否则无法解决循环依赖问题；
- AB 通过 setter 注入；
- A 使用 setter 注入，B 使用构造注入；
- @Lazy 延迟加载；



### 19、为什么要使用三级缓存呢？二级缓存能解决循环依赖吗？

如果使用二级缓存解决循环依赖，意味着所有 Bean 在实例化后就要完成 AOP 代理，这样违背了 Spring 设计的原则，Spring 在设计之初就是通过后置处理器在 Bean 生命周期的最后一步来完成 AOP 代理，而不是在实例化后就立马进行 AOP 代理。



### 20、Spring 的事务隔离级别？

Spring 有五种隔离级别，默认值为 ISOLATION_DEFAULT（使用数据库的设置），其他四个隔离级别和数据库的隔离级别一致：

1. ISOLATION_DEFAULT：由底层数据库的默认隔离级别决定；
2. ISOLATION_READ_UNCOMMITTED：读未提交，最低隔离级别、事务未提交前，就可被其他事务读取（会出现幻读、脏读、不可重复读）；
3. ISOLATION_READ_COMMITTED：读已提交，一个事务提交后才能被其他事务读取到（会造成幻读、不可重复读，也是 SQL Server 的默认级别）；
4. ISOLATION_REPEATABLE_READ：可重复读，保证多次读取同一个数据时和事务开始时的内容是一致，禁止读取到别的事务未提交的数据（会造成幻读，也是 MySQL 的默认级别）；
5. ISOLATION_SERIALIZABLE：序列化，代价最高最可靠的隔离级别，该隔离级别能防止脏读、不可重复读、幻读。



### 21、Spring 的事务传播行为？

Spring 有七大事务行为，而事务传播行为是 Spring 框架独有的事务增强特性，这是 Spring 为我们提供的强大的工具箱，使用事务传播行为可以为我们的开发工作提供许多便利。（required/supports/mandatory/requires_new/not_supported/never/nested）

1. **PROPAGATION_REQUIRED**：如果当前存在事务，则加入该事务，否则就创建一个新事务（**Spring 默认的事务传播行为**）;
2. PROPAGATION_SUPPORTS：如果当前存在事务，则加入该事务，否则以非事务的方式执行；
3. PROPAGATION_MANDATORY：如果当前存在事务，则加入该事务，否则抛出异常；
4. PROPAGATION_REQUIRES_NEW：无论当前是否存在事务，都会开启一个新的事务执行；
5. PROPAGATION_NOT_SUPPORTED：以非事务的方式执行操作，如果当前存在事务，则把当前事务挂起；
6. PROPAGATION_NEVER：以非事务方式执行，如果当前存在事务，则抛出异常；
7. PROPAGATION_NESTED：如果当前存在事务，则在嵌套事务内执行。否则按 REQUIRED 的方式执行；



### 22、Spring MVC 有哪些组件？

- DispatcherServlet：核心控制器；
- HandlerMapping：映射控制器；
- Controller：处理器；
- ModelAndView：模型和视图控制器；
- ViewResolver：视图解析器；



### 23、Spring 什么场景下会使用到多例模式？

- 状态保存：对于有状态的 Bean，即 Bean 实例需要保存状态信息，而这个状态信息对于不同的请求可能是不同的，这时使用多例模式比较合适。例如，一个会话 Bean 可能需要为每个用户会话保留不同的状态；
- 避免并发问题：如果一个 Bean 在并发环境下使用，而它的一些方法或属性的访问不是线程安全的，那么使用多例模式可以为每个请求或线程提供一个独立的实例，从而避免了并发访问导致的问题；
- 请求相关性：在 Web 应用中，有时希望每个 HTTP 请求都有自己的 Bean 实例，这样可以确保线程隔离和请求间的状态不干扰，此时使用多例模式；
- 资源分配：对于一些需要频繁创建和销毁的轻量级对象，使用多例模式可以更好地管理资源，避免长时间占用不必要的内存空间；
- 动态代理：在使用动态代理时，每个代理对象需要有自己的实例；