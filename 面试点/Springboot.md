### 1、SpringBoot 核心注解有哪些？

- @SpringBootConfiguration：开启配置文件功能；
- @EnableAutoConfiguration：开启自动装配功能；
- @ComponentScan：配置扫描的程序包；



### 2、SpringBoot 有哪几种读取配置的方式？

- @PropertySource
- @Value
- @Environment
- @ConfigurationProperties



### 3、SpringBoot 自动装配原理？

1. 通过 @EnableAutoConfiguration 开启配置；
2. 通过 SpringFactoriesLoader 加载位于 META-INF/spring.factories 中的配置类；
3. 通过 @ConditionalOnXXX 过滤不需要的配置类；



### 4、SpringBoot 有几种注入 Bean 的方式？

1. 通过 XML 的 bean 节点定义；
2. 通过 @Service/@Controller/@Repository@Component 等进行标注；
3. 通过 @Bean 进行标注；
4. 通过 @Import 导入；
5. 通过 @ImportResource 导入；
6. 通过 ImportSelector 进行导入；
7. 通过 @ComponentScan 进行扫描；
8. 通过 BeanFactoryPostProcessor  动态注入 Bean；
9. 通过 ApplicationContext 中的 registerBean 进行注入；

